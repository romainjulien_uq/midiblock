; The name of the installer
Name "MidiBlock Team48"

; The file to write
OutFile "MidiBlock_Team48_installer.exe"

; The default installation directory
InstallDir $PROGRAMFILES\MidiBlock-Team48

; The text to prompt the user to enter a directory
DirText "Choose a directory"

;--------------------------------

Section ""

; Set output path to the installation directory.
SetOutPath $INSTDIR


; Add all files to the installation directory (contain bin and lib subdirectories)
File /r "MIDIblock-Release\*"


; Create Menu directory and shortcut
CreateDirectory "$SMPROGRAMS\MidiBlock Team 48"
CreateShortCut "$SMPROGRAMS\MidiBlock Team 48\Run MidiBlock.lnk" "$INSTDIR\bin\MIDIblock.exe"

; Create Desktop shortcut
CreateShortCut "$DESKTOP\MidiBlock_Team48.lnk" "$INSTDIR\bin\MIDIblock.exe"


; Write uninstaller
WriteUninstaller $INSTDIR\bin\Uninstall.exe

; Create Menu shortcut to uninstall file
CreateShortCut "$SMPROGRAMS\MidiBlock Team 48\Uninstall.lnk" "$INSTDIR\bin\Uninstall.exe"


SectionEnd ; end the section

; The uninstall section
Section "Uninstall"

Delete $INSTDIR\bin\Uninstall.exe
Delete $INSTDIR\bin\MIDIblock.exe
Delete $INSTDIR\lib\MIDIblock-Release.jar
Delete $INSTDIR\lib\javax.json-1.0.4.jar
Delete $INSTDIR\lib\javax.json-api-1.0.jar
Delete $INSTDIR\lib\rxtx-2.1.7.jar
Delete $INSTDIR\lib\librxtxSerial.jnilib
Delete $INSTDIR\lib\rxtxParallel.dll
Delete $INSTDIR\lib\rxtxSerial.dll

RMDir $INSTDIR\lib
RMDir $INSTDIR\bin
RMDir $INSTDIR

Delete "$DESKTOP\MidiBlock_Team48.lnk"

Delete "$SMPROGRAMS\MidiBlock Team 48\Run MidiBlock.lnk"
Delete "$SMPROGRAMS\MidiBlock Team 48\Uninstall.lnk"
RMDIR "$SMPROGRAMS\MidiBlock Team 48"

SectionEnd