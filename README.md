# MIDI BLOCK
By Team 48

The project is built using Gradle (or in this case a gradle wrapper to ensure that we all use
the same version of Gradle to build, launch and deploy the application)

## Launch application

### Option 1: Inside your IDE (with a gradle plugin)
Use Task 'launchApp'

### Option 2: Using command line
cd into the root folder
./gradlew launchApp



## Deploy application

### Step 1: Generate a zip file containing a .bat script and all the libraries
cd into the root folder
./gradlew distZip

### Step 2: (Optional) Convert .bat script into an executable .exe
Go into root folder -> build -> distributions
unzip 'MidiBlock-release.zip'
Go into MidiBlock-release -> bin 
convert MIDIblock.bat to an executable (for example using http://www.battoexeconverter.com)
remove MIDIblock.sh (auto-generated) and MIDIblock.bat scripts from /bin folder

### Step 3: Create installer using NSIS 
Download and install NSIS (http://nsis.sourceforge.net/Download)
Copy the folder MidiBlock-release (root folder -> build -> distributions -> [unzip]) to your desktop
Launch NSIS
Locate the .nsi script in the root folder installer-script.nsi
Run the script in NSIS (note that if you haven't followed step 2, you need to replace 
"MIDIblock.exe" by "MIDIblock.bat", at lines 27, 30, and 37 of the script... Or the shortcuts 
will point to a file that doesn't exist!)

