package inputs;

import java.util.Arrays;
import java.util.List;

/**
 * A list of inputs the user can choose from.
 */
public enum InputTypeEnum {
  Custom,
  Virtual,
  MidiFile;

  private static List<InputTypeEnum> allValues = Arrays.asList(InputTypeEnum.values());

  /**
   * @return An array containing all possible inputs.
   */
  public static Object[] getAllValues() {
    return allValues.toArray();
  }

}
