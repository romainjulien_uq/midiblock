package inputs;

import output.OutputManager;
import playcontrolpane.KeyMapper;
import blockscontrolpane.MidiStream;
import noteconverter.NoteConverter;

import java.awt.KeyboardFocusManager;
import java.awt.event.KeyEvent;
import java.util.HashSet;
import java.util.Set;
import javax.swing.JOptionPane;
import javax.swing.JTextField;



/**
 * The InputEventManager waits for inputs from:
 * - The QWERTY keyboard
 * - The custom keyboard
 * - The virtual keyboard
 * - The MIDI file parser,
 *
 * When an input is received, it checks if the user has selected an output an if
 * the selected source corresponds to the source of the inputs. If the input
 * is valid, it sends a note on/off message to the MidiStream  to be processed.
 */
public class InputEventManager {

  // The source of input, as selected by the user
  private InputTypeEnum source = null;

  private Set<Integer> alreadyPressed;
  // Singleton
  private static InputEventManager ourInstance = new InputEventManager();

  public static InputEventManager getInstance() {
    return ourInstance;
  }

  private InputEventManager() {
    alreadyPressed = new HashSet<>();
    // start listening for keyboard events
    initKeyboardEventDispatcher();
  }

  /**
   * Function called when a key is pressed on the QWERTY keyboard.
   * It checks if a mapping exists to a note currently displayed on the custom keyboard.
   * If the key is mapped, send a Note On message to the MidiStream.
   * @param event KeyEvent captured. event.keyCode is an int.
   */
  public void qwertyKeyPressed(KeyEvent event) {
    if (!proceed(InputTypeEnum.Custom)) {
      return;
    }

    String keyName;
    if ((keyName = KeyMapper.getKeyNameMappedTo(event.getKeyCode())) != null) { // null == no mapping
      MidiStream.getInstance().startChainNoteOn(NoteConverter.getMidiNumber(keyName));
    }
  }

  /**
   * Function called when a key is released on the QWERTY keyboard.
   * It checks if a mapping exists to a note currently displayed on the custom keyboard.
   * If the key is mapped, send a Note Off message to the MidiStream.
   * @param event KeyEvent captured. event.keyCode is an int.
   */
  public void qwertyKeyReleased(KeyEvent event) {
    if (!proceed(InputTypeEnum.Custom)) {
      return;
    }

    String keyName;
    if ((keyName = KeyMapper.getKeyNameMappedTo(event.getKeyCode())) != null) {
      MidiStream.getInstance().startChainNoteOff(NoteConverter.getMidiNumber(keyName));
    }
  }

  /**
   * Function called when a key is pressed on the custom keyboard.
   * Send a Note on message to the MidiStream.
   * @param keyName format: C#1, D6, etc
   */
  public void customKeyPressed(String keyName) {
    if (!proceed(InputTypeEnum.Custom)) {
      return;
    }

    MidiStream.getInstance().startChainNoteOn(NoteConverter.getMidiNumber(keyName));
  }

  /**
   * Function called when a key is released on the custom keyboard.
   * Send a Note off message to the MidiStream.
   * @param keyName format: C#1, D6, etc
   */
  public void customKeyReleased(String keyName) {
    if (!proceed(InputTypeEnum.Custom)) {
      return;
    }

    MidiStream.getInstance().startChainNoteOff(NoteConverter.getMidiNumber(keyName));
  }

  /**
   * Function called when a note is played on the VMPK.
   * Distinguish note on and note off messages and forward it to the MidiStream.
   *
   * @param note Midi number of the note played
   * @param status 144 if note on, 128 if note off
   */
  public void VMPKNotePlayed(int note, int status) {
    if (!proceed(InputTypeEnum.Virtual)) return;
    switch (status) {
      case 144: // Note on status
        MidiStream.getInstance().startChainNoteOn(note);
        break;
      case 128: // Note off status
        MidiStream.getInstance().startChainNoteOff(note);
        break;
      default:
        break;
    }
  }

  /**
   * Function called when a note is played with the MidiFileReader.
   * Distinguish note on and note off messages and forward it to the MidiStream.
   *
   * @param note Midi number of the note played
   * @param status 144 if note on, 128 if note off
   */
  public void MIDIFileNotePlayed(int note, int status) {
    if (!proceed(InputTypeEnum.MidiFile)) return;

    switch (status) {
      case 144: // Note on status
        MidiStream.getInstance().startChainNoteOn(note);
        break;
      case 128: // Note off status
        MidiStream.getInstance().startChainNoteOff(note);
        break;
      default:
        break;
    }
  }


  // ############## GETTER + SETTER METHODS ################# //


  public void setSource(InputTypeEnum source) {
    this.source = source;
  }

  public InputTypeEnum getSource() {
    return this.source;
  }

  // ################# HELPER METHODS ###################### //


  /**
   * Sets up a listener for the QWERTY keyboard. We ignore keyboard events when
   * the user is typing into a text field.
   * (The function fired on event should return true to avoid receiving the same event over
   * and over again).
   */
  private void initKeyboardEventDispatcher() {
    KeyboardFocusManager.getCurrentKeyboardFocusManager().addKeyEventDispatcher((e) -> {
      System.out.println("Key event: " + e.getKeyChar() + " keycode: " + e.getKeyCode() + " type: " + e.getID());
      // Ignore if user is typing into a text field
      if (e.getSource() instanceof JTextField) {
        return false;
      }

      e.getKeyCode();
      switch (e.getID()) {
        case KeyEvent.KEY_PRESSED:
          if (alreadyPressed.contains(e.getKeyCode())) {
            break;
          }
          qwertyKeyPressed(e);
          alreadyPressed.add(e.getKeyCode());
          break;
        case KeyEvent.KEY_RELEASED:
          qwertyKeyReleased(e);
          alreadyPressed.remove(e.getKeyCode());
          break;
        default:
          break;
      }

      return false;
    });

  }

  /**
   * When an input is received, we first check if the user has selected a source
   * and an output, and if the selected source matches the source of the incoming input.
   *
   * @param expectedSource source of input
   * @return true if a source and output are selected, and sources match
   */
  private boolean proceed(InputTypeEnum expectedSource) {
    if (source == null) {
      JOptionPane.showMessageDialog(null, "Select a source");
      return false;
    }

    if (!OutputManager.getInstance().hasSelectedOutput()) {
      JOptionPane.showMessageDialog(null, "Select at least one output");
      return false;
    }

    return source.equals(expectedSource);
  }
}
