package inputs;

import settings.ControlPane;

import java.io.File;
import javax.sound.midi.MetaEventListener;
import javax.sound.midi.MetaMessage;
import javax.sound.midi.MidiSystem;
import javax.sound.midi.Sequence;
import javax.sound.midi.Sequencer;


/**
 * The MidiFileReader loads a Midi file from disk, and play or pause it on request.
 */
public class MidiFileReader {

  public static final int END_OF_TRACK = 47;

  // A notification is sent to the control pane when the lecture is over.
  private ControlPane callBack;

  // The sequencer used to read the file
  private Sequencer sequencer;

  /**
   * Open the given file, and a sequencer to process it. Set the sequencer receiver to
   * be a new MIDIMessageReceiver, and add a listener to be notified on EOF.
   *
   * @param filename A Midi file.
   * @param controlPane Pointer to the control pane.
   */
  public MidiFileReader(File filename, ControlPane controlPane) {
    callBack = controlPane;
    try {
      // Extract sequence in Midi file.
      Sequence sequence = MidiSystem.getSequence(filename);

      // Set up the Sequencer to process the sequence in MidiFile
      sequencer = MidiSystem.getSequencer();
      sequencer.open();
      sequencer.getTransmitter().setReceiver(new MIDIMessageReceiver(InputTypeEnum.MidiFile));
      sequencer.setSequence(sequence);

      // Listen for end of track
      sequencer.addMetaEventListener(new MetaEventListener() {
        public void meta(MetaMessage m) {
          if (m.getType() == END_OF_TRACK) {
            // Notify control pane
            callBack.endOfMidiFile();
            // Prepare sequencer to read sequence again.
            sequencer.setMicrosecondPosition(0);
          }
        }
      });
    } catch (Exception e) {
      System.out.println("Error while playing file...");
    }

  }

  /**
   * Start processing the sequence in MidiFile.
   */
  public void startPlaying() {
    sequencer.start();
  }

  /**
   * Stop processing the sequence in Midi file. Restart sequence on next play.
   */
  public void stopPlaying() {
    sequencer.stop();
    sequencer.setMicrosecondPosition(0);
  }

  /**
   * @return True if sequencer is currently playing file, and false otherwise.
   */
  public boolean isRunning() {
    return sequencer.isRunning();
  }
}
