package inputs;

import javax.sound.midi.MidiMessage;
import javax.sound.midi.Receiver;

/**
 * A custom Receiver to capture Midi messages sent by the MidiFileReader and the VMPK.
 */
public class MIDIMessageReceiver implements Receiver {

  // Keeps track of the source of input
  private InputTypeEnum inputSource;

  public MIDIMessageReceiver(InputTypeEnum source) {
    inputSource = source;
  }

  /**
   * Extract the Midi number and the status in the message and forward it to the InputEventListener.
   *
   * @param message Midi message received.
   * @param timeStamp not used.
   */
  @Override
  public void send(MidiMessage message, long timeStamp) {
    byte[] data = message.getMessage();
    int status = message.getStatus();

    switch (inputSource) {
      case Virtual:
        InputEventManager.getInstance().VMPKNotePlayed((int) data[1], status);
        break;
      case MidiFile:
        InputEventManager.getInstance().MIDIFileNotePlayed((int) data[1], status);
        break;
      default:
        break;
    }
  }

  @Override
  public void close() {
    //
  }
}
