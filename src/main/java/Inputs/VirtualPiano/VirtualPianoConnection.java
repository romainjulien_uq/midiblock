package inputs.virtualpiano;

import inputs.InputTypeEnum;
import inputs.MIDIMessageReceiver;

import javax.sound.midi.MidiDevice;
import javax.sound.midi.MidiSystem;
import javax.sound.midi.MidiUnavailableException;
import javax.sound.midi.Transmitter;

/**
 * A static class used to connect to the Virtual Midi Piano Keyboard.
 * It allows to retrieve a list of MidiDevices connected to the application and
 * to connect to one of them
 */
public class VirtualPianoConnection {

  // List of devices available
  private static MidiDevice.Info[] devicesAvailable;
  // The Midi device connected
  private static MidiDevice device;
  // A connection status to be displayed in the GUI
  private static String status = "Not connected";

  public VirtualPianoConnection() {
  }

  /**
   * Given the index of a device in devicesAvailable, it creates a new MIDIMessageReceiver
   * and set it to the receiver of the device.
   * @param index
   * @return true if the connection with the device was successful, and false otherwise
   */
  public static boolean openDeviceAtIndex(int index) {
    try {
      device = MidiSystem.getMidiDevice(devicesAvailable[index]);

      Transmitter trans = device.getTransmitter();
      trans.setReceiver(new MIDIMessageReceiver(InputTypeEnum.Virtual));
      device.open();

      status = "Connected to: " + devicesAvailable[index];
      return true;
    } catch (MidiUnavailableException e) {
      status = "Not connected";
      return false;
    }
  }

  public static void close() {
    if (device != null) {
      device.close();
    }
  }

  /**
   * @return The list of available devices
   */
  public static MidiDevice.Info[] getAvailableDevices() {
    devicesAvailable = MidiSystem.getMidiDeviceInfo();

    return devicesAvailable;
  }

  /**
   * @return The connection status
   */
  public static String getStatus() {
    return status;
  }


}

