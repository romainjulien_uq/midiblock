package noteconverter;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * A set of static methods used to conveniently convert midi note names to midi numbers, and do
 * other forms of calculations on midi numbers or note names.
 *
 * Constants used:
 * Middle C : C4 = 60, so C1 = 24, and B7 = 107
 * Number of notes in an octave: 12
 */
public class NoteConverter {

  // Maps the 12 key name in an octave to a note number (from 0 to 11)
  private static final Map<String, Integer> noteValueMap;

  // Maps a note number to its name
  private static final Map<Integer, String> noteNameMap;

  static {
    noteValueMap = new HashMap<>();
    noteValueMap.put("C", 0);
    noteValueMap.put("C#", 1);
    noteValueMap.put("D", 2);
    noteValueMap.put("D#", 3);
    noteValueMap.put("E", 4);
    noteValueMap.put("F", 5);
    noteValueMap.put("F#", 6);
    noteValueMap.put("G", 7);
    noteValueMap.put("G#", 8);
    noteValueMap.put("A", 9);
    noteValueMap.put("A#", 10);
    noteValueMap.put("B", 11);

    noteNameMap = new HashMap<>();
    noteNameMap.put(0, "C");
    noteNameMap.put(1, "C#");
    noteNameMap.put(2, "D");
    noteNameMap.put(3, "D#");
    noteNameMap.put(4, "E");
    noteNameMap.put(5, "F");
    noteNameMap.put(6, "F#");
    noteNameMap.put(7, "G");
    noteNameMap.put(8, "G#");
    noteNameMap.put(9, "A");
    noteNameMap.put(10, "A#");
    noteNameMap.put(11, "B");

  }

  public NoteConverter() {
  }

  /**
   * Calculate the note number of a midi number, 0 to 11, as defined in noteNameMap.
   * @param note midi number of a note
   * @return the note number
   */
  public static int getNoteNumber(int note) {
    return note % 12;
  }

  /**
   * Calculate the note number of a midi number, 0 to 11, as defined in noteNameMap.
   * @param noteName the full name of a note
   * @return the note number
   */
  public static int getNoteNumber(String noteName) {
    String keyName = "";
    char note = noteName.charAt(0);
    keyName += note;
    if (noteName.contains("#")) {
      keyName += "#";
    }

    return noteValueMap.get(keyName);
  }

  /**
   * Calculate the octave of a midi number.
   * @param note midi number of a note
   * @return the octave of the note
   */
  public static int getOctave(int note) {
    return (note / 12) - 1;
  }

  /**
   * Convert a midi number to its string representation (in the format: C#1, C1).
   * @param midiNumber to convert
   * @return the note name
   */
  public static String getNoteName(int midiNumber) {
    int octave = getOctave(midiNumber);
    String noteName = noteNameMap.get(getNoteNumber(midiNumber));

    return ("" + noteName.charAt(0) + (noteName.contains("#") ? "#" : "") + octave);
  }

  /**
   * Compute the midi number of a note, given its octave and its note number.
   * @param noteNumber a note number (0 to 11)
   * @param octave an octave
   * @return The midi number of a note
   */
  public static int getMidiNumber(int noteNumber, int octave) {
    return 24 + (12 * (octave - 1)) + noteNumber;
  }

  /**
   * Convert the string representation of a note (in the form C#1, C1) to a midi number.
   *
   * @param noteName the full name of a note
   * @return corresponding midi number
   */
  public static int getMidiNumber(String noteName) {
    // Get the note number (0 to 11)
    int noteNumber = getNoteNumber(noteName);

    // Get the octave number
    char octave = noteName.charAt(noteName.length() - 1);
    int octaveInt = Integer.parseInt(Character.toString(octave));

    // return midi number
    return getMidiNumber(noteNumber, octaveInt);
  }

  /**
   * Take a list of note names as parameters, and an octave, and return the corresponding
   * sorted list of midi numbers.
   *
   * @param notes List of note names
   * @param octave Octave of the notes
   * @return List of midi numbers
   */
  public static List<Integer> getMidiNumbersForNotes(List<String> notes, int octave) {
    List<Integer> midiNumbers = new ArrayList<>();

    for (String note : notes) {
      int noteNumber = getNoteNumber(note);
      midiNumbers.add(getMidiNumber(noteNumber, octave));
    }

    Collections.sort(midiNumbers);
    return midiNumbers;
  }


  /**
   * Convert the given list of note names to a list of midi numbers based on given octave,
   * then keep adding note, based on the next octave until the list contains 8 notes.
   *
   * @param notes List of note names
   * @param octaveRootNote Octave of the first note
   * @return A list of 8 midi numbers, such that their corresponding note name are in the
   * given list of notes
   */
  public static List<Integer> getFirstEightNotes(List<String> notes, int octaveRootNote) {
    List<Integer> eigthNotes = getMidiNumbersForNotes(notes, octaveRootNote);

    int index = 0;
    while (eigthNotes.size() != 8) {
      eigthNotes.add(eigthNotes.get(index) + 12);
      index++;
    }
    return eigthNotes;
  }
}
