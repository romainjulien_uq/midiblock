package blockscontrolpane.configuration;

import blockscontrolpane.BlockTypeEnum;
import blockscontrolpane.BlocksConfigurationPane;
import blockscontrolpane.MidiStream;
import blockscontrolpane.processingblocks.ChordifyBlock;
import blockscontrolpane.processingblocks.MonophonicBlock;
import blockscontrolpane.processingblocks.PitchShiftBlock;
import blockscontrolpane.processingblocks.ProcessingBlock;
import blockscontrolpane.processingblocks.arpeggiator.ArpeggiatorBlock;
import blockscontrolpane.processingblocks.arpeggiator.ArpeggiatorModeEnum;
import blockscontrolpane.processingblocks.gate.GateBlock;
import blockscontrolpane.processingblocks.gate.GateModeEnum;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import javax.json.JsonReader;
import javax.json.JsonValue;
import javax.json.JsonWriter;

/**
 * Read and save JSon configuration files in root directory.
 */
public class ConfigurationManager {

  // The list of configuration files currently in root directory
  private static List<String> listConfigurations;

  /**
   * Retrieve all files with extension '.json'
   */
  public ConfigurationManager() {
    listConfigurations = new ArrayList<>();

    File f = new File(".");
    ArrayList<String> filesInCurrentDir = new ArrayList<>(Arrays.asList(f.list()));
    for (String fileName : filesInCurrentDir) {
      if (fileName.contains(".json")) {
        listConfigurations.add(fileName.replace(".json", ""));
      }
    }
  }

  /**
   * Takes a configuration name as a parameter and save the current configuration in
   * MidiStream to file as a JSon array of blocks:
   * [{
   * block-type : BlockTypeEnum ,
   * parameters: []
   * },
   * { ... },
   * { ... },
   * <p>
   * <p>...<p>.
   * <p>
   * <p>]<p>.
   *
   * @param configurationName
   * @return true, if the JSon array was successfully written to file, false otherwise.
   */
  public static boolean saveConfiguration(String configurationName) {
    // Get list of processing blocks currently in stream
    List<ProcessingBlock> midiStream = MidiStream.getInstance().getBlocksInStream();
    // Array of blocks to be written to file
    JsonArrayBuilder listBlocksBuilder = Json.createArrayBuilder();

    for (ProcessingBlock block : midiStream) {

      if (block.getType() == null) {
        continue;
      }

      // Object representing a block
      JsonObjectBuilder blockBuilder = Json.createObjectBuilder();
      // Array of parameters for the block
      JsonArrayBuilder parametersListBuilder = Json.createArrayBuilder();

      int indexParam = 1;
      // Construct array of parameters (if any)
      for (Object param : block.getParameters()) {
        JsonObjectBuilder parameterBuilder = Json.createObjectBuilder();
        String parameterString;

        if (param instanceof GateModeEnum) {
          parameterString = ((GateModeEnum) param).getValue();
        } else if (param instanceof ArpeggiatorModeEnum) {
          parameterString = ((ArpeggiatorModeEnum) param).getValue();
        } else { //int
          parameterString = String.valueOf(param);
        }

        parameterBuilder.add(String.valueOf(indexParam), parameterString);
        parametersListBuilder.add(parameterBuilder.build());

        indexParam++;
      }

      // Construct block object
      blockBuilder.add("type", block.getType().getValue());
      blockBuilder.add("parameters", parametersListBuilder.build());
      // Add it to the list of blocks
      listBlocksBuilder.add(blockBuilder.build());
    }

    //Compute JSon Array
    JsonArray listBlocksArray = listBlocksBuilder.build();
    try {
      // write to given file
      OutputStream os = new FileOutputStream(configurationName + ".json");
      JsonWriter jsonWriter = Json.createWriter(os);
      jsonWriter.writeArray(listBlocksArray);
      jsonWriter.close();
      return true;
    } catch (FileNotFoundException e) {
      System.out.println("Can't write configuration file: " + configurationName + ".json");
      return false;
    }

  }

  /**
   * Given a configuration name from the list of configurations in root directory,
   * process the JSon file and re-create processing blocks, then add it to Midi stream.
   *
   * @param configurationName
   * @param blockPane         BlockConfiguration panel: parent of each processing block
   */
  public static void loadConfiguration(String configurationName, BlocksConfigurationPane blockPane) {
    // The list of processing blocks to send to the MidiStream
    List<ProcessingBlock> configurationBlocks = new ArrayList<>();

    // Array of blocks stored in configuration files
    JsonArray listBlocksArray;
    try {
      InputStream inputStream = new FileInputStream(configurationName + ".json");
      JsonReader jsonReader = Json.createReader(inputStream);
      listBlocksArray = jsonReader.readArray();
      jsonReader.close();
      inputStream.close();
    } catch (Exception e) {
      // should never happen
      System.out.println("Can't open configuration file: " + configurationName + ".json");
      return;
    }

    // Loop through the JSon array of blocks and re-instanciate the processing blocks with their parameters
    for (JsonValue block : listBlocksArray) {
      JsonObject blockObj = (JsonObject) block;
      JsonArray parameters;
      BlockTypeEnum blockType = BlockTypeEnum.getEnumForValue(blockObj.getJsonString("type").getString());
      switch (blockType) {
        case PitchShift:
          parameters = blockObj.getJsonArray("parameters");
          JsonObject shiftParamObject = (JsonObject) parameters.get(0);
          int shiftValue = Integer.parseInt(shiftParamObject.getString("1"));

          PitchShiftBlock pitch = new PitchShiftBlock(shiftValue, blockPane);
          configurationBlocks.add(pitch);
          break;
        case Monophonic:
          MonophonicBlock mono = new MonophonicBlock(blockPane);
          configurationBlocks.add(mono);
          break;
        case Chordify:
          ChordifyBlock chordo = new ChordifyBlock(blockPane);
          configurationBlocks.add(chordo);
          break;
        case Arpeggiator:
          parameters = blockObj.getJsonArray("parameters");
          JsonObject arpeModeParamObject = (JsonObject) parameters.get(0);
          ArpeggiatorModeEnum arpeMode = ArpeggiatorModeEnum.getEnumForValue(arpeModeParamObject.getString("1"));

          ArpeggiatorBlock arpo = new ArpeggiatorBlock(arpeMode, blockPane);
          configurationBlocks.add(arpo);
          break;
        case Gate:
          parameters = blockObj.getJsonArray("parameters");
          JsonObject gateModeParamObject = (JsonObject) parameters.get(0);
          GateModeEnum gateMode = GateModeEnum.getEnumForValue(gateModeParamObject.getString("1"));

          JsonObject notesPerTickParamObject = (JsonObject) parameters.get(1);
          float notesPerTickValue = Float.parseFloat(notesPerTickParamObject.getString("2"));

          GateBlock gate = new GateBlock(gateMode, notesPerTickValue, blockPane);
          configurationBlocks.add(gate);

          break;
        default:
          // should never happen
          System.out.println("Unrecognized block type: " + configurationName + ".json");
          break;
      }
    }

    // Update MidiStream
    MidiStream.getInstance().setBlocks(configurationBlocks);
  }

  /**
   * @return A list of configuration file names in root directory
   */
  public static Object[] getListConfigurations() {
    return listConfigurations.toArray();
  }

}
