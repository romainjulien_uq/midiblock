package blockscontrolpane.configuration.configurationchange;

import blockscontrolpane.MidiStream;
import blockscontrolpane.processingblocks.ProcessingBlock;

/**
 * Represent the deletion of a block to the MidiStream.
 * In order to restore it, we must keep track of the index in the MidiStream
 * where the change happened.
 */
public class Delete extends ConfigurationChange {

  private int indexInStream;

  public Delete(ProcessingBlock block, int index) {
    super(block);
    this.indexInStream = index;
  }

  /**
   * To undo this change, we add the block at index where it was removed
   */
  @Override
  public void undo() {
    MidiStream.getInstance().addBlock(this.indexInStream, this.block);
  }
}
