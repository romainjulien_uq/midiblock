package blockscontrolpane.configuration.configurationchange;

import blockscontrolpane.MidiStream;
import blockscontrolpane.processingblocks.ProcessingBlock;

/**
 * Represent the change of a block type. (ie, replacement of a ProcessingBlock by
 * another in the MidiStream)
 * In order to restore it, we must keep track of the block that was replaced, and the
 * block that replaced it.
 */
public class ChangeBlockType extends ConfigurationChange {

  // The block that replaced oldBlock
  private ProcessingBlock newBlock;

  public ChangeBlockType(ProcessingBlock oldBlock, ProcessingBlock newBlock) {
    super(oldBlock);
    this.newBlock = newBlock;
  }

  /**
   * To undo this change, we replace newBlock in the Stream by oldBlock.
   */
  @Override
  public void undo() {
    MidiStream.getInstance().replaceBlock(this.newBlock, this.block);
  }
}
