package blockscontrolpane.configuration.configurationchange;

import blockscontrolpane.processingblocks.ProcessingBlock;

/**
 * An instance of a ConfigurationChange represent a change that occured in the
 * BlockConfigurationPane.
 * The abstract undo() method must be implemented as a way to recover the change.
 */
public abstract class ConfigurationChange {

  // The block concerned by the change
  protected ProcessingBlock block;

  public ConfigurationChange(ProcessingBlock block) {
    this.block = block;
  }

  /**
   * Considering the change represented by the instance of ConfigurationChange,
   * this method must be implemented to allow to recover from it.
   */
  public abstract void undo();
}
