package blockscontrolpane.configuration.configurationchange;

import blockscontrolpane.processingblocks.ProcessingBlock;

import java.util.List;

/**
 * Represent the change of a parameter for a ProcessingBlock.
 * In order to restore it, we must keep track of the old parameters.
 */
public class ChangeParameter extends ConfigurationChange {

  private List<Object> parameters;

  public ChangeParameter(ProcessingBlock block, List<Object> params) {
    super(block);
    this.parameters = params;
  }

  /**
   * To undo this change, we set the block with the old parameters.
   */
  @Override
  public void undo() {
    this.block.setParameters(this.parameters);
  }
}
