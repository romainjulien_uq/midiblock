package blockscontrolpane.configuration.configurationchange;

import blockscontrolpane.MidiStream;
import blockscontrolpane.processingblocks.ProcessingBlock;

/**
 * Represent the left-shifting of a block in the MidiStream.
 */
public class ShiftRight extends ConfigurationChange {

  public ShiftRight(ProcessingBlock block) {
    super(block);
  }

  /**
   * To undo this change, we shift the block right in the MidiStream
   */
  @Override
  public void undo() {
    MidiStream.getInstance().shiftBlockLeft(this.block);
  }
}
