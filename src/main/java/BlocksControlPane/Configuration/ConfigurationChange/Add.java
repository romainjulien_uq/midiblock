package blockscontrolpane.configuration.configurationchange;

import blockscontrolpane.MidiStream;
import blockscontrolpane.processingblocks.ProcessingBlock;

/**
 * Represent the addition of a new block to the MidiStream.
 * In order to restore it, we must keep track of the index in the MidiStream
 * where the change happened.
 */
public class Add extends ConfigurationChange {

  // index in MidiStream
  private int indexInStream;

  public Add(ProcessingBlock block, int index) {
    super(block);
    this.indexInStream = index;
  }

  /**
   * To undo this change, the block at indexInStream is removed from the MidiStream.
   */
  @Override
  public void undo() {
    MidiStream.getInstance().removeBlockAtIndex(this.indexInStream);
  }
}
