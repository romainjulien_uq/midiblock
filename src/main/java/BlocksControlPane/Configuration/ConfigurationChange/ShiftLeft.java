package blockscontrolpane.configuration.configurationchange;

import blockscontrolpane.MidiStream;
import blockscontrolpane.processingblocks.ProcessingBlock;

/**
 * Represent the right-shifting of a block in the MidiStream.
 */
public class ShiftLeft extends ConfigurationChange {

  public ShiftLeft(ProcessingBlock block) {
    super(block);
  }

  /**
   * To undo this change, we shift the block left in the MidiStream
   */
  @Override
  public void undo() {
    MidiStream.getInstance().shiftBlockRight(this.block);
  }
}
