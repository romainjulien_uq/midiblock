package blockscontrolpane.processingblocks;

import blockscontrolpane.BlockTypeEnum;
import blockscontrolpane.BlocksConfigurationPane;
import blockscontrolpane.MidiStream;
import java.util.ArrayList;
import java.util.List;

/**
 * The monophonic block allows only one note to be on at once. If a new “note on” signal
 * passes through this block then any other notes should be turned off before the next note begins.
 */
public class MonophonicBlock extends ProcessingBlock {

  // Keep track of the last note on
  private int lastNoteOn;
  // A flag indicating if the last note on has been turned off yet.
  private boolean lastNoteTurnedOff = true;

  /**
   * Init the ProcessingBlockComponent (representation of the block in the UI)
   *
   * @param blocksConfigurationPane
   */
  public MonophonicBlock(BlocksConfigurationPane blocksConfigurationPane) {
    super(blocksConfigurationPane);
    blockUI = new ProcessingBlockComponent(this, "Monophonic");
    type = BlockTypeEnum.Monophonic;
  }


  // ################# ABSTRACT METHODS ###################### //


  /**
   * Turn the last note off, then turn the new note on.
   *
   * @param note
   * @param indexInChain
   */
  @Override
  public void noteOn(int note, int indexInChain) {
    if (!lastNoteTurnedOff) {
      // Turn off the last note off
      MidiStream.getInstance().getNextBlockInStream(indexInChain).noteOff(lastNoteOn, indexInChain + 1);
    }
    // Turn the new note on
    MidiStream.getInstance().getNextBlockInStream(indexInChain).noteOn(note, indexInChain + 1);
    lastNoteOn = note;
    lastNoteTurnedOff = false;
  }

  /**
   * Turn off the last note on, do nothing if it's been turned off already
   *
   * @param note
   * @param indexInChain
   */
  @Override
  public void noteOff(int note, int indexInChain) {
    if (note == lastNoteOn) {
      MidiStream.getInstance().getNextBlockInStream(indexInChain).noteOff(note, indexInChain + 1);
      lastNoteTurnedOff = true;
    }
  }

  @Override
  public List<Object> getParameters() {
    return new ArrayList<>();
  }

  @Override
  public void setParameters(List<Object> params) {
    //
  }
}
