package blockscontrolpane.processingblocks;

import style.Style;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Shape;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.geom.AffineTransform;
import java.awt.geom.Rectangle2D;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import javax.swing.JComponent;

/**
 * The representation of the block in the UI.
 * This class is the entry point of the user-triggered events select, shift and delete block.
 */
public class ProcessingBlockComponent extends JComponent implements MouseListener {

  // Pointer to the corresponding instance of ProcessingBlock
  private ProcessingBlock MIDIBlock;
  // Type of processing block
  private String name;

  private boolean isSelected = false;
  private boolean mouseIn = false;

  // List of parameters (if any) to be displayed on the block
  private List<String> parameters;

  /**
   * Set its size (fixed) and add a mouse listener.
   *
   * @param master    Pointer to the corresponding ProcessingBlock
   * @param blockName Type of block
   */
  public ProcessingBlockComponent(ProcessingBlock master, String blockName) {
    MIDIBlock = master;
    name = blockName;
    parameters = new ArrayList<>();

    this.setPreferredSize(new Dimension(120, 120));
    // Add Mouse listener
    this.addMouseListener(this);
  }

  /**
   * Define the representation of the block in the UI.
   *
   * @param g
   */
  @Override
  public void paint(Graphics g) {
    Graphics2D g2d = (Graphics2D) g;

    // The border of the block, depending on its selection state
    Color orangeColor = isSelected ? Style.COLOR_DARK_ORANGE : Style.COLOR_LIGHT_ORANGE;
    g2d.setColor(orangeColor);
    g2d.fillRect(0, 30, 90, 90);
    // The background of the block
    g2d.setColor(Style.COLOR_DARK_GREY);
    g2d.fillRect(3, 33, 84, 84);

    // Write the type of the block
    g2d.setColor(Style.COLOR_LIGHT_GREY);
    if (name.equals("Select Type")) {
      g2d.setFont(Style.FONT_TEXT_ITALIC);
    } else {
      g2d.setFont(Style.FONT_TEXT_HIGHLIGHT);
    }
    g2d.drawString(name, 10, 50);

    // Write parameters
    g2d.setFont(Style.FONT_TEXT);
    int yOffset = 90;
    for (String param : parameters) {
      g2d.drawString(param, 10, yOffset);
      yOffset += 15;
    }

    // When the mouse hover the block, the delete and shifts buttons are
    // revealed in the top-right corner
    if (mouseIn) {
      // Delete button
      g2d.setColor(Style.COLOR_DARK_GREY);
      g2d.fillOval(75, 0, 25, 25);
      g2d.setColor(orangeColor);
      g2d.fillOval(77, 2, 21, 21);
      g2d.setColor(Style.COLOR_DARK_GREY);

      // Cross inside delete
      Rectangle2D rect = new Rectangle2D.Double(0, 0, 14, 5);
      AffineTransform transform = new AffineTransform();
      transform.translate(84, 6);
      transform.rotate(Math.toRadians(45));
      Shape rotatedRect = transform.createTransformedShape(rect);
      g2d.fill(rotatedRect);

      rect = new Rectangle2D.Double(0, 0, 14, 5);
      transform = new AffineTransform();
      transform.translate(81, 16);
      transform.rotate(Math.toRadians(-45));
      rotatedRect = transform.createTransformedShape(rect);
      g2d.fill(rotatedRect);

      // arrows color
      g2d.setColor(orangeColor);

      // left arrow
      rect = new Rectangle2D.Double(0, 0, 15, 5);
      transform = new AffineTransform();
      transform.translate(60, 12);
      transform.rotate(Math.toRadians(-45));
      rotatedRect = transform.createTransformedShape(rect);
      g2d.fill(rotatedRect);

      rect = new Rectangle2D.Double(0, 0, 10, 5);
      transform = new AffineTransform();
      transform.translate(66, 11);
      transform.rotate(Math.toRadians(45));
      rotatedRect = transform.createTransformedShape(rect);
      g2d.fill(rotatedRect);

      // right arrow
      rect = new Rectangle2D.Double(0, 0, 15, 5);
      transform = new AffineTransform();
      transform.translate(105, 2);
      transform.rotate(Math.toRadians(45));
      rotatedRect = transform.createTransformedShape(rect);
      g2d.fill(rotatedRect);

      rect = new Rectangle2D.Double(0, 0, 10, 5);
      transform = new AffineTransform();
      transform.translate(102, 19);
      transform.rotate(Math.toRadians(-45));
      rotatedRect = transform.createTransformedShape(rect);
      g2d.fill(rotatedRect);
    }
  }


  // ################# GETTER METHODS ###################### //


  public String getBlockName() {
    return name;
  }

  public boolean isSelected() {
    return isSelected;
  }


  // ################# SETTER METHODS ###################### //


  /**
   * A block is set to be selected when:
   * - It is created
   * - The user click on it
   * <p>
   * It is deselected when:
   * - Another block is selected
   *
   * @param flag
   */
  public void setSelected(boolean flag) {
    isSelected = flag;
    repaint();
  }

  /**
   * Add new parameter to be displayed on the block
   *
   * @param newParameters
   */
  public void setParameters(String[] newParameters) {
    parameters = new ArrayList<>(Arrays.asList(newParameters));
  }


  // ################ MOUSE LISTENER INTERFACE #################### //


  @Override
  public void mouseClicked(MouseEvent e) {
    if (e.getX() >= 75 && e.getX() <= 100 && e.getY() >= 0 && e.getY() <= 25) { // Position of delete button
      MIDIBlock.delete();
    } else if (e.getX() >= 101 && e.getX() <= 120 && e.getY() >= 0 && e.getY() <= 25) { // Position of right arrow
      MIDIBlock.shiftRight();
    } else if (e.getX() >= 55 && e.getX() <= 74 && e.getY() >= 0 && e.getY() <= 25) { // Position of left arrow
      MIDIBlock.shiftLeft();
    } else {
      setSelected(true);
      // Update BlockConfiguration panel (display parameters box)
      MIDIBlock.isSelected();
    }
  }

  @Override
  public void mousePressed(MouseEvent e) {

  }

  @Override
  public void mouseReleased(MouseEvent e) {

  }

  @Override
  public void mouseEntered(MouseEvent e) {
    mouseIn = true;
    repaint();
  }

  @Override
  public void mouseExited(MouseEvent e) {
    mouseIn = false;
    repaint();
  }
}
