package blockscontrolpane.processingblocks;

import blockscontrolpane.BlockTypeEnum;
import blockscontrolpane.BlocksConfigurationPane;
import java.awt.Component;
import java.util.List;
import javax.swing.BoxLayout;
import javax.swing.JPanel;

/**
 * A wrapper for all processing blocks. Contains the standard methods and instance
 * variables applicable to all processing block types.
 */
public abstract class ProcessingBlock {

  protected BlockTypeEnum type;
  // The parent in the GUI
  protected BlocksConfigurationPane blockConfiguration;
  // The UI representation of the block
  protected ProcessingBlockComponent blockUI;
  // A panel containing components to set the block parameters (can be empty)
  protected JPanel parametersBox;

  public ProcessingBlock() {
  }

  /**
   * For all processing blocks, create an empty parameters panel and set
   * a pointer to the parent of the block.
   *
   * @param blocksConfigurationPane Parent in GUI
   */
  public ProcessingBlock(BlocksConfigurationPane blocksConfigurationPane) {
    blockConfiguration = blocksConfigurationPane;

    parametersBox = new JPanel();
    BoxLayout l = new BoxLayout(parametersBox, BoxLayout.Y_AXIS);
    parametersBox.setLayout(l);
    parametersBox.setOpaque(false);
    parametersBox.setAlignmentX(Component.LEFT_ALIGNMENT);
  }

  public void delete() {
    blockConfiguration.removeBlock(this);
  }

  public void shiftRight() {
    blockConfiguration.shiftBlockRight(this);
  }

  public void shiftLeft() {
    blockConfiguration.shiftBlockLeft(this);
  }

  public void isSelected() {
    blockConfiguration.blockIsSelected(this);
  }

  public void updateUI() {
    blockConfiguration.getPanel().updateUI();
  }

  // ################# GETTERS METHODS ###################### //

  public ProcessingBlockComponent getBlockComponent() {
    return blockUI;
  }

  public JPanel getParameterPanel() {
    return parametersBox;
  }

  public BlockTypeEnum getType() {
    return type;
  }

  // ################# ABSTRACT METHODS ###################### //

  /**
   * The block has received a new note on message to process.
   * IMPORTANT: It should output the results to the next block in stream.
   *
   * @param note
   * @param indexInChain
   */
  public abstract void noteOn(int note, int indexInChain);

  /**
   * The block has received a new note off message to process.
   * IMPORTANT: It should output the results to the next block in stream.
   *
   * @param note
   * @param indexInChain
   */
  public abstract void noteOff(int note, int indexInChain);

  /**
   * @return The parameters of the block
   */
  public abstract List<Object> getParameters();

  /**
   * Update the parameters of the block
   *
   * @param params
   */
  public abstract void setParameters(List<Object> params);
}
