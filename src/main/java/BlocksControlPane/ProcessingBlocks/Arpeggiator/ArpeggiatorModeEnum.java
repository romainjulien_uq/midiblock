package blockscontrolpane.processingblocks.arpeggiator;

import java.util.Arrays;
import java.util.List;

/**
 * Possible modes of the Arpeggiator block
 */
public enum ArpeggiatorModeEnum {
  Ascending("Ascending"),
  Descending("Descending"),
  PingPong("Ping-Pong"),
  Random("Random");

  private String value;
  private static List<ArpeggiatorModeEnum> allValues = Arrays.asList(ArpeggiatorModeEnum.values());

  /**
   * The constructor of the enum.
   *
   * @param value: The String associated to this enum
   */
  private ArpeggiatorModeEnum(String value) {
    this.value = value;
  }

  /**
   * @return The string representation of the enum
   */
  public String getValue() {
    return this.value;
  }

  /**
   * @return An array containing all possible block types
   */
  public static Object[] getAllValues() {
    return allValues.toArray();
  }

  /**
   * Given the value (String), this method returns the corresponding Enum.
   *
   * @param value: The value of the enum
   * @return The corresponding enum
   */
  public static ArpeggiatorModeEnum getEnumForValue(String value) {
    switch (value) {
      case "Ascending":
        return ArpeggiatorModeEnum.Ascending;
      case "Descending":
        return ArpeggiatorModeEnum.Descending;
      case "Ping-Pong":
        return ArpeggiatorModeEnum.PingPong;
      case "Random":
        return ArpeggiatorModeEnum.Random;
      default:
        return null;
    }
  }
}
