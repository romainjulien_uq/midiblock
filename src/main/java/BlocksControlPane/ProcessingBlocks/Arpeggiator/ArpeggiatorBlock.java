package blockscontrolpane.processingblocks.arpeggiator;

import style.Style;
import tempo.Tempo;
import tempo.tempoevent.BeatEvent;
import tempo.tempoevent.BeatListener;
import blockscontrolpane.BlockTypeEnum;
import blockscontrolpane.BlocksConfigurationPane;
import blockscontrolpane.MidiStream;
import blockscontrolpane.configuration.configurationchange.ChangeParameter;
import blockscontrolpane.processingblocks.ProcessingBlock;
import blockscontrolpane.processingblocks.ProcessingBlockComponent;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.KeyboardFocusManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.swing.JComboBox;

/**
 * An arpeggiator block takes all of the currently ‘on’ notes and outputs a sequence of
 * short notes made up of these notes.
 */
public class ArpeggiatorBlock extends ProcessingBlock implements BeatListener {

  // Since incoming midi messages are not immediately forwarded, we need to keep track of the
  // index of the block in stream
  private int indexInChain;

  // A Combo box for the user to select a mode
  private JComboBox<Object> modeComboBox;
  private ModeComboBoxActionListener listener = new ModeComboBoxActionListener();

  // The mode selected for this Arpeggiator block
  private ArpeggiatorModeEnum mode;

  // A list of notes to be played repeatedly, in the order of which they appear in the list
  private List<Integer> currentSequence;
  // The index of the next note in currentSequence to be played
  private int indexSequence = 0;
  // A temporary sequence, created on incoming midi messages
  private List<Integer> newSequence;
  // Currently on notes
  private Set<Integer> notesOn;
  // Keep track of the previously played note
  private int prevNotePlayed = -1;

  /**
   * Create an Arpeggiator block with default values.
   *
   * @param blocksConfigurationPane
   */
  public ArpeggiatorBlock(BlocksConfigurationPane blocksConfigurationPane) {
    super(blocksConfigurationPane);
    setup();
    // Default mode is Ascending
    this.mode = ArpeggiatorModeEnum.Ascending;
    setParametersUI();
    modeComboBox.addActionListener(listener);
  }

  /**
   * Create an Arpeggiator block with given values.
   *
   * @param blocksConfigurationPane
   */
  public ArpeggiatorBlock(ArpeggiatorModeEnum mode, BlocksConfigurationPane blocksConfigurationPane) {
    super(blocksConfigurationPane);
    setup();
    this.mode = mode;
    setParametersUI();
    modeComboBox.addActionListener(listener);
  }

  /**
   * Creates a ProcessingBlockComponent (representation of the block in the UI),
   * and a combo box to the parameter box.
   */
  private void setup() {
    currentSequence = new ArrayList<>();
    newSequence = new ArrayList<>();
    notesOn = new HashSet<>();

    blockUI = new ProcessingBlockComponent(this, "Arpeggiator");
    type = BlockTypeEnum.Arpeggiator;

    modeComboBox = new JComboBox<>(ArpeggiatorModeEnum.getAllValues());
    modeComboBox.setFont(Style.FONT_TEXT);
    modeComboBox.setForeground(Style.COLOR_GREY);
    modeComboBox.setAlignmentX(Component.LEFT_ALIGNMENT);
    modeComboBox.setPreferredSize(new Dimension(120, modeComboBox.getPreferredSize().height));

    parametersBox.add(modeComboBox);

    // Register to listen for beats
    Tempo.getInstance().addBeatEventListener(this);
  }

  /**
   * Update the UI with the values of this block parameters, both in the parameter box, and
   * on the block representation (blockUI)
   */
  private void setParametersUI() {
    blockUI.setParameters(new String[]{mode.getValue()});
    if ((modeComboBox.getSelectedItem()) != mode) {
      modeComboBox.setSelectedItem(mode);
    }

    updateUI();
  }

  /**
   * This method is called when the user modifies the mode of the block.
   * Updates the block
   */
  private void selectMode() {
    blockConfiguration.parameterChanged(new ChangeParameter(this, getParameters()));

    mode = (ArpeggiatorModeEnum) modeComboBox.getSelectedItem();
    setParametersUI();
    KeyboardFocusManager.getCurrentKeyboardFocusManager().clearGlobalFocusOwner();
  }


  // ################# ABSTRACT METHODS ###################### //

  /**
   * Adds the note to the set of notes on, then update the sequence
   *
   * @param note
   * @param indexInChain
   */
  @Override
  public void noteOn(int note, int indexInChain) {
    this.indexInChain = indexInChain;
    notesOn.add(note);
    createNewSequence();
  }

  /**
   * Remove the note from the set of notes on, then update the sequence
   *
   * @param note
   * @param indexInChain
   */
  @Override
  public void noteOff(int note, int indexInChain) {
    this.indexInChain = indexInChain;
    notesOn.remove(note);
    createNewSequence();
  }

  @Override
  public List<Object> getParameters() {
    List<Object> params = new ArrayList<>();
    params.add(mode);

    return params;
  }

  @Override
  public void setParameters(List<Object> params) {
    mode = (ArpeggiatorModeEnum) params.get(0);
    // Remove listener to avoid creating an unwanted ChangeParameter
    modeComboBox.removeActionListener(listener);
    setParametersUI();
    modeComboBox.addActionListener(listener);
  }


  // ################# CORE PROCESSING METHODS ###################### //

  /**
   * Create a new sequence based on the mode currently selected
   */
  private void createNewSequence() {
    newSequence.addAll(notesOn);

    switch (mode) {
      case Ascending: // Sort notes in ascending order
        Collections.sort(newSequence);
        break;
      case Descending: // Sort notes in descending order
        Collections.sort(newSequence);
        Collections.reverse(newSequence);
        break;
      case PingPong: // Sort notes in ascending order
        Collections.sort(newSequence);
        break;
      case Random:
        break;
    }
  }


  // ################# BEAT LISTENER METHODS ###################### //


  /**
   * Start by turning off note played last beat.
   * If sequence needs to be updated (reached the end, new sequence ready) then this is
   * done first by updating currentSequence and indexSequence.
   * Then we output the note on message currentSequence[indexSequence]
   *
   * @param event
   */
  @Override
  public void beat(BeatEvent event) {
    // Turn previous note off
    if (prevNotePlayed != -1) { // A note was played last beat
      MidiStream.getInstance().getNextBlockInStream(indexInChain).noteOff(
          prevNotePlayed, indexInChain + 1);
    }

    // Nothing to play
    if (notesOn.isEmpty()) {
      prevNotePlayed = -1;
      return;
    }

    // If the current sequence is finished, and a new one is ready, then start new one
    if (indexSequence == currentSequence.size() && !newSequence.isEmpty()) {
      currentSequence.clear();
      currentSequence.addAll(newSequence);
      newSequence.clear();
      indexSequence = 0;
    }

    // If the mode == Random then play the random sequence
    if (mode == ArpeggiatorModeEnum.Random) {
      playRandomNote();
      return;
    }

    // If we are at the end of sequence, start again
    if (indexSequence == currentSequence.size()) {
      indexSequence = 0;

      // If mode == PingPong, reverse sequence THEN start again
      if (mode == ArpeggiatorModeEnum.PingPong) {
        Collections.reverse(currentSequence);
        if (currentSequence.size() > 1) {
          indexSequence = 1;
        }
      }
    }

    prevNotePlayed = currentSequence.get(indexSequence);
    MidiStream.getInstance().getNextBlockInStream(indexInChain).noteOn(prevNotePlayed, indexInChain + 1);

    indexSequence++;
  }

  /**
   * Uses the Collections.shuffle method to reorganize the sequence, then look for
   * the first note in the sequence that is not the previous note played.
   */
  private void playRandomNote() {
    List<Integer> copy = new ArrayList<>(notesOn);
    int index = 0;

    if (notesOn.size() != 1) {
      Collections.shuffle(copy);
      while (copy.get(index) == prevNotePlayed) {
        index++;
      }
    }

    prevNotePlayed = copy.get(index);
    MidiStream.getInstance().getNextBlockInStream(indexInChain).noteOn(prevNotePlayed, indexInChain + 1);
  }


  // ################# PRIVATE LISTENERS  ###################### //


  /**
   * An action listener for the combo box.
   */
  private class ModeComboBoxActionListener implements ActionListener {

    @Override
    public void actionPerformed(ActionEvent e) {
      selectMode();
    }
  }

}
