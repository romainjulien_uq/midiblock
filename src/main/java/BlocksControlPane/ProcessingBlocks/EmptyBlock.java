package blockscontrolpane.processingblocks;

import blockscontrolpane.BlocksConfigurationPane;
import blockscontrolpane.MidiStream;
import java.util.ArrayList;
import java.util.List;

/**
 * An empty clock is a processing block where user hasn't selected a type for it yet.
 * It is ignored when saving configurations and midi messages just pass through
 * without any modifications.
 */
public class EmptyBlock extends ProcessingBlock {

  /**
   * Creates a ProcessingBlockComponent (representation of the block in the UI)
   *
   * @param blocksConfigurationPane
   */
  public EmptyBlock(BlocksConfigurationPane blocksConfigurationPane) {
    super(blocksConfigurationPane);
    blockUI = new ProcessingBlockComponent(this, "Select Type");
    type = null;
  }

  /**
   * Forward message without any modification
   *
   * @param note
   * @param indexInChain
   */
  @Override
  public void noteOn(int note, int indexInChain) {
    MidiStream.getInstance().getNextBlockInStream(indexInChain).noteOn(note, indexInChain + 1);
  }

  /**
   * Forward message without any modification
   *
   * @param note
   * @param indexInChain
   */
  @Override
  public void noteOff(int note, int indexInChain) {
    MidiStream.getInstance().getNextBlockInStream(indexInChain).noteOff(note, indexInChain + 1);
  }

  @Override
  public List<Object> getParameters() {
    return new ArrayList<>();
  }

  @Override
  public void setParameters(List<Object> params) {
    //
  }
}
