package blockscontrolpane.processingblocks.gate;

import java.util.Arrays;
import java.util.List;

/**
 * Possible modes of the Gate block
 */
public enum GateModeEnum {
  Queue("Queue"),
  FirstHold("First Hold"),
  LastHold("Last Hold");

  private String value;
  private static List<GateModeEnum> allValues = Arrays.asList(GateModeEnum.values());

  /**
   * The constructor of the enum.
   *
   * @param value: The String associated to this enum
   */
  private GateModeEnum(String value) {
    this.value = value;
  }

  /**
   * @return The string representation of the enum
   */
  public String getValue() {
    return this.value;
  }

  /**
   * @return An array containing all possible block types
   */
  public static Object[] getAllValues() {
    return allValues.toArray();
  }

  /**
   * Given the value (String), this method returns the corresponding Enum.
   *
   * @param value: The value of the enum
   * @return The corresponding enum
   */
  public static GateModeEnum getEnumForValue(String value) {
    switch (value) {
      case "Queue":
        return GateModeEnum.Queue;
      case "First Hold":
        return GateModeEnum.FirstHold;
      case "Last Hold":
        return GateModeEnum.LastHold;
      default:
        return null;
    }
  }
}
