package blockscontrolpane.processingblocks.gate;

import style.Style;
import tempo.Tempo;
import tempo.tempoevent.BeatEvent;
import tempo.tempoevent.BeatListener;
import blockscontrolpane.BlockTypeEnum;
import blockscontrolpane.BlocksConfigurationPane;
import blockscontrolpane.MidiStream;
import blockscontrolpane.configuration.configurationchange.ChangeParameter;
import blockscontrolpane.processingblocks.ProcessingBlock;
import blockscontrolpane.processingblocks.ProcessingBlockComponent;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.KeyboardFocusManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import javax.swing.Box;
import javax.swing.JComboBox;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

/**
 * The gate block is responsible for ensuring the MIDI stream maintains a constant tempo.
 * The block can be set into one of three modes which determines how new Midi messages are
 * handled while the gate is closed.
 * <p>
 * In every cycle of the global clock the gate open to allow the configured number of notes
 * to pass through the gate. The three gate modes are:
 * <p>
 * Queue ­ Notes are queued within the block and passed in a first­in­first out fashion
 * First Hold ­ Only the first note in the queue is passed on the next tick, other notes are ignored.
 * Last Hold ­ Only the most recent note is passed on the next tick, earlier notes are discarded.
 */
public class GateBlock extends ProcessingBlock implements BeatListener {

  // Since incoming midi messages are not immediately forwarded, we need to keep track of the
  // index of the block in stream
  private int indexInChain;

  // A Combo box for the user to select a mode
  private JComboBox<Object> modeComboBox;
  private ModeComboBoxActionListener listenerComboBox = new ModeComboBoxActionListener();

  // The mode selected for this Gate block
  private GateModeEnum mode;

  // A text field for the user to input the number of notes per tick
  private JTextField notesPerTickTF;
  private NotesPerTickTFActionListener listenerTF = new NotesPerTickTFActionListener();

  // This block will send notesPerTickValue messages at each beat
  private float notesPerTickValue;

  // two values necessary to know when to output midi messages when notesPerTickValue < 1
  private int tickCounter = 0;
  private int tickInterval;

  // Keep track of the notes to be sent when the gate open
  private LinkedList<Integer> queue;
  // A list of notes that should be turned off when the gate open
  private LinkedList<Integer> turnNotesOff;
  // Keep track of notes on which don't have a matching note off yet
  private Set<Integer> outputs;

  /**
   * Create a Gate block with default values.
   *
   * @param blocksConfigurationPane
   */
  public GateBlock(BlocksConfigurationPane blocksConfigurationPane) {
    super(blocksConfigurationPane);
    setup();
    // Default number of notes per tick is 1
    this.notesPerTickValue = (float) 1;
    // Default mode is Queue
    this.mode = GateModeEnum.Queue;
    setParametersUI();
    notesPerTickTF.addActionListener(listenerTF);
    modeComboBox.addActionListener(listenerComboBox);
  }

  /**
   * Create a Gate block with given values.
   *
   * @param mode
   * @param notesPerTickValue
   * @param blocksConfigurationPane
   */
  public GateBlock(GateModeEnum mode, float notesPerTickValue, BlocksConfigurationPane blocksConfigurationPane) {
    super(blocksConfigurationPane);
    setup();
    this.mode = mode;
    this.notesPerTickValue = notesPerTickValue;
    setParametersUI();
    notesPerTickTF.addActionListener(listenerTF);
    modeComboBox.addActionListener(listenerComboBox);
  }

  /**
   * Creates a ProcessingBlockComponent (representation of the block in the UI),
   * and add a text field, and a combo box to the parameter box.
   */
  private void setup() {
    queue = new LinkedList<>();
    turnNotesOff = new LinkedList<>();
    outputs = new HashSet<>();

    blockUI = new ProcessingBlockComponent(this, "Gate");
    type = BlockTypeEnum.Gate;

    // Add the text field
    notesPerTickTF = new JTextField();
    notesPerTickTF.setFont(Style.FONT_TEXT);
    notesPerTickTF.setForeground(Style.COLOR_GREY);
    notesPerTickTF.setAlignmentX(Component.LEFT_ALIGNMENT);

    // Add the combo box
    modeComboBox = new JComboBox<>(GateModeEnum.getAllValues());
    modeComboBox.setFont(Style.FONT_TEXT);
    modeComboBox.setForeground(Style.COLOR_GREY);
    modeComboBox.setAlignmentX(Component.LEFT_ALIGNMENT);
    modeComboBox.setPreferredSize(new Dimension(120, modeComboBox.getPreferredSize().height));

    parametersBox.add(notesPerTickTF);
    parametersBox.add(Box.createVerticalStrut(10));
    parametersBox.add(modeComboBox);

    // Register to listen for beats
    Tempo.getInstance().addBeatEventListener(this);
  }

  /**
   * Update the UI with the values of this block parameters, both in the parameter box, and
   * on the block representation (blockUI)
   */
  private void setParametersUI() {
    blockUI.setParameters(new String[]{mode.getValue(), notesPerTickValue + " notes/tick"});
    if ((modeComboBox.getSelectedItem()) != mode) {
      modeComboBox.setSelectedItem(mode);
    }

    if (!notesPerTickTF.getText().equals(String.valueOf(notesPerTickValue))) {
      notesPerTickTF.setText(String.valueOf(notesPerTickValue));
    }

    updateUI();
  }

  /**
   * This method is called when the user modifies the 'notes per tick' value.
   * A warning is displayed if the input is incorrect, else the block is updated after
   * input value has been rounded.
   */
  private void setNotesPerTickValue() {
    blockConfiguration.parameterChanged(new ChangeParameter(this, getParameters()));

    try {
      String val = notesPerTickTF.getText();
      float input = Float.valueOf(val);
      // Round the input and set notesPerTick value and, if input < 1, reset counter and update tick interval
      roundAndSetNotesPerTick(input);
      setParametersUI();
    } catch (Exception e) {
      JOptionPane.showMessageDialog(null, "Invalid Number");
    }
    KeyboardFocusManager.getCurrentKeyboardFocusManager().clearGlobalFocusOwner();
  }

  /**
   * if input > 1
   * Round the input to the closest integer and set notesPerTickValue
   * else
   * Leave notesPerTickValue as it is, and calculate how many ticks should
   * happen before we open the gate
   *
   * @param input
   */
  private void roundAndSetNotesPerTick(float input) {
    if (input >= 1.0) {
      notesPerTickValue = Math.round(input);
    } else {
      tickCounter = 0;
      tickInterval = Math.round(1 / input);
      notesPerTickValue = input;
    }
  }

  /**
   * This method is called when the user modifies the mode of the block.
   * Updates the block
   */
  private void selectMode() {
    blockConfiguration.parameterChanged(new ChangeParameter(this, getParameters()));

    mode = (GateModeEnum) modeComboBox.getSelectedItem();
    setParametersUI();
    KeyboardFocusManager.getCurrentKeyboardFocusManager().clearGlobalFocusOwner();
  }

  // ################# ABSTRACT METHODS ###################### //

  /**
   * Check the current mode to decide what to do with incoming 'note on' message.
   * <p>
   * Keep tracks of the index of this block in MidiStream
   *
   * @param note
   * @param indexInChain
   */
  @Override
  public void noteOn(int note, int indexInChain) {
    this.indexInChain = indexInChain;

    switch (mode) {
      case Queue: // Add this note to the end of the queue
        queue.add(note);
        break;
      case FirstHold: // Discard this note, unless the queue is empty
        if (queue.isEmpty()) {
          queue.add(note);
        }
        break;
      case LastHold: // Only keep this note in the queue
        queue.clear();
        queue.add(note);
        break;
    }
  }

  /**
   * Check the mode to decide what to do with incoming 'note off' message.
   *
   * @param note
   * @param indexInChain
   */
  @Override
  public void noteOff(int note, int indexInChain) {
    this.indexInChain = indexInChain;

    switch (mode) {
      case Queue:     // If the note is still waiting to be played, it is cancelled out
        if (queue.contains(note)) {
          queue.remove(Integer.valueOf(note));
        } else {    // add note off message to the queue (note that the note number will be checked
          // against the set of outputs to figure if this note should be sent as note on or
          // note off message.
          queue.add(note);
        }
        break;
      default:        // Will be forwarded next time the gate open
        if (outputs.contains(note)) {
          turnNotesOff.add(note);
        }
        break;
    }
  }

  @Override
  public List<Object> getParameters() {
    List<Object> params = new ArrayList<>();
    params.add(mode);
    params.add(notesPerTickValue);

    return params;
  }

  @Override
  public void setParameters(List<Object> params) {
    mode = (GateModeEnum) params.get(0);

    float notesPerTickVal = (float) params.get(1);
    roundAndSetNotesPerTick(notesPerTickVal);

    // Remove listeners to avoid creating an unwanted ChangeParameter
    modeComboBox.removeActionListener(listenerComboBox);
    notesPerTickTF.removeActionListener(listenerTF);
    setParametersUI();
    modeComboBox.addActionListener(listenerComboBox);
    notesPerTickTF.addActionListener(listenerTF);
  }


  // ################# CORE PROCESSING METHODS ###################### //

  /**
   * Send the specified number of midi messages.
   * Keep track of note the notes on messages sent.
   *
   * @param numberNotes
   */
  private void openGate(int numberNotes) {

    while (numberNotes > 0) {
      int note = queue.removeFirst();

      if (outputs.contains(note)) {
        MidiStream.getInstance().getNextBlockInStream(indexInChain).noteOff(note, indexInChain + 1);
        outputs.remove(Integer.valueOf(note));
      } else {
        MidiStream.getInstance().getNextBlockInStream(indexInChain).noteOn(note, indexInChain + 1);
        outputs.add(note);
      }

      numberNotes--;
    }
  }


  // ################# BEAT LISTENER METHODS ###################### //

  /**
   * If mode == queue
   * Check notesPerTickValue or tickInterval to figure if the gate should open and
   * how many messages should be sent
   * Else
   * Send all pending notes off messages
   * Send note on message in queue.
   *
   * @param event
   */
  @Override
  public void beat(BeatEvent event) {
    switch (mode) {
      case Queue:
        if (notesPerTickValue >= 1.0) {
          openGate(Math.min((int) notesPerTickValue, queue.size()));
        } else {
          tickCounter++;
          if (tickCounter == tickInterval) {
            openGate(Math.min(1, queue.size()));
            tickCounter = 0;
          }
        }
        break;
      default:
        while (!turnNotesOff.isEmpty()) {
          int note = turnNotesOff.removeFirst();
          MidiStream.getInstance().getNextBlockInStream(indexInChain).noteOff(note, indexInChain + 1);
          outputs.remove(Integer.valueOf(note));
        }
        if (!queue.isEmpty()) {
          int note = queue.removeFirst();
          MidiStream.getInstance().getNextBlockInStream(indexInChain).noteOn(note, indexInChain + 1);
          outputs.add(note);
        }
    }
  }


  // ################# PRIVATE LISTENERS  ###################### //

  /**
   * An action listener for the combo box.
   */
  private class ModeComboBoxActionListener implements ActionListener {

    @Override
    public void actionPerformed(ActionEvent e) {
      selectMode();
    }
  }

  /**
   * An action listener for the text field.
   */
  private class NotesPerTickTFActionListener implements ActionListener {

    @Override
    public void actionPerformed(ActionEvent e) {
      setNotesPerTickValue();
    }
  }
}
