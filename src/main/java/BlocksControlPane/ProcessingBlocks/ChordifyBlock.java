package blockscontrolpane.processingblocks;

import playcontrolpane.ScaleLoader;
import blockscontrolpane.BlockTypeEnum;
import blockscontrolpane.BlocksConfigurationPane;
import blockscontrolpane.MidiStream;
import java.util.ArrayList;
import java.util.List;
import noteconverter.NoteConverter;

/**
 * The chordify block takes every input note and outputs a chord based on that note.
 * A chord is defined as note (the note played) plus the notes 2, and 4 higher within
 * the globally set scale.
 */
public class ChordifyBlock extends ProcessingBlock {

  /**
   * Init the ProcessingBlockComponent (representation of the block in the UI)
   *
   * @param blocksConfigurationPane
   */
  public ChordifyBlock(BlocksConfigurationPane blocksConfigurationPane) {
    super(blocksConfigurationPane);
    blockUI = new ProcessingBlockComponent(this, "Chordify");
    type = BlockTypeEnum.Chordify;
  }


  // ################# ABSTRACT METHODS ###################### //

  /**
   * Compute the chord, then send all midi notes in the chord as note on messages
   *
   * @param note
   * @param indexInChain
   */
  @Override
  public void noteOn(int note, int indexInChain) {
    int octave = NoteConverter.getOctave(note);
    // Get the midi numbers of the notes in scale with the same octave as the note received
    List<Integer> noteNumbers = NoteConverter.getMidiNumbersForNotes(ScaleLoader.getCurrentScale(), octave);

    List<Integer> chord = getChord(note, noteNumbers);
    for (int noteNumber : chord) {
      MidiStream.getInstance().getNextBlockInStream(indexInChain).noteOn(noteNumber, indexInChain + 1);
    }
  }

  /**
   * Uses the same process as noteOn to make sure the note off message sent will match a note
   * currently on.
   *
   * @param note
   * @param indexInChain
   */
  @Override
  public void noteOff(int note, int indexInChain) {
    int octave = NoteConverter.getOctave(note);
    // Get the midi numbers of the notes in scale with the same octave as the note received
    List<Integer> noteNumbers = NoteConverter.getMidiNumbersForNotes(ScaleLoader.getCurrentScale(), octave);

    List<Integer> chord = getChord(note, noteNumbers);
    for (int noteNumber : chord) {
      MidiStream.getInstance().getNextBlockInStream(indexInChain).noteOff(noteNumber, indexInChain + 1);
    }
  }

  @Override
  public List<Object> getParameters() {
    return new ArrayList<>();
  }

  @Override
  public void setParameters(List<Object> params) {
    //
  }


  // ################# CORE PROCESSING METHODS ###################### //


  /**
   * Compute the chord, ie create a list of notes containing the note received plus the notes 2, and 4 higher
   * within the scale.
   *
   * @param note
   * @param noteNumbers Midi numbers of the notes in scale with the same octave as the note received
   * @return
   */
  private List<Integer> getChord(int note, List<Integer> noteNumbers) {
    List<Integer> chord = new ArrayList<>();
    chord.add(note);

    int index = 0;
    for (int noteNumber : noteNumbers) {
      if (noteNumber == note) {
        break;
      }
      index++;
    }

    // Add the next 4 notes
    noteNumbers.add(noteNumbers.get(0) + 12);
    noteNumbers.add(noteNumbers.get(1) + 12);
    noteNumbers.add(noteNumbers.get(2) + 12);
    noteNumbers.add(noteNumbers.get(3) + 12);

    if (noteNumbers.get(index + 2) <= 107) {
      chord.add(noteNumbers.get(index + 2));
    }

    if (noteNumbers.get(index + 4) <= 107) {
      chord.add(noteNumbers.get(index + 4));
    }

    return chord;
  }
}
