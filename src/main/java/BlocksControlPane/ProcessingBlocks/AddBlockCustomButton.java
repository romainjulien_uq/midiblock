package blockscontrolpane.processingblocks;

import style.Style;
import blockscontrolpane.BlocksConfigurationPane;

import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import javax.swing.JComponent;

/**
 * The custom 'Add block' button is added to the MidiBlock canvas.
 * On click it will add a new block to the stream.
 */
public class AddBlockCustomButton extends JComponent implements MouseListener {

  private boolean isPressed = false;
  // The parent in the GUI
  private BlocksConfigurationPane blockConfiguration;

  /**
   * Create a new 'Add block' button
   *
   * @param blocksConfigurationPane
   */
  public AddBlockCustomButton(BlocksConfigurationPane blocksConfigurationPane) {
    blockConfiguration = blocksConfigurationPane;
    this.setPreferredSize(new Dimension(70, 70));

    // Add Mouse listener
    this.addMouseListener(this);
  }

  /**
   * The representation of the 'Add' button in the UI:
   * A '+' sign in a square box
   *
   * @param g
   */
  @Override
  public void paint(Graphics g) {
    Graphics2D g2d = (Graphics2D) g;
    g2d.setColor(isPressed ? Style.COLOR_LIGHT_ORANGE_TRANSPARENT : Style.COLOR_LIGHT_ORANGE);
    // A bordered square
    g2d.drawRect(0, 0, 70, 70);
    g2d.drawRect(2, 2, 66, 66);

    // Add the + sign in the center
    g2d.setFont(Style.FONT_ADD_BUTTON);
    g2d.drawString("+", 26, 40);
  }

  // ################ MOUSE LISTENER INTERFACE #################### //

  @Override
  public void mouseClicked(MouseEvent e) {

  }

  @Override
  public void mousePressed(MouseEvent e) {
    isPressed = true;
    repaint();
  }

  @Override
  public void mouseReleased(MouseEvent e) {
    if (isPressed) {
      blockConfiguration.addNewBlock();
      isPressed = false;
      repaint();
    }
  }

  @Override
  public void mouseEntered(MouseEvent e) {

  }

  @Override
  public void mouseExited(MouseEvent e) {

  }
}
