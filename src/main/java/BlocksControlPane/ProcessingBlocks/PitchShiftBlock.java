package blockscontrolpane.processingblocks;

import playcontrolpane.ScaleLoader;
import style.Style;
import blockscontrolpane.BlockTypeEnum;
import blockscontrolpane.BlocksConfigurationPane;
import blockscontrolpane.MidiStream;
import blockscontrolpane.configuration.configurationchange.ChangeParameter;
import java.awt.Component;
import java.awt.KeyboardFocusManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import noteconverter.NoteConverter;

/**
 * The pitch shift block shifts a MIDI note up or down by a certain number of notes.
 * The block also constrains all notes to the globally set scale.
 */
public class PitchShiftBlock extends ProcessingBlock {

  // A text field for the user to input a shift value.
  private JTextField shiftTF;
  private ShiftTFActionListener listener = new ShiftTFActionListener();

  // This block will shift incoming midi messages by the shiftValue.
  private int shiftValue;

  /**
   * Create a Pitch Shift block with default values.
   *
   * @param blocksConfigurationPane
   */
  public PitchShiftBlock(BlocksConfigurationPane blocksConfigurationPane) {
    super(blocksConfigurationPane);
    setup();
    // Default value is 0
    this.shiftValue = 0;
    setParametersUI();
    shiftTF.addActionListener(listener);
  }

  /**
   * Create a Pitch Shift block with given values.
   *
   * @param shiftval
   * @param blocksConfigurationPane
   */
  public PitchShiftBlock(int shiftval, BlocksConfigurationPane blocksConfigurationPane) {
    super(blocksConfigurationPane);
    setup();
    this.shiftValue = shiftval;
    setParametersUI();
    shiftTF.addActionListener(listener);
  }

  /**
   * Creates a ProcessingBlockComponent (representation of the block in the UI),
   * and add a text field to the parameter box.
   */
  private void setup() {
    blockUI = new ProcessingBlockComponent(this, "Pitch Shift");
    type = BlockTypeEnum.PitchShift;

    shiftTF = new JTextField();
    shiftTF.setFont(Style.FONT_TEXT);
    shiftTF.setForeground(Style.COLOR_GREY);
    shiftTF.setAlignmentX(Component.LEFT_ALIGNMENT);

    parametersBox.add(shiftTF);
  }

  /**
   * Update the UI with the values of this block parameters, both in the parameter box, and
   * on the block representation (blockUI)
   */
  private void setParametersUI() {
    blockUI.setParameters(new String[]{String.valueOf(shiftValue)});
    if (!shiftTF.getText().equals(String.valueOf(shiftValue))) {
      shiftTF.setText(String.valueOf(shiftValue));
    }

    updateUI();
  }

  /**
   * This method is called when the user modifies the shift value.
   * A warning is displayed if the input is incorrect, else the block is updated.
   */
  private void setShiftValue() {
    blockConfiguration.parameterChanged(new ChangeParameter(this, getParameters()));

    try {
      shiftValue = Integer.parseInt(shiftTF.getText());
      setParametersUI();
    } catch (Exception e) {
      JOptionPane.showMessageDialog(null, "Invalid Number");
    }
    KeyboardFocusManager.getCurrentKeyboardFocusManager().clearGlobalFocusOwner();
  }

  // ################# ABSTRACT METHODS ###################### //

  /**
   * Add the shift value to the note number received, round it and make its in range
   * before sending the new note to the next block in list
   *
   * @param note
   * @param indexInChain
   */
  @Override
  public void noteOn(int note, int indexInChain) {
    int outputNote = getOutput(note + shiftValue);
    MidiStream.getInstance().getNextBlockInStream(indexInChain).noteOn(outputNote, indexInChain + 1);
  }

  /**
   * Uses the same process as noteOn to make sure the note off message sent will match a note
   * currently on.
   *
   * @param note
   * @param indexInChain
   */
  @Override
  public void noteOff(int note, int indexInChain) {
    int outputNote = getOutput(note + shiftValue);
    MidiStream.getInstance().getNextBlockInStream(indexInChain).noteOff(outputNote, indexInChain + 1);
  }

  @Override
  public List<Object> getParameters() {
    List<Object> params = new ArrayList<>();
    params.add(shiftValue);

    return params;
  }

  @Override
  public void setParameters(List<Object> params) {
    shiftValue = (int) params.get(0);
    // Remove listener to avoid creating an unwanted ChangeParameter
    shiftTF.removeActionListener(listener);
    setParametersUI();
    shiftTF.addActionListener(listener);
  }


  // ################# CORE PROCESSING METHODS ###################### //


  /**
   * Takes the shifted note as input. Ensure that the note is in range and round it as necessary.
   *
   * @param input
   * @return
   */
  private int getOutput(int input) {
    int noteInRange = inRange(input);

    int roundedNote = roundNote(noteInRange, ScaleLoader.getCurrentScale());

    return roundedNote;
  }

  /**
   * If the note is out of range, return the boundary values.
   *
   * @param note midi number of a note
   * @return the note itself, or the boundary values 24 or 107
   */
  private int inRange(int note) {
    if (note > 107) {
      return 107;
    } else if (note < 24) {
      return 24;
    }
    return note;
  }

  /**
   * The note is rounded to the closest note in scale
   *
   * @param noteNumber midi number of a note
   * @param notes      Notes in current scale
   * @return The note rounded up
   */
  private int roundNote(int noteNumber, List<String> notes) {

    int octave = NoteConverter.getOctave(noteNumber);
    // Get the midi numbers of the notes in scale with the same octave as the note to round.
    List<Integer> noteNumbers = NoteConverter.getMidiNumbersForNotes(notes, octave);
    if (noteNumbers.contains(noteNumber)) {
      return noteNumber;
    } else {
      return findClosestNote(noteNumber, noteNumbers);
    }
  }

  /**
   * A method to find the closest note in scale.
   *
   * @param noteToRound
   * @param noteNumbers Midi numbers of the notes in scale with the same octave as the noteToRound.
   * @return
   */
  private int findClosestNote(int noteToRound, List<Integer> noteNumbers) {
    int roundedNote = 0;
    int difference = 1000;

    // Add the last note of the previous octave
    int lastNotePrevOctave = noteNumbers.get(noteNumbers.size() - 1) - 12;
    if (lastNotePrevOctave >= 24) {
      noteNumbers.add(lastNotePrevOctave);
    }

    // Add the first note of the next octave
    int firstNoteNextOctave = noteNumbers.get(0) + 12;
    if (firstNoteNextOctave <= 107) {
      noteNumbers.add(firstNoteNextOctave);
    }

    Collections.sort(noteNumbers);

    // Find closest note in list
    for (Integer note : noteNumbers) {
      int newDiff = Math.abs(note - noteToRound);
      if (newDiff < difference) {
        roundedNote = note;
        difference = newDiff;
      }
    }

    return roundedNote;
  }

  // ################# PRIVATE LISTENERS  ###################### //


  /**
   * An action listener for the text field.
   */
  private class ShiftTFActionListener implements ActionListener {

    @Override
    public void actionPerformed(ActionEvent e) {
      setShiftValue();
    }
  }

}
