package blockscontrolpane;

import style.ClickLabel;
import style.Style;
import blockscontrolpane.processingblocks.AddBlockCustomButton;
import blockscontrolpane.processingblocks.ProcessingBlock;
import blockscontrolpane.processingblocks.ProcessingBlockComponent;
import java.awt.Component;
import java.util.List;
import javax.swing.BorderFactory;
import javax.swing.JPanel;
import javax.swing.JScrollBar;
import javax.swing.border.EtchedBorder;

/**
 * A custom JPanel used for displaying processing blocks currently in the MidiStream.
 * It uses absolute positioning to layout the blocks and implement a scrolling functionality.
 */
public class MidiBlocksCanvas extends JPanel {

  // scrollbar appears only when necessary, ie there is more blocks than we can display
  private JScrollBar scrollBar;
  // used for scrolling functionality
  private int prevScroll;

  /**
   * Creates a Jpanel without any layout manager.
   */
  public MidiBlocksCanvas() {
    super();
    setLayout(null);
    setBackground(Style.COLOR_GREY);
    setBorder(BorderFactory.createEtchedBorder(EtchedBorder.LOWERED));
  }

  /**
   * Add custom 'Add block' button to the left of the canvas
   *
   * @param button
   */
  public void addAddButton(AddBlockCustomButton button) {
    button.setBounds(20, 55, button.getPreferredSize().width, button.getPreferredSize().height);
    add(button);
  }

  /**
   * Add 'Undo' button to the top left of the canvas
   *
   * @param undoButton
   */
  public void addUndoButton(ClickLabel undoButton) {
    undoButton.setBounds(20, 10, undoButton.getPreferredSize().width, undoButton.getPreferredSize().height);
    add(undoButton);
  }

  /**
   * Add a processing blocks at coordinates (x, 15)
   *
   * @param block
   * @param x     The x coordinates of where the block should be placed
   */
  public void addProcessingBlock(ProcessingBlockComponent block, int x) {
    block.setBounds(x, 15, block.getPreferredSize().width, block.getPreferredSize().height);
    add(block);
  }

  /**
   * Takes a list of blocks as parameters and drawnthem all in the canvas, revealing the
   * scroll bar if necessary. All blocks are spaced out by 50
   *
   * @param midiStream
   * @return
   */
  public ProcessingBlock refresh(List<ProcessingBlock> midiStream) {
    int x = 0;
    ProcessingBlock currentSelection = null;

    // Add blocks to the stream
    for (ProcessingBlock block : midiStream) {
      x += 150;
      ProcessingBlockComponent blockComponent = block.getBlockComponent();

      if (blockComponent.isSelected()) {
        currentSelection = block;
      }
      addProcessingBlock(blockComponent, x);
    }

    // Check if blocks were added furter than the canvas bound
    if (getSize().width <= x + 150) {
      scrollBar = new JScrollBar(JScrollBar.HORIZONTAL);
      scrollBar.setMinimum(0);
      scrollBar.setMaximum((x + 200) - getSize().width);
      scrollBar.setBlockIncrement(100);
      scrollBar.addAdjustmentListener(e -> scrolling(e.getValue()));

      scrollBar.setBounds(2, getHeight() - scrollBar.getPreferredSize().height - 3,
          getWidth() - 5, scrollBar.getPreferredSize().height);
      add(scrollBar);
      prevScroll = 0;
    }

    return currentSelection;
  }

  // ################# HELPER METHODS ###################### //

  /**
   * Function called when the user scrolls. Reposition all blocks accordingly.
   *
   * @param scrollValue
   */
  private void scrolling(int scrollValue) {
    int diff = scrollValue - prevScroll;
    Component[] list = getComponents();
    for (Component comp : list) {
      if (comp instanceof JScrollBar) {
        continue;
      }
      comp.setLocation((comp.getX() - diff), comp.getY());
    }
    prevScroll = scrollValue;
  }
}
