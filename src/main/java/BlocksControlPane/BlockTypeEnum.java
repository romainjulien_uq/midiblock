package blockscontrolpane;

import java.util.Arrays;
import java.util.List;

/**
 * Possible processing block types.
 */
public enum BlockTypeEnum {
  PitchShift("Pitch Shift"),
  Arpeggiator("Arpeggiator"),
  Monophonic("Monophonic"),
  Chordify("Chordify"),
  Gate("Gate");

  private String value;
  private static List<BlockTypeEnum> allValues = Arrays.asList(BlockTypeEnum.values());

  /**
   * The constructor of the enum.
   *
   * @param value: The String associated to this enum
   */
  private BlockTypeEnum(String value) {
    this.value = value;
  }

  /**
   * @return The string representation of the enum
   */
  public String getValue() {
    return this.value;
  }

  /**
   * @return An array containing all possible block types
   */
  public static Object[] getAllValues() {
    return allValues.toArray();
  }

  /**
   * Given the value (String), this method returns the corresponding Enum.
   *
   * @param value: The value of the enum
   * @return The corresponding enum
   */
  public static BlockTypeEnum getEnumForValue(String value) {
    switch (value) {
      case "Pitch Shift":
        return BlockTypeEnum.PitchShift;
      case "Arpeggiator":
        return BlockTypeEnum.Arpeggiator;
      case "Monophonic":
        return BlockTypeEnum.Monophonic;
      case "Chordify":
        return BlockTypeEnum.Chordify;
      case "Gate":
        return BlockTypeEnum.Gate;
      default:
        return null;
    }
  }
}
