package blockscontrolpane;

import settings.ControlPane;
import style.ClickLabel;
import style.Style;
import blockscontrolpane.configuration.ConfigurationManager;
import blockscontrolpane.configuration.configurationchange.Add;
import blockscontrolpane.configuration.configurationchange.ChangeBlockType;
import blockscontrolpane.configuration.configurationchange.ChangeParameter;
import blockscontrolpane.configuration.configurationchange.ConfigurationChange;
import blockscontrolpane.configuration.configurationchange.Delete;
import blockscontrolpane.configuration.configurationchange.ShiftLeft;
import blockscontrolpane.configuration.configurationchange.ShiftRight;
import blockscontrolpane.processingblocks.AddBlockCustomButton;
import blockscontrolpane.processingblocks.ChordifyBlock;
import blockscontrolpane.processingblocks.EmptyBlock;
import blockscontrolpane.processingblocks.MonophonicBlock;
import blockscontrolpane.processingblocks.PitchShiftBlock;
import blockscontrolpane.processingblocks.ProcessingBlock;
import blockscontrolpane.processingblocks.arpeggiator.ArpeggiatorBlock;
import blockscontrolpane.processingblocks.gate.GateBlock;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.KeyboardFocusManager;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.Stack;
import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSeparator;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.border.Border;
import javax.swing.border.EtchedBorder;

/**
 * The block configuration pane is where the user can:
 * - Create/delete and shifts new processing blocks
 * - Modify block parameters and type
 * - Create, load and save block configurations
 * - Undo last configuration changes
 * <p>
 * The panel implements a hide/show functionality.
 */
public class BlocksConfigurationPane {

  // A pointer to the control pane, to retrieve info about the app current state
  private ControlPane controlPane;

  // The container for this block configuration panel
  private JPanel mainPane;

  // Top row. Contains the buttons to reveal/hide this block configuration panel, and the
  // menu buttons to show create/load/save configuration panels
  private JPanel menuRow;
  private boolean configRowIsShown = false;

  // The main row. The left column contains commands to interact with blocks settings or configurations.
  // The rest of the row is the JPanel containing the MIDI blocks
  private JPanel configRow;

  // A custom JPanel allowing absolute positioning of processing blocks
  private MidiBlocksCanvas blocksCanvas;
  private AddBlockCustomButton addButton;
  private ClickLabel undoButton;

  // Contains one of the blockSettingBox, saveConfigurationBox, and openConfigurationBox at a time.
  private JPanel leftColumn;

  private JPanel blockSettingBox;
  private JComboBox<Object> blockTypeComboBox; // All possible block types

  private JPanel saveConfigurationBox;

  private JPanel openConfigurationBox;
  private JComboBox configurationListComboBox; // All configuration files in root directory

  // Keep track of the changes happening with the current configuration
  private Stack<ConfigurationChange> configurationChangeStack;

  // Keep track of the current selection to display the associated parameter box in the left column
  private ProcessingBlock blockSelected;

  /**
   * - Set the style of the block configuration pane.
   * - Init and layout components of the menu row and the config row.
   * Note: The config row is initially hidden.
   *
   * @param controls Pointer to the control pane
   */
  public BlocksConfigurationPane(ControlPane controls) {
    controlPane = controls;

    mainPane = new JPanel();
    BoxLayout layout = new BoxLayout(mainPane, BoxLayout.Y_AXIS);
    mainPane.setLayout(layout);

    // Both menu and config rows have an etched border
    Border panelBorder = BorderFactory.createEtchedBorder(EtchedBorder.LOWERED);

    // Init menu row
    menuRow = new JPanel(new BorderLayout());
    menuRow.setAlignmentX(Component.LEFT_ALIGNMENT);
    menuRow.setBackground(Style.COLOR_DARK_GREY);
    Border menuRowInsets = BorderFactory.createEmptyBorder(0, 5, 0, 5);
    menuRow.setBorder(BorderFactory.createCompoundBorder(panelBorder, menuRowInsets));

    initMenuRowComponents();

    // Init config row
    configRow = new JPanel(new BorderLayout());
    configRow.setPreferredSize(new Dimension(configRow.getPreferredSize().width, 200));
    configRow.setAlignmentX(Component.LEFT_ALIGNMENT);
    configRow.setBackground(Style.COLOR_DARK_GREY);
    Border configRowInsets = BorderFactory.createEmptyBorder(5, 5, 5, 5);
    configRow.setBorder(BorderFactory.createCompoundBorder(panelBorder, configRowInsets));

    initConfigRowComponents();

    // Only show menu row, config row is initially hidden
    mainPane.add(menuRow);

    // Instanciate a new stack
    configurationChangeStack = new Stack<>();
  }


  // ################# INIT GUI METHODS ###################### //


  /**
   * Init and layout menu row components.
   * All labels here are clickable and reveal the config row. Only the panel title "MIDI blocks" can hide it.
   */
  private void initMenuRowComponents() {
    // "New" button -> clear the current blocks in the config row
    ClickLabel newLabel = new ClickLabel("New", Style.COLOR_LIGHT_GREY, Style.COLOR_WHITE_TRANSPARENT, false);
    newLabel.setFont(Style.FONT_TEXT);
    newLabel.addMouseListener(new MouseAdapter() {
      @Override
      public void mouseClicked(MouseEvent e) {
        if (!configRowIsShown) {
          showConfigRow();
        } else {
          createNewConfiguration();
        }
      }
    });

    // "Open" button -> update the left column -> reveal a list of configurations for the user to choose from
    ClickLabel openLabel = new ClickLabel("Open", Style.COLOR_LIGHT_GREY, Style.COLOR_WHITE_TRANSPARENT, false);
    openLabel.setFont(Style.FONT_TEXT);
    openLabel.addMouseListener(new MouseAdapter() {
      @Override
      public void mouseClicked(MouseEvent e) {
        if (!configRowIsShown) {
          showConfigRow();
        }
        showOpenConfigurationBox();
      }
    });

    // "Save" button -> update the left column -> reveal a text fied for the user to input a name and save the
    // current configuration
    ClickLabel saveLabel = new ClickLabel("Save", Style.COLOR_LIGHT_GREY, Style.COLOR_WHITE_TRANSPARENT, false);
    saveLabel.setFont(Style.FONT_TEXT);
    saveLabel.addMouseListener(new MouseAdapter() {
      @Override
      public void mouseClicked(MouseEvent e) {
        if (!configRowIsShown) {
          showConfigRow();
        }
        showSaveConfigurationBox();
      }
    });

    // Wrap new, open and save buttons together
    JPanel wrapperLeft = new JPanel();
    wrapperLeft.setOpaque(false);

    wrapperLeft.add(newLabel);
    wrapperLeft.add(Box.createHorizontalStrut(10));
    wrapperLeft.add(openLabel);
    wrapperLeft.add(Box.createHorizontalStrut(10));
    wrapperLeft.add(saveLabel);

    // "MIDI Blocks" button -> hide/show the config row
    ClickLabel panelTitle = new ClickLabel("MIDI Blocks", Style.COLOR_DARK_ORANGE, Style.COLOR_DARK_ORANGE_TRANSPARENT, false);
    panelTitle.setFont(Style.FONT_TEXT_HIGHLIGHT);
    panelTitle.addMouseListener(new MouseAdapter() {
      @Override
      public void mouseClicked(MouseEvent e) {
        if (configRowIsShown) {
          hideConfigRow();
        } else {
          showConfigRow();
        }
      }
    });

    menuRow.add(wrapperLeft, BorderLayout.WEST);
    menuRow.add(panelTitle, BorderLayout.EAST);
  }

  /**
   * Init and layout config row components.
   * The config row is divided into two sections:
   * - A command panel on the left-hand side (leftColumn pane), contains one of the 'block setting box',
   * 'save configuration box', or 'load configuration box' at all time (initially block setting box).
   * - The Midi block canvas fills up the rest of the row, it contains a graphical representation of
   * all processing blocks currently in Midi Stream + an 'Add block' button.
   * This is where the user can create, delete and shift blocks.
   */
  private void initConfigRowComponents() {
    // Init the left column and set its style
    leftColumn = new JPanel();
    leftColumn.setPreferredSize(new Dimension(150, leftColumn.getPreferredSize().height));
    leftColumn.setBackground(Style.COLOR_GREY);
    Border raisedBorder = BorderFactory.createEtchedBorder(EtchedBorder.LOWERED);
    Border insets = BorderFactory.createEmptyBorder(5, 5, 5, 5);
    leftColumn.setBorder(BorderFactory.createCompoundBorder(raisedBorder, insets));

    // Init block settings box
    blockSettingBox = new JPanel();
    BoxLayout layoutSettingBox = new BoxLayout(blockSettingBox, BoxLayout.Y_AXIS);
    blockSettingBox.setLayout(layoutSettingBox);
    blockSettingBox.setOpaque(false);

    initBlockSettingBoxComponents();

    // Init save configuration box
    saveConfigurationBox = new JPanel();
    BoxLayout layoutSaveBox = new BoxLayout(saveConfigurationBox, BoxLayout.Y_AXIS);
    saveConfigurationBox.setLayout(layoutSaveBox);
    saveConfigurationBox.setOpaque(false);

    initSaveConfigurationBoxComponents();

    // Init open configuration box
    openConfigurationBox = new JPanel();
    BoxLayout layoutOpenBox = new BoxLayout(openConfigurationBox, BoxLayout.Y_AXIS);
    openConfigurationBox.setLayout(layoutOpenBox);
    openConfigurationBox.setOpaque(false);

    initOpenConfigurationBoxComponents();

    // Initially add the block setting box to the left column
    leftColumn.add(blockSettingBox);


    // Init the MidiBlock canvas and add the 'Add block' and 'Undo' buttons to it
    blocksCanvas = new MidiBlocksCanvas();

    // On click, the add button creates a new 'empty block' (ie has no type) and add it to the stream.
    addButton = new AddBlockCustomButton(this);

    // On click, the undo button restore the last change on the configurationChanges Stack
    undoButton = new ClickLabel("UNDO", Style.COLOR_DARK_ORANGE, Style.COLOR_DARK_ORANGE_TRANSPARENT, true);
    undoButton.setFont(Style.FONT_TEXT_HIGHLIGHT);
    // initially "Disable" the button, enable if stack contains at least one change.
    undoButton.setSelected(true);
    undoButton.addMouseListener(new MouseAdapter() {
      @Override
      public void mouseClicked(MouseEvent e) {
        if (configurationChangeStack.isEmpty()) return;
        configurationChangeStack.pop().undo();
        undoButton.setSelected(configurationChangeStack.isEmpty());
        refreshMIDIBlocksCanvas();
      }
    });

    blocksCanvas.addAddButton(addButton);
    blocksCanvas.addUndoButton(undoButton);

    configRow.add(leftColumn, BorderLayout.WEST);
    configRow.add(blocksCanvas, BorderLayout.CENTER);
  }

  /**
   * Init and layout the 'block setting box' components.
   * This is where the user can modify the block currently selected, ie change its type or parameters.
   * <p>
   * The top part of the box is for selecting the type of the block,
   * The bottom part displays the parameter box of the currently selected block. (Added when a block is selected)
   */
  private void initBlockSettingBoxComponents() {

    JLabel titleBlockType = new JLabel(" Select Block Type");
    titleBlockType.setFont(Style.FONT_TEXT);
    titleBlockType.setForeground(Style.COLOR_LIGHT_GREY);
    titleBlockType.setAlignmentX(Component.LEFT_ALIGNMENT);

    // A list of all possible block types, as defined in BlockTypeEnum class
    blockTypeComboBox = new JComboBox<>(BlockTypeEnum.getAllValues());
    blockTypeComboBox.setFont(Style.FONT_TEXT);
    blockTypeComboBox.setForeground(Style.COLOR_DARK_ORANGE);
    blockTypeComboBox.setAlignmentX(Component.LEFT_ALIGNMENT);
    blockTypeComboBox.setPreferredSize(new Dimension(120, blockTypeComboBox.getPreferredSize().height));
    blockTypeComboBox.setSelectedIndex(-1);
    blockTypeComboBox.addActionListener((e) -> selectBlockType());

    blockSettingBox.add(titleBlockType);
    blockSettingBox.add(Box.createVerticalStrut(5));
    blockSettingBox.add(blockTypeComboBox);
    blockSettingBox.add(Box.createVerticalStrut(20));
    blockSettingBox.add(new JSeparator(SwingConstants.HORIZONTAL));
    blockSettingBox.add(Box.createVerticalStrut(20));
    // Initially hide it (until a block is selected)
    blockSettingBox.setVisible(false);
  }

  /**
   * Init and layout the 'open configuration box' components.
   * This is where the user can select a previously-saved configuration and load it into the Midi Stream.
   */
  private void initOpenConfigurationBoxComponents() {
    JLabel titleSelectConfig = new JLabel(" Select Configuration");
    titleSelectConfig.setFont(Style.FONT_TEXT);
    titleSelectConfig.setForeground(Style.COLOR_LIGHT_GREY);
    titleSelectConfig.setAlignmentX(Component.LEFT_ALIGNMENT);

    configurationListComboBox = new JComboBox<>(ConfigurationManager.getListConfigurations());
    configurationListComboBox.setFont(Style.FONT_TEXT);
    configurationListComboBox.setForeground(Style.COLOR_GREY);
    configurationListComboBox.setAlignmentX(Component.LEFT_ALIGNMENT);
    configurationListComboBox.setPreferredSize(new Dimension(120, blockTypeComboBox.getPreferredSize().height));
    configurationListComboBox.setSelectedIndex(-1);
    configurationListComboBox.addActionListener((e) ->
        loadConfiguration((String) configurationListComboBox.getSelectedItem()));

    openConfigurationBox.add(titleSelectConfig);
    openConfigurationBox.add(Box.createVerticalStrut(5));
    openConfigurationBox.add(configurationListComboBox);
  }

  /**
   * Init and layout the 'save configuration box' components.
   * This is where the user can save the current block configuration and choose a name for it.
   */
  private void initSaveConfigurationBoxComponents() {
    JLabel titleSave = new JLabel(" Configuration name:");
    titleSave.setFont(Style.FONT_TEXT);
    titleSave.setForeground(Style.COLOR_LIGHT_GREY);
    titleSave.setAlignmentX(Component.LEFT_ALIGNMENT);

    JTextField configurationNameTF = new JTextField("");
    configurationNameTF.setFont(Style.FONT_TEXT);
    configurationNameTF.setForeground(Style.COLOR_GREY);
    configurationNameTF.setAlignmentX(Component.LEFT_ALIGNMENT);

    JButton saveButton = new JButton("Save");
    saveButton.setFont(Style.FONT_TEXT_HIGHLIGHT);
    saveButton.setForeground(Style.COLOR_DARK_ORANGE);
    saveButton.setAlignmentX(Component.LEFT_ALIGNMENT);
    saveButton.addActionListener((e) -> {
      if (ConfigurationManager.saveConfiguration(configurationNameTF.getText())) {
        // Update the list of available configurations if the save succeeded.
        configurationListComboBox.addItem(configurationNameTF.getText());
      }
    });

    saveConfigurationBox.add(titleSave);
    saveConfigurationBox.add(Box.createVerticalStrut(5));
    saveConfigurationBox.add(configurationNameTF);
    saveConfigurationBox.add(Box.createVerticalStrut(10));
    saveConfigurationBox.add(saveButton);
  }


  // ################# END INIT GUI METHODS ###################### //

  // ################# BLOCKS CONTROL METHODS #################### //


  /**
   * Add a new EmptyBlock to the stream (no type, no effect on Midi stream) and set it to be
   * the current selection. Update UI.
   * Add the change to the stack.
   */
  public void addNewBlock() {
    ProcessingBlock newBlock = new EmptyBlock(this);
    newBlock.getBlockComponent().setSelected(true);
    int changeIndex = MidiStream.getInstance().addBlock(newBlock);
    configurationChangeStack.add(new Add(newBlock, changeIndex));
    undoButton.setSelected(false);
    refreshMIDIBlocksCanvas();
  }

  /**
   * Remove a block from MidiStream and update UI.
   * Add the change to the stack.
   *
   * @param blockToRemove
   */
  public void removeBlock(ProcessingBlock blockToRemove) {
    int changeIndex = MidiStream.getInstance().removeBlock(blockToRemove);
    configurationChangeStack.add(new Delete(blockToRemove, changeIndex));
    undoButton.setSelected(false);
    refreshMIDIBlocksCanvas();
  }

  /**
   * Shift the block to the right, ie move its position +1 in the MidiStream and update UI.
   * Add the change to the stack.
   * <p>
   * Note: Nothing happen if the block is at the end of the Stream
   *
   * @param blockToShift
   */
  public void shiftBlockRight(ProcessingBlock blockToShift) {
    boolean hasShifted = MidiStream.getInstance().shiftBlockRight(blockToShift);
    if (hasShifted) {
      configurationChangeStack.add(new ShiftRight(blockToShift));
      undoButton.setSelected(false);
      refreshMIDIBlocksCanvas();
    }
  }

  /**
   * Shift the block to the left, ie move its position -1 in the MidiStream and update UI.
   * Add the change to the stack.
   * <p>
   * Note: Nothing happen if the block is at the beginning of the Stream
   *
   * @param blockToShift
   */
  public void shiftBlockLeft(ProcessingBlock blockToShift) {
    boolean hasShifted = MidiStream.getInstance().shiftBlockLeft(blockToShift);
    if (hasShifted) {
      configurationChangeStack.add(new ShiftLeft(blockToShift));
      undoButton.setSelected(false);
      refreshMIDIBlocksCanvas();
    }
  }

  /**
   * Change the type of the currently selected block:
   * - Create a new block of the selected type
   * - In the MidiStream, replace the selected block by the newly created one
   * - Update block canvas & left column
   * - Add the change to the stack
   */
  private void selectBlockType() {
    ProcessingBlock newBlock = null;
    if (blockTypeComboBox.getSelectedIndex() == -1) return;

    // Get the user selection
    BlockTypeEnum blockType = (BlockTypeEnum) blockTypeComboBox.getSelectedItem();

    // Create new block
    switch (blockType) {
      case PitchShift:
        if (blockSelected instanceof PitchShiftBlock) {
          return;
        }
        newBlock = new PitchShiftBlock(this);
        break;
      case Arpeggiator:
        if (blockSelected instanceof ArpeggiatorBlock) {
          return;
        }
        newBlock = new ArpeggiatorBlock(this);
        break;
      case Monophonic:
        if (blockSelected instanceof MonophonicBlock) {
          return;
        }
        newBlock = new MonophonicBlock(this);
        break;
      case Chordify:
        if (blockSelected instanceof ChordifyBlock) {
          return;
        }
        newBlock = new ChordifyBlock(this);
        break;
      case Gate:
        if (blockSelected instanceof GateBlock) {
          return;
        }
        newBlock = new GateBlock(this);
        break;
    }

    // Set the new block to be selected
    newBlock.getBlockComponent().setSelected(true);
    // Replace old block (selected) by the new one
    MidiStream.getInstance().replaceBlock(blockSelected, newBlock);
    configurationChangeStack.add(
        new ChangeBlockType(blockSelected, newBlock));
    undoButton.setSelected(false);
    // Update UI
    refreshMIDIBlocksCanvas();

    KeyboardFocusManager.getCurrentKeyboardFocusManager().clearGlobalFocusOwner();
  }

  /**
   * Receives a notification from the blocks when parameters are changed by the user.
   * Add the change to the stack.
   *
   * @param change
   */
  public void parameterChanged(ChangeParameter change) {
    configurationChangeStack.add(change);
    undoButton.setSelected(false);
  }


  // ################# END BLOCKS CONTROL METHODS #################### //

  // #################### CONFIGURATION METHODS ###################### //


  /**
   * Clear the MidiStream and refresh block canvas.
   * Reinitialize the configuration changes stack.
   */
  private void createNewConfiguration() {
    MidiStream.getInstance().clearBlocks();
    blockSelected = null;

    showSettingBox();
    refreshMIDIBlocksCanvas();

    configurationChangeStack = new Stack<>();
    undoButton.setSelected(configurationChangeStack.isEmpty());
  }

  /**
   * Load the selected configuration to the MidiStream and refresh block canvas.
   * If Midi File is playing, stop the lecture.
   * Reinitialize the configuration changes stack.
   *
   * @param configName
   */
  private void loadConfiguration(String configName) {
    // Load new configuration in MidiStream
    ConfigurationManager.loadConfiguration(configName, this);

    refreshMIDIBlocksCanvas();
    //stop playing midi file
    controlPane.stopMidiFileReading();

    configurationChangeStack = new Stack<>();
    undoButton.setSelected(configurationChangeStack.isEmpty());
  }


  // ################ END CONFIGURATION METHODS ################## //

  // ###################### GUI METHODS ########################## //


  /**
   * Hide the config row, ie remove it from the main panel.
   */
  private void hideConfigRow() {
    configRowIsShown = !configRowIsShown;
    mainPane.remove(configRow);
    mainPane.updateUI();
  }

  /**
   * Show the config row, ie add it to the main panel.
   */
  private void showConfigRow() {
    configRowIsShown = !configRowIsShown;
    mainPane.add(configRow);
    mainPane.updateUI();
  }

  /**
   * Show the 'save configuration box', ie add it to the left column.
   */
  public void showSaveConfigurationBox() {
    leftColumn.removeAll();
    leftColumn.add(saveConfigurationBox);
    leftColumn.updateUI();
  }

  /**
   * Show the 'open configuration box', ie add it to the left column.
   */
  public void showOpenConfigurationBox() {
    leftColumn.removeAll();
    leftColumn.add(openConfigurationBox);
    leftColumn.updateUI();
  }

  /**
   * Show the 'block setting box', ie add it to the left column.
   */
  public void showSettingBox() {
    leftColumn.removeAll();
    leftColumn.add(blockSettingBox);
    leftColumn.updateUI();
  }

  /**
   * A block can be set selected by the user, or automatically upon creation.
   * Display the parameter block of the block in the leftColumn, and deselect previous selection.
   *
   * @param newSelection
   */
  public void blockIsSelected(ProcessingBlock newSelection) {
    // Add the setting box to the left column (will contain the parameter box of the block selected)
    showSettingBox();

    if (blockSelected == newSelection) {
      return;
    }

    if (blockSelected != null) {
      blockSelected.getBlockComponent().setSelected(false);
    }
    blockSelected = newSelection;

    // Add the parameter box of the block selected to the setting box
    refreshSettingBox();
  }

  /**
   * Method called when a new block is selected, or when the block canvas is updated.
   * <p>
   * If a block is currently selected, it displays its type in the blockTypeComboBox,
   * if not, the setting box is hidden.
   * <p>
   * Display the block selected's parameters box. (Empty in the case of an EmptyBlock or if the block type
   * has no parameters)
   */
  public void refreshSettingBox() {
    if (blockSettingBox.getComponentCount() == 7) {
      // Remove the parameter box currently in the setting box.
      blockSettingBox.remove(6);
    }

    if (blockSelected == null) {
      // If no block is selected, hide the box
      blockTypeComboBox.setSelectedIndex(-1);
      blockSettingBox.setVisible(false);
    } else {
      // Refresh the blockTypeComboBox, if the selection is not an empty then display
      // the type of the block in the combo box.
      BlockTypeEnum selectedBlockType = BlockTypeEnum.getEnumForValue(
          blockSelected.getBlockComponent().getBlockName());
      if (selectedBlockType == null) {
        blockTypeComboBox.setSelectedIndex(-1);
      } else {
        blockTypeComboBox.setSelectedItem(selectedBlockType);
      }
      // Get the parameter box of the current selection.
      blockSettingBox.add(blockSelected.getParameterPanel());
      blockSettingBox.setVisible(true);
    }

    blockSettingBox.updateUI();
  }

  /**
   * Clear the bock canvas, then fill it up again with the 'Add block' and 'Undo' buttons, and all blocks
   * currently in the MidiStream.
   * If one block is currently selected, update the left column.
   */
  public void refreshMIDIBlocksCanvas() {
    blocksCanvas.removeAll();
    blocksCanvas.addUndoButton(undoButton);
    blocksCanvas.addAddButton(addButton);

    ProcessingBlock currentSelection = blocksCanvas.refresh(MidiStream.getInstance().getBlocksInStream());

    if (currentSelection != null) {
      blockIsSelected(currentSelection);
    } else {
      blockSelected = null;
      refreshSettingBox();
    }

    blocksCanvas.updateUI();
  }

  // #################### END GUI METHODS ######################### //

  // ################# GETTER METHODS ###################### //

  /**
   * @return The block configuration panel (contains all components of this view)
   */
  public JPanel getPanel() {
    return mainPane;
  }
}
