package blockscontrolpane;

import tempo.Tempo;
import blockscontrolpane.processingblocks.ProcessingBlock;
import java.util.ArrayList;
import java.util.List;
import output.OutputManager;

/**
 * The Midi Stream keeps track, and modify the list of processing blocks
 * added by the user.
 * <p>
 * It is also the entry point of the chain of processing block, and act as a controller
 * for all processing blocks by sending pointers them to the next block in the list.
 * When there is no more blocks, or the list is empty, it points to the OutputManager.
 */
public class MidiStream {

  // A flag to start the tempo when the first note is played
  private boolean isFirstInput = true;

  // The list of blocks in the stream
  private List<ProcessingBlock> blocks;

  // Singleton
  private static MidiStream ourInstance = new MidiStream();

  public static MidiStream getInstance() {
    return ourInstance;
  }

  private MidiStream() {
    blocks = new ArrayList<>();
  }

  /**
   * Add a new block at the end of the list
   *
   * @param newBlock
   * @return The index of the newly added block
   */
  public int addBlock(ProcessingBlock newBlock) {
    blocks.add(newBlock);
    return blocks.size() - 1;
  }

  /**
   * Add a new block at the specified index
   *
   * @param index
   * @param newBlock
   */
  public void addBlock(int index, ProcessingBlock newBlock) {
    blocks.add(index, newBlock);
  }

  /**
   * Remove block from the list.
   *
   * @param blockToRemove
   * @return Index at which the block was removed
   */
  public int removeBlock(ProcessingBlock blockToRemove) {
    int blockIndex = findIndexBlock(blockToRemove);
    blocks.remove(blockIndex);
    return blockIndex;
  }

  /**
   * Remove a block at the specified index.
   *
   * @param index
   */
  public void removeBlockAtIndex(int index) {
    blocks.remove(index);
  }

  /**
   * Replace a block by another in the list
   *
   * @param oldBlock The block to be replaced
   * @param newBlock
   * @return Index at chich the block was replaced
   */
  public int replaceBlock(ProcessingBlock oldBlock, ProcessingBlock newBlock) {
    int oldBlockIndex = findIndexBlock(oldBlock);
    blocks.set(oldBlockIndex, newBlock);
    return oldBlockIndex;
  }

  /**
   * Move the block one position up. Does nothing if the block is at the end of the list.
   *
   * @param blockToShift
   * @return True if the block was shifted, and false otherwise.
   */
  public boolean shiftBlockRight(ProcessingBlock blockToShift) {
    int blockIndex = findIndexBlock(blockToShift);
    if ((blockIndex != -1) && (blockIndex != blocks.size() - 1)) {
      ProcessingBlock save = blocks.remove(blockIndex);
      blocks.add(blockIndex + 1, save);
      return true;
    } else {
      return false;
    }
  }

  /**
   * Move the block one position down. Does nothing if the block is at the beginning of the list.
   *
   * @param blockToShift
   * @return True if the block was shifted, and false otherwise.
   */
  public boolean shiftBlockLeft(ProcessingBlock blockToShift) {
    int blockIndex = findIndexBlock(blockToShift);
    if ((blockIndex != -1) && (blockIndex != 0)) {
      ProcessingBlock save = blocks.remove(blockIndex);
      blocks.add(blockIndex - 1, save);
      return true;
    } else {
      return false;
    }
  }

  /**
   * Clear the list of blocks
   */
  public void clearBlocks() {
    this.blocks.clear();
  }


  // ############## BLOCK CONTROLLER METHODS #################### //


  /**
   * This function is called when the InputEventManager receives a new input from the user
   * and sends a note on message. It forwards the note on message to the first block in the list,
   * or the OutputManager if the list is empty.
   * <p>
   * If this is the first input played by the user, it starts the tempo.
   *
   * @param midiNote
   */
  public void startChainNoteOn(int midiNote) {
    if (isFirstInput) {
      Tempo.getInstance().startTimer();
      isFirstInput = false;
    }
    if (blocks.isEmpty()) {
      OutputManager.getInstance().noteOn(midiNote, 0);
    } else {
      blocks.get(0).noteOn(midiNote, 0);
    }
  }

  /**
   * This function is called when the InputEventManager receives a new input from the user
   * and sends a note on message. It forwards the note on message to the first block in the list,
   * or the OutputManager if the list is empty.
   *
   * @param midiNote
   */
  public void startChainNoteOff(int midiNote) {
    if (blocks.isEmpty()) {
      OutputManager.getInstance().noteOff(midiNote, 0);
    } else {
      blocks.get(0).noteOff(midiNote, 0);
    }
  }

  /**
   * Given the index of a block, it returns a pointer to the next block in the list,
   * or a pointer to the OutputManager if there is none.
   *
   * @param index
   * @return
   */
  public ProcessingBlock getNextBlockInStream(int index) {
    if (index == (blocks.size() - 1)) {
      return OutputManager.getInstance();
    } else {
      return blocks.get(index + 1);
    }
  }


  // ################# SETTER METHODS ###################### //


  /**
   * Replace the list of blocks.
   *
   * @param blocks New list of processing blocks
   */
  public void setBlocks(List<ProcessingBlock> blocks) {
    this.blocks = blocks;
  }

  /**
   * When a new tempo is set by user, we wait for the next input to start it again.
   *
   * @param flag
   */
  public void resetFirstInput(boolean flag) {
    isFirstInput = flag;
  }


  // ################# GETTER METHODS ###################### //


  /**
   * @return The list of processing blocks currently in the stream
   */
  public List<ProcessingBlock> getBlocksInStream() {
    return blocks;
  }


  // ################# HELPER METHODS ###################### //


  /**
   * @param oldBlock
   * @return The index in list of the given block
   */
  private int findIndexBlock(ProcessingBlock oldBlock) {
    int index = 0;
    for (ProcessingBlock block : blocks) {
      if (block == oldBlock) {
        return index;
      }
      index++;
    }
    return -1;
  }
}
