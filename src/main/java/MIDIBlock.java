import blockscontrolpane.configuration.ConfigurationManager;
import inputs.virtualpiano.VirtualPianoConnection;
import output.PlayerTest;
import output.SerialClass;
import playcontrolpane.KeyMapper;
import playcontrolpane.ScaleLoader;
import style.Style;

import java.awt.Dimension;
import java.awt.GraphicsEnvironment;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import javax.swing.ImageIcon;
import javax.swing.JFrame;

public class MIDIBlock {

  private static Dimension effectiveScreenSize = GraphicsEnvironment.getLocalGraphicsEnvironment().
      getMaximumWindowBounds().getSize();

  /**
   * The entry point of the application.
   * The element that encapsulates the entire app is the MIDIBlockContainer.
   */
  public MIDIBlock() {
    JFrame main = new JFrame();
    // Set title
    main.setTitle("MIDI Blocks");
    // Set size of the application window
    main.setSize(getWindowWidth(), getWindowHeight());

    // Retrieve images from resources
    ClassLoader loader = this.getClass().getClassLoader();
    Style.refreshIcon = new ImageIcon(loader.getResource("refresh.png"));
    Style.setRecordButton(loader.getResource("record_button.png"),
        loader.getResource("record_button_transparent.png"));
    Style.setPauseButton(loader.getResource("pause_button.png"),
        loader.getResource("pause_button_transparent.png"));
    Style.setPlayButton(loader.getResource("play_button.png"),
        loader.getResource("play_button_transparent.png"));
    Style.setStopButton(loader.getResource("stop_button.png"),
        loader.getResource("stop_button_transparent.png"));

    // Init classes with static methods
    new KeyMapper();
    new ScaleLoader();
    new VirtualPianoConnection();
    new ConfigurationManager();

    // Initiate the container and it's children views
    new MIDIBlockContainer(main.getContentPane());

    // TODO: Uncomment to test software functionalities ONLY
    new PlayerTest(); // Used as a substitute for the hardware

    // Clean up when window closes
    main.addWindowListener(new WindowAdapter() {
      public void windowClosing(WindowEvent windowEvent) {
        SerialClass.getInstance().close();
        VirtualPianoConnection.close();
        System.exit(0);
      }
    });

    main.setVisible(true);
  }

  public static void main(String[] args) {
    new MIDIBlock();
  }

  /**
   * The size of the application window is set to 1920x1080 unless the
   * effective screen size is smaller than this
   *
   * @return int The window width
   */
  public static int getWindowWidth() {
    return Math.min(1920, effectiveScreenSize.width);
  }

  /**
   * The size of the application window is set to 1920x1080 unless the
   * effective screen size is smaller than this
   *
   * @return int The window height
   */
  public static int getWindowHeight() {
    return Math.min(1080, effectiveScreenSize.height);
  }

}
