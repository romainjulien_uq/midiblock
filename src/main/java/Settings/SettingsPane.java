package settings;

import playcontrolpane.KeyMapper;
import playcontrolpane.PlayPane;
import playcontrolpane.ScaleLoader;
import style.*;
import tempo.*;
import blockscontrolpane.MidiStream;
import inputs.virtualpiano.VirtualPianoConnection;
import java.awt.*;
import java.io.File;
import java.util.List;
import javax.swing.*;
import javax.swing.border.Border;
import javax.swing.border.EtchedBorder;
import javax.swing.border.TitledBorder;
import noteconverter.NoteConverter;
import output.SerialClass;

/** The settings pane consist of four rectangles allowing the user to:
 * - set a scale
 * - set a tempo
 * - connect to the hardware
 * - connect to the VPMK
 *
 */
public class SettingsPane {

  private final Object[] rootNotes = {"A", "A#", "B", "C", "C#", "D", "D#", "E", "F", "F#", "G", "G#"};
  private PlayPane playPane;
  private JPanel settingsPane;
  private JPanel scalePane;
  private JComboBox<Object> scaleComboBox;
  private JComboBox<Object> rootNoteComboBox;
  private String currentScale = null;
  private String currentRootNote = null;

  private JPanel connectionPane;
  private JComboBox<Object> availablePortsComboBox;
  private JLabel serialStatus;

  private JPanel VPMKPane;
  private JComboBox<Object> MIDIDevicesComboBox;
  private JLabel VPMKstatus;

  private JPanel tempoPane;
  private JTextField tempoTextField;

  public SettingsPane(PlayPane playPane) {
    this.playPane = playPane;

    settingsPane = new JPanel();
    GridLayout layout = new GridLayout(2, 2, 20, 20);
    settingsPane.setLayout(layout);
    settingsPane.setBackground(Style.COLOR_GREY);
    Border loweredBorder = BorderFactory.createEtchedBorder(EtchedBorder.LOWERED);
    Border paneInset = BorderFactory.createEmptyBorder(20, 20, 20, 20);
    settingsPane.setBorder(BorderFactory.createCompoundBorder(loweredBorder, paneInset));

    initAndLayoutComponents();
  }

  private void initAndLayoutComponents() {
    Border loweredBorder = BorderFactory.createEtchedBorder(EtchedBorder.LOWERED);
    Border paneInset = BorderFactory.createEmptyBorder(10, 10, 10, 10);

    // Init scale pane
    scalePane = new JPanel();
    BoxLayout layoutScalePane = new BoxLayout(scalePane, BoxLayout.Y_AXIS);
    scalePane.setLayout(layoutScalePane);
    scalePane.setOpaque(false);

    initAndLayoutScalePaneComponents();

    // Init connection Panel
    connectionPane = new JPanel();
    BoxLayout layoutConnectionPane = new BoxLayout(connectionPane, BoxLayout.Y_AXIS);
    connectionPane.setLayout(layoutConnectionPane);
    connectionPane.setOpaque(false);

    initAndLayoutConnectionPaneComponents();

    // init VPMK panel
    VPMKPane = new JPanel();
    BoxLayout layoutVPMKPane = new BoxLayout(VPMKPane, BoxLayout.Y_AXIS);
    VPMKPane.setLayout(layoutVPMKPane);
    VPMKPane.setOpaque(false);

    initAndLayoutVPMKPaneComponents();

    // init tempo panel
    tempoPane = new JPanel();
    BoxLayout layoutTempoPane = new BoxLayout(tempoPane, BoxLayout.Y_AXIS);
    tempoPane.setLayout(layoutTempoPane);
    tempoPane.setOpaque(false);

    initAndLayoutTempoPaneComponents();

    // add 4 panes to the container
    // Wrap scale pane
    FlowLayout layoutScale = new FlowLayout();
    layoutScale.setAlignment(FlowLayout.LEFT);
    JPanel wrapScale = new JPanel(layoutScale);
    Border scaleBorder = BorderFactory.createTitledBorder(loweredBorder,
        "Scale",
        TitledBorder.DEFAULT_JUSTIFICATION,
        TitledBorder.DEFAULT_POSITION,
        Style.FONT_TITLE,
        Style.COLOR_DARK_GREY);
    wrapScale.setBorder(BorderFactory.createCompoundBorder(scaleBorder, paneInset));
    wrapScale.setBackground(Style.COLOR_GREY);

    wrapScale.add(scalePane);
    settingsPane.add(wrapScale);

    // Wrap connection pane
    FlowLayout layoutConnection = new FlowLayout();
    layoutConnection.setAlignment(FlowLayout.LEFT);
    JPanel wrapConnection = new JPanel(layoutConnection);
    Border connectionBorder = BorderFactory.createTitledBorder(loweredBorder,
        "Connection",
        TitledBorder.DEFAULT_JUSTIFICATION,
        TitledBorder.DEFAULT_POSITION,
        Style.FONT_TITLE,
        Style.COLOR_DARK_GREY);
    wrapConnection.setBorder(BorderFactory.createCompoundBorder(connectionBorder, paneInset));
    wrapConnection.setBackground(Style.COLOR_GREY);

    wrapConnection.add(connectionPane);
    settingsPane.add(wrapConnection);

    // Wrap VPMK pane
    FlowLayout layoutVPMK = new FlowLayout();
    layoutVPMK.setAlignment(FlowLayout.LEFT);
    JPanel wrapVPMK = new JPanel(layoutVPMK);
    Border VPMKBorder = BorderFactory.createTitledBorder(loweredBorder,
        "Virtual Piano Keyboard",
        TitledBorder.DEFAULT_JUSTIFICATION,
        TitledBorder.DEFAULT_POSITION,
        Style.FONT_TITLE,
        Style.COLOR_DARK_GREY);
    wrapVPMK.setBorder(BorderFactory.createCompoundBorder(VPMKBorder, paneInset));
    wrapVPMK.setBackground(Style.COLOR_GREY);

    wrapVPMK.add(VPMKPane);
    settingsPane.add(wrapVPMK);

    // Wrap tempo pane
    FlowLayout layoutTempo = new FlowLayout();
    layoutTempo.setAlignment(FlowLayout.LEFT);
    JPanel wrapTempo = new JPanel(layoutTempo);
    Border tempoBorder = BorderFactory.createTitledBorder(loweredBorder,
        "tempo",
        TitledBorder.DEFAULT_JUSTIFICATION,
        TitledBorder.DEFAULT_POSITION,
        Style.FONT_TITLE,
        Style.COLOR_DARK_GREY);
    wrapTempo.setBorder(BorderFactory.createCompoundBorder(tempoBorder, paneInset));
    wrapTempo.setBackground(Style.COLOR_GREY);

    wrapTempo.add(tempoPane);
    settingsPane.add(wrapTempo);
  }

  private void initAndLayoutScalePaneComponents() {
    JButton loadScales = new JButton("Load Scales");
    loadScales.setFont(Style.FONT_TEXT_HIGHLIGHT);
    loadScales.setForeground(Style.COLOR_DARK_ORANGE);
    loadScales.addActionListener((e) -> chooseScaleFile());

    JLabel titleScale = new JLabel(" Select Scale");
    titleScale.setFont(Style.FONT_TEXT);
    titleScale.setForeground(Style.COLOR_LIGHT_GREY);
    titleScale.setAlignmentX(Component.LEFT_ALIGNMENT);

    scaleComboBox = new JComboBox<>();
    scaleComboBox.setFont(Style.FONT_TEXT);
    scaleComboBox.setForeground(Style.COLOR_DARK_ORANGE);
    scaleComboBox.setAlignmentX(Component.LEFT_ALIGNMENT);
    scaleComboBox.setPreferredSize(new Dimension(200, scaleComboBox.getPreferredSize().height));
    scaleComboBox.addActionListener((e) -> updateKeyboard());

    JLabel titleRootNote = new JLabel(" Select Root Note");
    titleRootNote.setFont(Style.FONT_TEXT);
    titleRootNote.setForeground(Style.COLOR_LIGHT_GREY);
    titleRootNote.setAlignmentX(Component.LEFT_ALIGNMENT);

    rootNoteComboBox = new JComboBox<>();
    rootNoteComboBox.setFont(Style.FONT_TEXT);
    rootNoteComboBox.setForeground(Style.COLOR_DARK_ORANGE);
    rootNoteComboBox.setAlignmentX(Component.LEFT_ALIGNMENT);
    rootNoteComboBox.setPreferredSize(new Dimension(200, rootNoteComboBox.getPreferredSize().height));
    rootNoteComboBox.addActionListener((e) -> updateKeyboard());

    scalePane.add(loadScales);
    scalePane.add(Box.createVerticalStrut(10));
    scalePane.add(titleScale);
    scalePane.add(Box.createVerticalStrut(5));
    scalePane.add(scaleComboBox);
    scalePane.add(Box.createVerticalStrut(10));
    scalePane.add(titleRootNote);
    scalePane.add(Box.createVerticalStrut(5));
    scalePane.add(rootNoteComboBox);
  }

  private void initAndLayoutConnectionPaneComponents() {
    // Title for the connection section
    JLabel sectionTitle = new JLabel(" Select USB port");
    sectionTitle.setFont(Style.FONT_TEXT);
    sectionTitle.setForeground(Style.COLOR_LIGHT_GREY);
    sectionTitle.setAlignmentX(Component.LEFT_ALIGNMENT);

    // The combo box for the available ports
    availablePortsComboBox = new JComboBox<>(SerialClass.getInstance().getPortsArray());
    availablePortsComboBox.setFont(Style.FONT_TEXT);
    availablePortsComboBox.setForeground(Style.COLOR_DARK_ORANGE);
    availablePortsComboBox.setAlignmentX(Component.LEFT_ALIGNMENT);
    availablePortsComboBox.setPreferredSize(new Dimension(200, availablePortsComboBox.getPreferredSize().height));
    availablePortsComboBox.setSelectedIndex(-1);

    // The refresh button
    JButton refreshButton = new JButton(Style.refreshIcon);
    refreshButton.setFont(Style.FONT_TEXT_HIGHLIGHT);
    refreshButton.setForeground(Style.COLOR_LIGHT_GREY);
    refreshButton.setAlignmentX(Component.LEFT_ALIGNMENT);
    refreshButton.addActionListener((e) -> refreshPortsList());

    JPanel horizontalWrap = new JPanel(new FlowLayout(FlowLayout.LEADING, 0, 0));
    horizontalWrap.setAlignmentX(FlowLayout.LEFT);
    horizontalWrap.setOpaque(false);
    horizontalWrap.add(availablePortsComboBox);
    horizontalWrap.add(refreshButton);

    // The connect button
    JButton connectButton = new JButton("Connect");
    connectButton.setFont(Style.FONT_TEXT_HIGHLIGHT);
    connectButton.setForeground(Style.COLOR_DARK_ORANGE);
    connectButton.setAlignmentX(Component.LEFT_ALIGNMENT);
    connectButton.addActionListener((e) -> connect());

    // Title for the connection section
    serialStatus = new JLabel("");
    serialStatus.setFont(Style.FONT_TEXT);
    serialStatus.setForeground(Style.COLOR_LIGHT_ORANGE);
    serialStatus.setAlignmentX(Component.LEFT_ALIGNMENT);

    // Add components to the Panel and space them out
    connectionPane.add(sectionTitle);
    connectionPane.add(Box.createVerticalStrut(5));
    connectionPane.add(horizontalWrap);
    connectionPane.add(Box.createVerticalStrut(5));
    connectionPane.add(connectButton);
    connectionPane.add(Box.createVerticalStrut(10));
    connectionPane.add(serialStatus);
  }

  private void initAndLayoutVPMKPaneComponents() {
    JLabel titleMIDIDevice = new JLabel(" Select Device");
    titleMIDIDevice.setFont(Style.FONT_TEXT);
    titleMIDIDevice.setForeground(Style.COLOR_LIGHT_GREY);
    titleMIDIDevice.setAlignmentX(Component.LEFT_ALIGNMENT);

    MIDIDevicesComboBox = new JComboBox<>(VirtualPianoConnection.getAvailableDevices());
    MIDIDevicesComboBox.setFont(Style.FONT_TEXT);
    MIDIDevicesComboBox.setForeground(Style.COLOR_DARK_ORANGE);
    MIDIDevicesComboBox.setAlignmentX(Component.LEFT_ALIGNMENT);
    MIDIDevicesComboBox.setPreferredSize(new Dimension(200, MIDIDevicesComboBox.getPreferredSize().height));
    MIDIDevicesComboBox.setSelectedIndex(-1);

    // The refresh button
    JButton refreshButton = new JButton(Style.refreshIcon);
    refreshButton.setFont(Style.FONT_TEXT_HIGHLIGHT);
    refreshButton.setForeground(Style.COLOR_LIGHT_GREY);
    refreshButton.setAlignmentX(Component.LEFT_ALIGNMENT);
    refreshButton.addActionListener((e) -> refreshMIDIDevices());

    JPanel horizontalWrap = new JPanel(new FlowLayout(FlowLayout.LEADING, 0, 0));
    horizontalWrap.setAlignmentX(FlowLayout.LEFT);
    horizontalWrap.setOpaque(false);
    horizontalWrap.add(MIDIDevicesComboBox);
    horizontalWrap.add(refreshButton);

    // The connect button
    JButton connectButton = new JButton("Connect");
    connectButton.setFont(Style.FONT_TEXT_HIGHLIGHT);
    connectButton.setForeground(Style.COLOR_DARK_ORANGE);
    connectButton.setAlignmentX(Component.LEFT_ALIGNMENT);
    connectButton.addActionListener((e) -> selectDevice());

    VPMKstatus = new JLabel("");
    VPMKstatus.setFont(Style.FONT_TEXT);
    VPMKstatus.setForeground(Style.COLOR_LIGHT_ORANGE);
    VPMKstatus.setAlignmentX(Component.LEFT_ALIGNMENT);

    VPMKPane.add(titleMIDIDevice);
    VPMKPane.add(Box.createVerticalStrut(5));
    VPMKPane.add(horizontalWrap);
    VPMKPane.add(Box.createVerticalStrut(5));
    VPMKPane.add(connectButton);
    VPMKPane.add(Box.createVerticalStrut(10));
    VPMKPane.add(VPMKstatus);
  }

  private void initAndLayoutTempoPaneComponents() {
    JLabel titleTempo = new JLabel(" Enter Tempo (beats/seconds)");
    titleTempo.setFont(Style.FONT_TEXT);
    titleTempo.setForeground(Style.COLOR_LIGHT_GREY);
    titleTempo.setAlignmentX(Component.LEFT_ALIGNMENT);

    tempoTextField = new JTextField("120");
    tempoTextField.setFont(Style.FONT_TEXT);
    tempoTextField.setForeground(Style.COLOR_GREY);
    tempoTextField.setAlignmentX(Component.LEFT_ALIGNMENT);
    tempoTextField.addActionListener(e -> setTempo());

    tempoPane.add(titleTempo);
    tempoPane.add(Box.createVerticalStrut(5));
    tempoPane.add(tempoTextField);
    tempoPane.add(Box.createVerticalStrut(10));

  }

  private void chooseScaleFile() {
    JFileChooser fileChooser = new JFileChooser();
    fileChooser.showOpenDialog(null);
    File scales = fileChooser.getSelectedFile();
    // Do nothing if a wasn't selected
    if (scales == null) {
      return;
    }
    if (ScaleLoader.readScaleFile(scales) != 0) {
      JOptionPane.showMessageDialog(scalePane, "Invalid File Format");
    } else {
      for (Object item : ScaleLoader.getArrayScalesName()) {
        scaleComboBox.addItem(item);
      }
      scaleComboBox.setSelectedIndex(-1);

      for (Object item : rootNotes) {
        rootNoteComboBox.addItem(item);
      }
      rootNoteComboBox.setSelectedIndex(-1);
    }
  }

  private void updateKeyboard() {
    currentScale = (String) scaleComboBox.getSelectedItem();
    currentRootNote = (String) rootNoteComboBox.getSelectedItem();

    if (currentScale != null && currentRootNote != null) {

      ScaleLoader.setNewScale(currentScale, currentRootNote);
      KeyMapper.loadNewScale(ScaleLoader.getCurrentScale(), ScaleLoader.getRootNote());

      playPane.getKeyboard().refreshKeyboard();
      playPane.showKeyboard();
      KeyboardFocusManager.getCurrentKeyboardFocusManager().clearGlobalFocusOwner();

      List<Integer> firstEightNotes = NoteConverter.getFirstEightNotes(
          ScaleLoader.getCurrentScale(), KeyMapper.getRootNoteOctave());
      String notes = "";
      for (Integer note : firstEightNotes) {
        notes += ":" + note;
      }
      SerialClass.getInstance().writeData("conf:scale" + notes);
    }
  }

  private void refreshPortsList() {
    Object[] listPortsString = SerialClass.getInstance().getPortsArray();
    availablePortsComboBox.removeAllItems();
    for (Object port : listPortsString) {
      availablePortsComboBox.addItem(port);
    }
    availablePortsComboBox.setSelectedIndex(-1);
  }

  private void connect() {
    Object selected = availablePortsComboBox.getSelectedItem();
    String port = (String) selected;
    if (SerialClass.getInstance().openPort(port)) {
      serialStatus.setText("Port " + port + " is opened");
    } else {
      serialStatus.setText("Can't open " + port);
    }
  }

  private void selectDevice() {
    if (MIDIDevicesComboBox.getSelectedIndex() >= 0) {
      if (VirtualPianoConnection.openDeviceAtIndex(MIDIDevicesComboBox.getSelectedIndex())) {
        VPMKstatus.setText(MIDIDevicesComboBox.getSelectedItem() + " is connected");
      } else {
        VPMKstatus.setText("Can't open " + MIDIDevicesComboBox.getSelectedItem());
      }
    }
  }

  private void refreshMIDIDevices() {
    MIDIDevicesComboBox.removeAllItems();

    for (Object item : VirtualPianoConnection.getAvailableDevices()) {
      MIDIDevicesComboBox.addItem(item);
    }
    MIDIDevicesComboBox.setSelectedIndex(-1);
  }

  private void setTempo() {
    String val = tempoTextField.getText();
    try {
      int newTempo = Integer.parseInt(val);
      Tempo.getInstance().setTempo(newTempo);
      MidiStream.getInstance().resetFirstInput(true);
    } catch (Exception e) {
      JOptionPane.showMessageDialog(null, "Invalid Number");
    }
    KeyboardFocusManager.getCurrentKeyboardFocusManager().clearGlobalFocusOwner();
  }

  /**
   * @return The input panel (contains all components of this view)
   */
  public JPanel getPanel() {
    return settingsPane;
  }
}
