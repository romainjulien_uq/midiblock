package settings;

import inputs.MidiFileReader;
import output.OutputManager;
import inputs.InputEventManager;
import inputs.InputTypeEnum;
import inputs.virtualpiano.VirtualPianoConnection;
import output.MidiFileManager;
import output.SerialClass;
import playcontrolpane.ScaleLoader;
import style.*;
import tempo.Metronome;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;

/**
 * The control pane allow user to select source of input and outputs.
 * It also contain, at the bottom, the MetronomePanel (visual metronome) and a button to mute
 * the metronome.
 */
public class ControlPane {

    private JPanel sidePane;

    private JPanel inputBox;
    private JComboBox<Object> inputSourceComboBox;
    private JLabel inputStatus;

    private JPanel MIDIFileBox;
    private MidiFileReader midiFileReader;
    private ClickImage playButton;
    private ClickImage pauseRecordingButton;

    private JPanel outputBox;
    private JLabel outputStatus;

    private JPanel recordingBox;
    private JTextField midiFileName;
    private JPanel recordingButtonBox;
    private ClickImage startRecordingButton;
    private ClickImage stopButton;

    private Metronome metronome;


    public ControlPane() {
        sidePane = new JPanel();
        BoxLayout l = new BoxLayout(sidePane, BoxLayout.Y_AXIS);
        sidePane.setLayout(l);
        sidePane.setBackground(Style.COLOR_DARK_GREY);
        sidePane.setBorder(BorderFactory.createEmptyBorder(10, 20, 10, 20));

        initControlPaneComponent();
    }


    private void initControlPaneComponent() {
        inputBox = new JPanel();
        BoxLayout inputLayout = new BoxLayout(inputBox, BoxLayout.Y_AXIS);
        inputBox.setLayout(inputLayout);
        inputBox.setOpaque(false);

        initAndLayoutInputPaneComponents();

        MIDIFileBox = new JPanel();
        BoxLayout fileLayout = new BoxLayout(MIDIFileBox, BoxLayout.Y_AXIS);
        MIDIFileBox.setLayout(fileLayout);
        MIDIFileBox.setOpaque(false);

        initAndLayoutMIDIFilePaneComponents();

        outputBox = new JPanel();
        BoxLayout outputLayout = new BoxLayout(outputBox, BoxLayout.Y_AXIS);
        outputBox.setLayout(outputLayout);
        outputBox.setOpaque(false);

        initAndLayoutOutputPaneComponents();

        recordingBox = new JPanel();
        BoxLayout recordingLayout = new BoxLayout(recordingBox, BoxLayout.Y_AXIS);
        recordingBox.setLayout(recordingLayout);
        recordingBox.setOpaque(false);

        initAndLayoutRecordingPaneComponents();

        metronome = new Metronome();
        ClickLabel undoButton = new ClickLabel("MUTE", Style.COLOR_DARK_ORANGE, Style.COLOR_DARK_ORANGE_TRANSPARENT, false);
        undoButton.setFont(Style.FONT_TEXT_HIGHLIGHT);

        // Initially mute (ie, selected)
        undoButton.setSelected(metronome.getMute());
        undoButton.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                metronome.setMute(!metronome.getMute());
                undoButton.setSelected(metronome.getMute());
            }
        });

        sidePane.add(inputBox);
        sidePane.add(Box.createVerticalStrut(10));
        sidePane.add(new JSeparator(SwingConstants.HORIZONTAL));
        sidePane.add(Box.createVerticalStrut(10));
        sidePane.add(outputBox);
        sidePane.add(Box.createVerticalStrut(10));
        sidePane.add(new JSeparator(SwingConstants.HORIZONTAL));
        sidePane.add(Box.createVerticalStrut(20));
        sidePane.add(undoButton);
        sidePane.add(Box.createVerticalStrut(10));
        sidePane.add(metronome.getPanel());


    }

    private void initAndLayoutInputPaneComponents() {
        JLabel titleInputs = new JLabel(" Select input source");
        titleInputs.setFont(Style.FONT_TEXT_HIGHLIGHT);
        titleInputs.setForeground(Style.COLOR_LIGHT_GREY);
        titleInputs.setAlignmentX(Component.LEFT_ALIGNMENT);

        inputSourceComboBox = new JComboBox<>(InputTypeEnum.getAllValues());
        inputSourceComboBox.setFont(Style.FONT_TEXT);
        inputSourceComboBox.setForeground(Style.COLOR_DARK_ORANGE);
        inputSourceComboBox.setAlignmentX(Component.LEFT_ALIGNMENT);
        inputSourceComboBox.setSelectedIndex(-1);
        inputSourceComboBox.addActionListener((e) -> selectInputSource());

        inputStatus = new JLabel("");
        inputStatus.setFont(Style.FONT_TEXT);
        inputStatus.setForeground(Style.COLOR_LIGHT_ORANGE);
        inputStatus.setAlignmentX(Component.LEFT_ALIGNMENT);

        inputBox.add(titleInputs);
        inputBox.add(Box.createVerticalStrut(10));
        inputBox.add(inputSourceComboBox);
        inputBox.add(Box.createVerticalStrut(10));
        inputBox.add(inputStatus);
        inputBox.add(Box.createVerticalStrut(10));
    }

    /**
     * - Initiates and layout MIDI file panel components
     */
    private void initAndLayoutMIDIFilePaneComponents() {
        JButton loadMidiFile = new JButton("Choose File");
        loadMidiFile.setFont(Style.FONT_TEXT_HIGHLIGHT);
        loadMidiFile.setForeground(Style.COLOR_DARK_ORANGE);
        loadMidiFile.setAlignmentX(Component.LEFT_ALIGNMENT);
        loadMidiFile.addActionListener((e) -> chooseMidiFile());

        playButton = Style.playButton;
        playButton.setSelected(true);
        playButton.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                if (playButton.isSelected()) return;

                if (!OutputManager.getInstance().hasSelectedOutput()) {
                    JOptionPane.showMessageDialog(null, "Select at least one output");
                    return;
                }

                if (OutputManager.getInstance().getOutputToFile()) {
                    if (!startRecording()) {
                        return;
                    }
                }
                midiFileReader.startPlaying();
                stopButton.setSelected(false);
                playButton.setSelected(true);
            }
        });

        stopButton = Style.stopButton;
        stopButton.setSelected(true);
        stopButton.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                if (stopButton.isSelected()) return;

                midiFileReader.stopPlaying();
                playButton.setSelected(false);
                stopButton.setSelected(true);
            }
        });

        JPanel buttonsWrapper = new JPanel();
        BoxLayout recordingLayout = new BoxLayout(buttonsWrapper, BoxLayout.X_AXIS);
        buttonsWrapper.setLayout(recordingLayout);
        buttonsWrapper.setOpaque(false);
        buttonsWrapper.setAlignmentX(Component.LEFT_ALIGNMENT);

        buttonsWrapper.add(playButton);
        buttonsWrapper.add(Box.createHorizontalStrut(20));
        buttonsWrapper.add(stopButton);

        MIDIFileBox.add(loadMidiFile);
        MIDIFileBox.add(Box.createVerticalStrut(10));
        MIDIFileBox.add(buttonsWrapper);
    }

    private void initAndLayoutOutputPaneComponents() {
        JLabel titleOutputs = new JLabel(" Select outputs");
        titleOutputs.setFont(Style.FONT_TEXT_HIGHLIGHT);
        titleOutputs.setForeground(Style.COLOR_LIGHT_GREY);
        titleOutputs.setAlignmentX(Component.LEFT_ALIGNMENT);

        JCheckBox outputHardware = new JCheckBox("Synthesizer");
        outputHardware.setFont(Style.FONT_TEXT_HIGHLIGHT);
        outputHardware.setForeground(Style.COLOR_LIGHT_GREY);
        outputHardware.setOpaque(false);
        outputHardware.setAlignmentX(Component.LEFT_ALIGNMENT);
        outputHardware.addActionListener((e) -> outputToHardware(e));

        outputStatus = new JLabel("");
        outputStatus.setFont(Style.FONT_TEXT);
        outputStatus.setForeground(Style.COLOR_LIGHT_ORANGE);
        outputStatus.setAlignmentX(Component.LEFT_ALIGNMENT);

        JCheckBox outputFile = new JCheckBox("MIDI File");
        outputFile.setFont(Style.FONT_TEXT_HIGHLIGHT);
        outputFile.setForeground(Style.COLOR_LIGHT_GREY);
        outputFile.setOpaque(false);
        outputFile.setAlignmentX(Component.LEFT_ALIGNMENT);
        outputFile.addActionListener((e) -> outputToFile(e));

        outputBox.add(titleOutputs);
        outputBox.add(Box.createVerticalStrut(10));
        outputBox.add(outputHardware);
        outputBox.add(Box.createVerticalStrut(5));
        outputBox.add(Box.createVerticalStrut(5));
        outputBox.add(outputFile);
        outputBox.add(Box.createVerticalStrut(10));
    }

    /**
     * - Initiates and layout Recording panel components
     */
    private void initAndLayoutRecordingPaneComponents() {
        JLabel titleRecord = new JLabel(" Save to file:");
        titleRecord.setFont(Style.FONT_TEXT);
        titleRecord.setForeground(Style.COLOR_LIGHT_ORANGE);
        titleRecord.setAlignmentX(Component.LEFT_ALIGNMENT);

        midiFileName = new JTextField(".mid");
        midiFileName.setFont(Style.FONT_TEXT);
        midiFileName.setForeground(Style.COLOR_LIGHT_ORANGE);
        midiFileName.setAlignmentX(Component.LEFT_ALIGNMENT);

        startRecordingButton = Style.recordButton;
        startRecordingButton.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                if (startRecordingButton.isSelected()) return;
                startRecording();
            }
        });

        pauseRecordingButton = Style.pauseButton;
        pauseRecordingButton.setSelected(true);
        pauseRecordingButton.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                if (pauseRecordingButton.isSelected()) return;
                pauseRecording();
            }
        });

        recordingButtonBox = new JPanel();
        BoxLayout recordingLayout = new BoxLayout(recordingButtonBox, BoxLayout.X_AXIS);
        recordingButtonBox.setLayout(recordingLayout);
        recordingButtonBox.setOpaque(false);
        recordingButtonBox.setAlignmentX(Component.LEFT_ALIGNMENT);

        recordingButtonBox.add(startRecordingButton);
        recordingButtonBox.add(Box.createHorizontalStrut(20));
        recordingButtonBox.add(pauseRecordingButton);

        recordingBox.add(titleRecord);
        recordingBox.add(Box.createVerticalStrut(5));
        recordingBox.add(midiFileName);
        recordingBox.add(Box.createVerticalStrut(10));
    }

    private void selectInputSource() {
        if (inputSourceComboBox.getSelectedIndex() < 0) return;

        InputTypeEnum sourceSelected = (InputTypeEnum) inputSourceComboBox.getSelectedItem();
        InputEventManager.getInstance().setSource(sourceSelected);

        inputBox.remove(MIDIFileBox);

        switch (sourceSelected) {
            case Custom:
                inputStatus.setText(" Scale: " + ScaleLoader.getCurrentScale().toString());
                break;
            case Virtual:
                inputStatus.setText(VirtualPianoConnection.getStatus());
                break;
            case MidiFile:
                inputStatus.setText(" File selected: ");
                inputBox.add(MIDIFileBox);
                break;
        }

        if (OutputManager.getInstance().getOutputToFile()) {
            if (sourceSelected == InputTypeEnum.MidiFile) {
                recordingBox.remove(recordingButtonBox);
            } else {
                recordingBox.add(recordingButtonBox);
            }
        }

        KeyboardFocusManager.getCurrentKeyboardFocusManager().clearGlobalFocusOwner();
    }

    private void chooseMidiFile() {
        JFileChooser fileChooser = new JFileChooser();
        fileChooser.showOpenDialog(null);
        File midiFile = fileChooser.getSelectedFile();
        // Do nothing if a wasn't selected
        if (midiFile == null) {
            return;
        }
        midiFileReader = new MidiFileReader(midiFile, this);
        inputStatus.setText("File selected: " + midiFile.getName());
        playButton.setSelected(false);
    }

    private void outputToHardware(ActionEvent event) {
        JCheckBox source = (JCheckBox) event.getSource();
        if (source.getModel().isSelected()) {
            outputBox.add(outputStatus, 4);
            inputStatus.setText(SerialClass.getInstance().getStatus());
            OutputManager.getInstance().setOutputToHardware(true);
        } else {
            outputBox.remove(outputStatus);
            OutputManager.getInstance().setOutputToHardware(false);
        }
    }

    private void outputToFile(ActionEvent event) {
        JCheckBox source = (JCheckBox) event.getSource();
        if (source.getModel().isSelected()) {
            OutputManager.getInstance().setOutputToFile(true);
            outputBox.add(recordingBox);
            if (InputEventManager.getInstance().getSource() != InputTypeEnum.MidiFile) {
                recordingBox.add(recordingButtonBox);
            }
            outputBox.updateUI();
        } else {
            OutputManager.getInstance().setOutputToFile(false);
            outputBox.remove(recordingBox);
            if (outputBox.getComponentCount() == 7) {
                recordingBox.remove(recordingButtonBox);
            }
            outputBox.updateUI();
        }
    }

    private boolean startRecording() {
        KeyboardFocusManager.getCurrentKeyboardFocusManager().clearGlobalFocusOwner();

        if (!MidiFileManager.getInstance().startRecording(midiFileName.getText())) {
            JOptionPane.showMessageDialog(null, "Enter file name.");
            return false;
        } else {
            pauseRecordingButton.setSelected(false);
            startRecordingButton.setSelected(true);
            return true;
        }
    }

    private void pauseRecording() {
        MidiFileManager.getInstance().endRecording();
        startRecordingButton.setSelected(false);
        pauseRecordingButton.setSelected(true);
    }

    public void endOfMidiFile() {
        playButton.setSelected(false);
        stopButton.setSelected(true);

        if (OutputManager.getInstance().getOutputToFile()) {
            pauseRecording();
        }
    }

    public void stopMidiFileReading() {
        if (midiFileReader == null) return;
        if (midiFileReader.isRunning()) {
            midiFileReader.stopPlaying();
        }
    }

    /**
     * @return The input panel (contains all components of this view)
     */
    public JPanel getPanel() {
        return sidePane;
    }
}
