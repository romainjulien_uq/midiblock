package tempo.tempoevent;

/**
 * The beat listener should be implemented by the processing blocks that rely on tempo, to be
 * notified when a new tick occurs.
 */
public interface BeatListener {

  /**
   * Method called when a beat occur
   */
  void beat(BeatEvent event);
}
