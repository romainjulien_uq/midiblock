package tempo.tempoevent;

/**
 * A beat is fired when a new tick occur in the Tempo class
 */
public class BeatEvent {

  /**
   * New beat.
   */
  public BeatEvent() {
  }
}
