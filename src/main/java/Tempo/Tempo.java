package tempo;

import tempo.tempoevent.BeatEvent;
import tempo.tempoevent.BeatListener;

import java.util.ArrayList;
import java.util.List;
import javax.swing.Timer;

/**
 * A Tempo, in beats per minute. Uses a timer to fire beat events.
 * The timer is not started until the first note is played by the user.
 */
public class Tempo {

  // In beats per minute. Defaults is 120
  private int tempo = 120;
  // The timer used to fire beat events
  private Timer timer;
  // A list of observers to be notified when a beat occurs
  private List<BeatListener> listeners;

  // Singleton
  private static Tempo ourInstance = new Tempo();

  public static Tempo getInstance() {
    return ourInstance;
  }

  private Tempo() {
    listeners = new ArrayList<>();
    timer = new Timer(0, e -> fireBeat());
  }

  /**
   * Start the time, and consequently, start firing beat events on each tick.
   */
  public void startTimer() {
    timer.setDelay((1000 * 60) / tempo);
    timer.start();
  }

  public void stopTimer() {
    timer.stop();
  }

  public int getTempo() {
    return this.tempo;
  }

  public void setTempo(int newTempo) {
    timer.setDelay((1000 * 60) / newTempo);
    this.tempo = newTempo;
  }

    /* ############# LISTENER METHODS ##################### */

  /**
   * Add an object as a listener
   *
   * @param listener
   */
  public void addBeatEventListener(Object listener) {
    listeners.add((BeatListener) listener);
  }

  /**
   * Remove a listener from the list
   *
   * @param listener
   */
  public void removeBeatEventListener(Object listener) {
    listeners.remove((BeatListener) listener);
  }

  /**
   * Fire a beat event on each tick.
   */
  private void fireBeat() {
    for (BeatListener listener : listeners) {
      listener.beat(new BeatEvent());
    }
  }

    /* ############# END LISTENER METHODS ##################### */

}
