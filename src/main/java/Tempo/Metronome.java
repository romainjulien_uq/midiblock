package tempo;

import style.Style;
import tempo.tempoevent.BeatEvent;
import tempo.tempoevent.BeatListener;

import java.awt.FlowLayout;
import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.DataLine;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;
import javax.swing.JPanel;

/**
 * On every beat event the Metronome will change the color of the MetronomePanel and play
 * a sound; on the fourth one it plays a different sound.
 */
public class Metronome implements BeatListener {

  // The metronome panel
  private MetronomePanel metronomePane;

  // The ticking sound
  private Clip tickingNoise;
  private InputStream tickFile =
      this.getClass().getClassLoader().getResourceAsStream("tick.wav");
  // The ticking sound to be played every four beats
  private Clip tickingNoiseFourth;
  private InputStream fourthTickFile =
      this.getClass().getClassLoader().getResourceAsStream("fourthTick.wav");

  // Keep track of the number of beats, reset to 1 after the fourth one
  private int tickCounter = 1;

  // A flag indicating if the metronome should be muted
  private boolean mute = true;

  /**
   * Init the sound streams and register to BeatEvents
   */
  public Metronome() {

    metronomePane = new MetronomePanel();

    initAudioClipTick();
    initAudioClipTickFourthTick();

    Tempo.getInstance().addBeatEventListener(this);
  }

  /**
   * Init the Clip to play for the ticking sound.
   */
  private void initAudioClipTick() {
    try {
      tickFile = this.getClass().getClassLoader().getResourceAsStream("tick.wav");
      InputStream bufferedIn = new BufferedInputStream(tickFile);
      AudioInputStream audioStream = AudioSystem.getAudioInputStream(bufferedIn);
      AudioFormat format = audioStream.getFormat();
      DataLine.Info info = new DataLine.Info(Clip.class, format);
      tickingNoise = (Clip) AudioSystem.getLine(info);
      tickingNoise.open(audioStream);

    } catch (UnsupportedAudioFileException ex) {
      System.out.println("The specified audio file is not supported.");
      ex.printStackTrace();
    } catch (LineUnavailableException ex) {
      System.out.println("Audio line for playing back is unavailable.");
      ex.printStackTrace();
    } catch (IOException ex) {
      System.out.println("Error playing the audio file.");
      ex.printStackTrace();
    }
  }

  /**
   * Init the clip to play for the 4th ticking sound.
   */
  private void initAudioClipTickFourthTick() {
    try {
      fourthTickFile = this.getClass().getClassLoader().getResourceAsStream("fourthTick.wav");
      InputStream bufferedIn = new BufferedInputStream(fourthTickFile);
      AudioInputStream audioStream = AudioSystem.getAudioInputStream(bufferedIn);
      AudioFormat format = audioStream.getFormat();
      DataLine.Info info = new DataLine.Info(Clip.class, format);
      tickingNoiseFourth = (Clip) AudioSystem.getLine(info);
      tickingNoiseFourth.open(audioStream);

    } catch (UnsupportedAudioFileException ex) {
      System.out.println("The specified audio file is not supported.");
    } catch (LineUnavailableException ex) {
      System.out.println("Audio line for playing back is unavailable.");
    } catch (IOException ex) {
      System.out.println("Error playing the audio file.");
    }
  }

  public void close() {
    tickingNoise.close();
  }


  // ####################### GETTER METHODS ##################### //

  public boolean getMute() {
    return this.mute;
  }

  /**
   * Set a wrapper for the metronome panel.
   * @return The wrapper
   */
  public JPanel getPanel() {
    JPanel wrap = new JPanel(new FlowLayout());
    wrap.setOpaque(false);
    wrap.add(metronomePane);
    return wrap;
  }

  // ####################### SETTER METHODS ##################### //

  public void setMute(boolean set) {
    this.mute = set;
  }

  // ########### BEAT LISTENER METHODS ############### //

  /**
   * Play a ticking sound (if not mute) and change the color of the metronome panel.
   * Since we need to init the stream every beat to be able to play the clip, we do so after the
   * clip has been played in order to "be ready" for the newt one.
   * @param event BeatEvent
   */
  @Override
  public void beat(BeatEvent event) {
    if (!mute) {
      if (tickCounter == 4) {
        tickingNoiseFourth.start();
        initAudioClipTick();
      } else if (tickCounter == 3) {
        tickingNoise.start();
        initAudioClipTickFourthTick();
      } else {
        tickingNoise.start();
        initAudioClipTick();
      }
    }

    changeColorMetronomePane();

    tickCounter = (tickCounter == 4) ? 1 : (tickCounter + 1);
  }

  private void changeColorMetronomePane() {
    switch (tickCounter) {
      case 1:
        metronomePane.setColor(Style.COLOR_LIGHT_ORANGE_TRANSPARENT);
        break;
      case 2:
        metronomePane.setColor(Style.COLOR_LIGHT_ORANGE);
        break;
      case 3:
        metronomePane.setColor(Style.COLOR_ORANGE);
        break;
      case 4:
        metronomePane.setColor(Style.COLOR_DARK_ORANGE);
        break;
      default:
        break;
    }
    metronomePane.repaint();
  }
}
