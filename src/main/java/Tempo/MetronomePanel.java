package tempo;

import style.Style;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import javax.swing.JPanel;

/**
 * A custom component used as a visual metronome.
 */
public class MetronomePanel extends JPanel {

  private Color color = Style.COLOR_LIGHT_ORANGE_TRANSPARENT;
  private Color borderColor = Style.COLOR_DARK_ORANGE;

  /**
   * Set the size and the layout of the metronome panel.
   */
  public MetronomePanel() {
    super();
    setLayout(null);
    setOpaque(false);
    setPreferredSize(new Dimension(100, 100));
  }

  public void setColor(Color color) {
    this.color = color;
  }

  @Override
  protected void paintComponent(Graphics g) {
    super.paintComponent(g);
    Graphics2D graphics = (Graphics2D) g;

    graphics.setColor(color);
    graphics.fillOval(0, 0, getWidth() - 1, getHeight() - 1);
    graphics.setColor(borderColor);
    graphics.drawOval(0, 0, getWidth() - 1, getHeight() - 1);
  }

}
