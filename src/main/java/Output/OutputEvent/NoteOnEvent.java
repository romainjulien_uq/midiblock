package output.outputevent;

/**
 * The note on event is fired after an input has been processed through the list of
 * ProcessingBlocks. It contains the Midi number of the note that should be turned on.
 */
public class NoteOnEvent {

  // The midi number of the note on
  private int midiNumber;

  public NoteOnEvent(int midiNumber) {
    this.midiNumber = midiNumber;
  }

  public int getMidiNumber() {
    return this.midiNumber;
  }
}
