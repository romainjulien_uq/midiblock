package output.outputevent;

/**
 * The noteOn and noteOff events will be fired after an input has been processed through
 * the list of ProcessingBlock currently in the MidiStream.
 */
public interface OutputEventListener {

  /**
   * Method called when a note is On
   */
  void noteOn(NoteOnEvent event);

  /**
   * Method called when a note is Off
   */
  void noteOff(NoteOffEvent event);
}