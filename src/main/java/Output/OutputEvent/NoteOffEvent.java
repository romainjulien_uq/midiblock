package output.outputevent;

/**
 * The note off event is fired after an input has been processed through the list of
 * ProcessingBlocks. It contains the Midi number of the note that should be turned off.
 */
public class NoteOffEvent {

  // The midi number of the note off
  private int midiNumber;

  public NoteOffEvent(int midiNumber) {
    this.midiNumber = midiNumber;
  }

  public int getMidiNumber() {
    return this.midiNumber;
  }

}
