package output;

import output.outputevent.NoteOffEvent;
import output.outputevent.NoteOnEvent;
import output.outputevent.OutputEventListener;

import javax.sound.midi.MidiChannel;
import javax.sound.midi.MidiSystem;
import javax.sound.midi.Synthesizer;

/**
 * A class used to test the software. Simulate the output from the hardware.
 */
public class PlayerTest implements OutputEventListener {

  private static final int VOLUME = 127;
  private Synthesizer synthesizer;
  private MidiChannel piano;

  /**
   * Open synthesizer and open the piano chanel.
   */
  public PlayerTest() {
    try {
      synthesizer = MidiSystem.getSynthesizer();
      synthesizer.open();

      MidiChannel[] channels = synthesizer.getChannels();
      piano = channels[0];

    } catch (Exception e) {
      synthesizer.close();
      System.out.println("Can't start Java Midi Synthesizer");
    }

    OutputManager.getInstance().addOutputEventListener(this);
  }

  /**
   * Play the note as it would be done by the hardware.
   * @param event Note on event.
   */
  @Override
  public void noteOn(NoteOnEvent event) {
    piano.noteOn(event.getMidiNumber(), VOLUME);
  }

  /**
   * Turn off the note as it would be done by the hardware.
   * @param event Note off event.
   */
  @Override
  public void noteOff(NoteOffEvent event) {
    piano.noteOff(event.getMidiNumber());
  }
}
