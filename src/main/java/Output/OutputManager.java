package output;

import blockscontrolpane.processingblocks.ProcessingBlock;
import output.outputevent.NoteOffEvent;
import output.outputevent.NoteOnEvent;
import output.outputevent.OutputEventListener;

import java.util.ArrayList;
import java.util.List;

/**
 * The OutputManager is responsible for firing noteOn and noteOff events after a user input
 * has been processed through the list of ProcessingBlock in the MidiStream.
 * Note that classes that implements the OutputEventListener don't register them-self,
 * instead the OutputManager receives notifications when the user select an output
 * and register or unregister the corresponding classes.
 * The OutputManager extends ProcessingBlock to allow the last block in MidiStream to forward
 * its output directly here.
 */
public class OutputManager extends ProcessingBlock {

  // A flag indicating if SerialClass (connection to hardware) should receive note on/off events
  private boolean outputToHardware = false;
  // A flag indicating if MidiFileManager (record MidiFiles) should receive note on/off events
  private boolean outputToFile = false;

  // A list of observers to be notified when a note has been processed
  private List<OutputEventListener> listeners;

  // Singleton
  private static OutputManager ourInstance = new OutputManager();

  public static OutputManager getInstance() {
    return ourInstance;
  }

  private OutputManager() {
    type = null;
    listeners = new ArrayList<>();
  }


  // ############# LISTENER METHODS ##################### //

  /**
   * Add an object as a listener for note on/off events.
   *
   * @param listener Instance of OutputEventListener
   */
  public void addOutputEventListener(Object listener) {
    listeners.add((OutputEventListener) listener);
  }

  /**
   * Remove a listener from the list.
   *
   * @param listener Instance of OutputEventListener
   */
  public void removeOutputEventListener(Object listener) {
    listeners.remove(listener);
  }

  /**
   * Notify observers of a note on event.
   * @param midiNumber Midi number of the note
   */
  private void fireNoteOn(int midiNumber) {
    NoteOnEvent event = new NoteOnEvent(midiNumber);

    for (OutputEventListener listener : listeners) {
      listener.noteOn(event);
    }
  }

  /**
   * Notify observers of a note off event.
   * @param midiNumber Midi number of the note
   */
  private void fireNoteOff(int midiNumber) {
    NoteOffEvent event = new NoteOffEvent(midiNumber);

    for (OutputEventListener listener : listeners) {
      listener.noteOff(event);
    }
  }


  // ############# END LISTENER METHODS #####################//

  // ################# SETTER METHODS ###################### //

  /**
   * The user has selected/deselected output to the synthesizer.
   * Add or remove SerialClass (connection to hardware) to (from) the list of listeners.
   *
   * @param set Indicate if output to hardware is selected in UI
   */
  public void setOutputToHardware(boolean set) {
    outputToHardware = set;
    if (set) {
      addOutputEventListener(SerialClass.getInstance());
    } else {
      removeOutputEventListener(SerialClass.getInstance());
    }
  }

  /**
   * The user has selected/deselected output to Midi file.
   * Add or remove MidiFileManager (record MidiFiles) to (from) the list of listeners.
   *
   * @param set Indicate if output to Midi file is selected in UI
   */
  public void setOutputToFile(boolean set) {
    outputToFile = set;
    if (set) {
      addOutputEventListener(MidiFileManager.getInstance());
    } else {
      removeOutputEventListener(MidiFileManager.getInstance());
    }
  }

  // ################# GETTER METHODS ###################### //


  public boolean getOutputToHardware() {
    return outputToHardware;
  }

  public boolean getOutputToFile() {
    return outputToFile;
  }

  public boolean hasSelectedOutput() {
    return (outputToHardware || outputToFile);
  }


  // ################# ABSTRACT METHODS ###################### //


  @Override
  public void noteOn(int note, int indexInChain) {
    fireNoteOn(note);
  }

  @Override
  public void noteOff(int note, int indexInChain) {
    fireNoteOff(note);
  }

  @Override
  public List<Object> getParameters() {
    // SHOULD NEVER BE CALLED
    return new ArrayList<>();
  }

  @Override
  public void setParameters(List<Object> params) {
    // SHOULD NEVER BE CALLED
  }


}
