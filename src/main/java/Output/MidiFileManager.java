package output;

import output.outputevent.NoteOffEvent;
import output.outputevent.NoteOnEvent;
import output.outputevent.OutputEventListener;

import java.io.File;
import javax.sound.midi.MidiEvent;
import javax.sound.midi.MidiSystem;
import javax.sound.midi.Sequence;
import javax.sound.midi.ShortMessage;
import javax.sound.midi.Track;
import javax.swing.Timer;

/**
 * This class is responsible for recording note on and note off events and write to a Midi file.
 * It implements OutputEventListener to be notified on noteOn and noteOff events.
 */
public class MidiFileManager implements OutputEventListener {

  // Variable used to write the midi file

  // maximum volume
  private static final int VOLUME = 127;
  // beats per minute
  private static final int TEMPO = 120;
  // the RESOLUTION is expressed in ticks per quarter note
  private static final float DIVISION_TYPE = Sequence.PPQ;
  // RESOLUTION is 16 ticks per quarter note.
  private static final int RESOLUTION = 16;
  // Piano
  private static final int INSTRUMENT = 0;

  // The timer is used to have a time component in the midi file
  private Timer sequenceTimer;
  private int currentTick = 0;

  // The sequence to write to file
  private Sequence sequence = null;
  // The track that records note on and note off events
  private Track track = null;

  // The name of the midi file to write
  private String midiFile;

  // Singleton
  private static MidiFileManager ourInstance = new MidiFileManager();

  public static MidiFileManager getInstance() {
    return ourInstance;
  }

  /**
   * Sets up the resolution and parameters of the sequence to record.
   */
  private MidiFileManager() {
    // Initialize sequenceTimer
    sequenceTimer = new Timer(0, e -> currentTick++);
    // The sequence timer matches the chosen resolution of 16 ticks per quarter note.
    sequenceTimer.setDelay(1000 / ((TEMPO / 60) * RESOLUTION));

    try {
      // RESOLUTION is 16 ticks per quarter note.
      sequence = new Sequence(DIVISION_TYPE, RESOLUTION);

      // Start a new track
      track = sequence.createTrack();

      // Set the INSTRUMENT on channel 0
      ShortMessage setInstrumentMessage = new ShortMessage();
      setInstrumentMessage.setMessage(ShortMessage.PROGRAM_CHANGE, 0, INSTRUMENT, 0);
      track.add(new MidiEvent(setInstrumentMessage, 0));

    } catch (Exception e) {
      System.out.println("Can't set up track");
    }
  }

  /**
   * Start recording outputs from the MidiStream, and save the name of the file to write
   * this recording to.
   *
   * @param filename The name of the midi file.
   * @return true if the filename is valid, and false otherwise
   */
  public boolean startRecording(String filename) {
    if (filename.equals("") || filename.equals(".mid")) {
      return false;
    }

    // Make sure the file has a .mid extension
    if (!filename.contains(".mid")) {
      midiFile = filename + ".mid";
    } else {
      midiFile = filename;
    }

    sequenceTimer.start();
    return true;
  }

  /**
   * Stop recording outputs from the MidiStream and save the current record to the midi file.
   * Reinitialize track and tick count to be ready for another recording.
   */
  public void endRecording() {
    sequenceTimer.stop();
    writeSequenceToFile();

    // Get ready for a new recording
    currentTick = 0;
    track = sequence.createTrack();
  }


  // #################### HELPER METHOD ######################### //


  /**
   * Write the recorded sequence to a MidiFile.
   */
  private void writeSequenceToFile() {
    int[] allowedTypes = MidiSystem.getMidiFileTypes(sequence);
    if (allowedTypes.length == 0) {
      System.out.println("No supported MIDI file types.");
    } else {
      try {
        MidiSystem.write(sequence, allowedTypes[0], new File(midiFile));
      } catch (Exception e) {
        System.out.println("Can't write to given file");
      }
    }
  }


  // ############# OUTPUT EVENT LISTENER METHODS ################ //

  /**
   * If the user has started recording (ie, timer is running) then add NOTE_ON MidiEvents
   * to the track.
   *
   * @param event NoteOn
   */
  @Override
  public void noteOn(NoteOnEvent event) {
    if (!sequenceTimer.isRunning()) { // ie user doesn't wish to record
      return;
    }

    if (track == null) {
      System.out.println("Track is null...");
      return;
    }

    try {
      ShortMessage on = new ShortMessage();
      on.setMessage(ShortMessage.NOTE_ON, 0, event.getMidiNumber(), VOLUME);
      track.add(new MidiEvent(on, currentTick));
    } catch (Exception e) {
      System.out.println("Can't write note On event");
    }
  }

  /**
   * If the user has started recording (ie, timer is running) then add NOTE_OFF MidiEvents
   * to the track.
   *
   * @param event NoteOn
   */
  @Override
  public void noteOff(NoteOffEvent event) {
    if (!sequenceTimer.isRunning()) { // ie user doesn't wish to record
      return;
    }

    if (track == null) {
      System.out.println("Track is null...");
      return;
    }

    try {
      ShortMessage off = new ShortMessage();
      off.setMessage(ShortMessage.NOTE_OFF, 0, event.getMidiNumber(), VOLUME);
      track.add(new MidiEvent(off, currentTick));
    } catch (Exception e) {
      System.out.println("Can't write note Off event");
    }
  }
}
