package output;

import gnu.io.CommPortIdentifier;
import gnu.io.PortInUseException;
import gnu.io.SerialPort;
import gnu.io.SerialPortEvent;
import gnu.io.SerialPortEventListener;
import gnu.io.UnsupportedCommOperationException;
import output.outputevent.NoteOffEvent;
import output.outputevent.NoteOnEvent;
import output.outputevent.OutputEventListener;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.TooManyListenersException;


/**
 * This class is responsible for the communication with the synthesizer (hardware).
 * When a port is selected by the user, it opens a communication through that port,
 * allowing messages to be sent to the hardware via USB cable.
 */
public class SerialClass implements SerialPortEventListener, OutputEventListener {

  // Timeout when attempting to open a port
  public static final int TIME_OUT = 2000;
  // Baud rate
  public static final int DATA_RATE = 9600;

  // A buffer to receive acknowledgement messages from the hardware
  public static BufferedReader input;
  // The stream to send messages to the hardware
  public static OutputStream output;

  // The id of the port used to communicate with the hardware
  private static CommPortIdentifier portId = null;
  // The port to communicate with the hardware
  public SerialPort serialPort;
  // A connection status to be displayed in the GUI
  private String status = "";

  // Singleton
  private static SerialClass ourInstance = new SerialClass();

  public static SerialClass getInstance() {
    return ourInstance;
  }

  private SerialClass() {
  }


  /**
   * Get a list of available ports.
   * @return Available ports
   */
  public Enumeration getPorts() {
    return CommPortIdentifier.getPortIdentifiers();
  }

  /**
   * Convert the list of available ports to an array of Objects.
   * @return Array of ports
   */
  public Object[] getPortsArray() {
    Enumeration portEnum = getPorts();
    List<String> ports = new ArrayList<>();

    while (portEnum.hasMoreElements()) {
      CommPortIdentifier currPortId = (CommPortIdentifier) portEnum.nextElement();
      ports.add(currPortId.getName());
    }

    return ports.toArray();
  }

  /**
   * Takes a port name as a parameter, find the corresponding port id, and open the port for
   * communication with hardware.
   * Once the port is open, set the defined parameters, open input and output streams, and
   * add a SerialPortEventListener.
   * @param portName Name of the port to open
   * @return true if the port was successfully opened, and false otherwise.
   */
  public boolean openPort(String portName) {
    close();
    setPortId(portName);

    if (portId == null) {
      System.out.println("Could not find COM port.");
      status = "Not connected";
      return false;
    }

    try {
      // open serial port, and use class name for the appName.
      serialPort = (SerialPort) portId.open(this.getClass().getName(), TIME_OUT);
      System.out.println("1) Serial port opened: " + portId.getName());
    } catch (PortInUseException e) {
      System.out.println("Can't open port: " + portId.getName());
      close();
      return false;
    }

    try {
      // set port parameters
      serialPort.setSerialPortParams(DATA_RATE,
          SerialPort.DATABITS_8,
          SerialPort.STOPBITS_1,
          SerialPort.PARITY_NONE);
      System.out.println("2) Serial port parameters set");
    } catch (UnsupportedCommOperationException e) {
      System.out.println("Can't set port parameters: " + portId.getName());
      close();
      return false;
    }

    try {
      // open the streams
      input = new BufferedReader(new InputStreamReader(serialPort.getInputStream()));
      output = serialPort.getOutputStream();
      System.out.println("3) Input and output streams are opened");
    } catch (IOException e) {
      System.out.println("Can't open inout and output streams: " + portId.getName());
      close();
      return false;
    }

    if (writeData("conf:1")) {
      System.out.println("4) Sent notification port open");
    } else {
      System.out.println("Can't send notification port open" + portId.getName());
    }


    try {
      // add event listeners
      serialPort.addEventListener(this);
      serialPort.notifyOnDataAvailable(true);
      System.out.println("4) Add listeners");
    } catch (TooManyListenersException e) {
      System.out.println("Can't add listeners: " + portId.getName());
      close();
      return false;
    }

    status = "Connected to: " + portId.getName();
    return true;
  }

  /**
   * Set portId instance variable. Given a port name, iterate through the list of available
   * ports looking for a match.
   *
   * @param portName The port name to match.
   */
  private void setPortId(String portName) {
    Enumeration portEnum = getPorts();

    while (portEnum.hasMoreElements()) {
      CommPortIdentifier currPortId = (CommPortIdentifier) portEnum.nextElement();
      if (currPortId.getName().equals(portName)) {
        portId = currPortId;
        break;
      }
    }
  }

  /**
   * Convert the string into bytes and send teh array of bytes to the hardware.
   *
   * Note: All outputs are terminated with a new line character to facilitate reading
   * on the hardware side.
   *
   * @param msg Message to send
   * @return true if the message was sent, and false otherwise.
   */
  public synchronized boolean writeData(String msg) {
    try {
      String toSend = msg + '\n';
      output.write(toSend.getBytes());
      System.out.println("Sent:  " + toSend);
      return true;
    } catch (Exception e) {
      System.out.println("could not write to port");
      return false;
    }
  }

  /**
   * Close the serial port.
   */
  public synchronized void close() {
    if (serialPort != null) {
      writeData("conf:0");
      serialPort.removeEventListener();
      serialPort.close();
    }
    status = "Not connected";
  }


  // ####################### GETTER METHOD ##################### //


  /**
   * Get connection status (ie, connected? if yes, which port?).
   * @return connection status
   */
  public String getStatus() {
    return status;
  }


  // ########### SERIAL PORT EVENT LISTENER METHODS ############### //


  /**
   * Used to read data from hardware.
   *
   * @param oEvent: SerialPortEvent.DATA_AVAILABLE indicate a new input
   */
  @Override
  public synchronized void serialEvent(SerialPortEvent oEvent) {
    if (oEvent.getEventType() == SerialPortEvent.DATA_AVAILABLE) {
      try {
        String inputLine = input.readLine();
        System.out.println("RECEIVED: " + inputLine);
      } catch (Exception e) {
        System.err.println("Error reading inputs. Closing connection.");
        close();
      }
    }
  }


  // ############# OUTPUT EVENT LISTENER METHODS ################ //


  /**
   * Send message of the form "note:on:[midi number] to the hardware.
   * @param event Note on event
   */
  @Override
  public void noteOn(NoteOnEvent event) {
    writeData("note:on:" + event.getMidiNumber());
  }

  /**
   * Send message of the form "note:off:[midi number] to the hardware.
   * @param event Note off event
   */
  @Override
  public void noteOff(NoteOffEvent event) {
    writeData("note:off:" + event.getMidiNumber());
  }
}
