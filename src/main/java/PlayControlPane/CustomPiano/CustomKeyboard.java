package playcontrolpane.custompiano;

import playcontrolpane.KeyMapper;

import java.awt.Dimension;
import javax.swing.JPanel;

/**
 * The custom keyboard is a JPanel that contains 15 custom piano keys.
 */
public class CustomKeyboard extends JPanel {

  /**
   * Bound the height of the keyboard to 200.
   */
  public CustomKeyboard() {
    super();
    this.setPreferredSize(new Dimension(this.getPreferredSize().width, 200));
    this.setOpaque(false);
  }

  /**
   * Remove all piano keys from the panel and add the ones currently stored in KeyMapper.
   */
  public void refreshKeyboard() {
    this.removeAll();

    for (CustomPianoKey key : KeyMapper.getCustomKeys()) {
      this.add(key);
    }

    this.updateUI();
  }

}
