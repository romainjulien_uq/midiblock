package playcontrolpane.custompiano;

import style.Style;
import inputs.InputEventManager;
import inputs.InputTypeEnum;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;
import javax.swing.*;

/**
 * Represent the custom piano keys on the custom keyboards. The keys are custom components
 * of fixed size (50x200) and contain:
 * - The note name
 * - Indicator (circle) if it is a root note
 * - The QWERTY key(s) mapped to this key
 *
 * The KeyMapper is responsible for creating instances of the CustomKey.
 */
public class CustomPianoKey extends JComponent implements MouseListener {

  // The note name - one letter (ie, C, C#, D, E... etc)
  private Character name;
  // The note octave (number between 1 and 7, can be 0 or 8 and set inactive)
  private String octave = "";
  // Black or white
  private Color color;

  // A flag indicating if this key represent a root note
  private boolean isRootNote;

  // The rectangle representing the key
  private Rectangle rectangleKey;
  // Necessary to indicate when the key is pressed (change opacity)
  private boolean isPressed = false;
  // true if note is out of range
  private boolean isInactive;

  // Contains the QWERTY key names mapped to this key (1 or 2)
  private ArrayList<Character> qwertyMapping;
  // true if the mapping is on the 3rd or 4th row of the QWERTY keyboard
  private boolean mappingOnTwoLastRows = false;

  /**
   *
   * @param keyName Note name (single char, ie C, D, E...)
   * @param keyOctave Octave
   * @param keyColor Black or WHite
   * @param inactive flag indicating if the key is out of range
   * @param rootNote flag indicating if the key is a root note
   */
  public CustomPianoKey(Character keyName, String keyOctave, Color keyColor, boolean inactive, boolean rootNote) {
    super();
    name = keyName;
    color = keyColor;
    octave = keyOctave;
    isInactive = inactive;
    isRootNote = rootNote;

    qwertyMapping = new ArrayList<>();

    this.setPreferredSize(new Dimension(50, 200));
    rectangleKey = new Rectangle(0, 0, 50, 200);

    // Add Mouse listener
    this.addMouseListener(this);
  }

  /**
   * Draw the piano key
   * @param g
   */
  public void paint(Graphics g) {
    Graphics2D g2d = (Graphics2D) g;

    // Set the color of the key (transparent if inactive or pressed)
    if (isPressed || isInactive) {
      g2d.setColor(color == Color.BLACK ? Style.COLOR_BLACK_TRANSPARENT : Style.COLOR_WHITE_TRANSPARENT);
    } else {
      g2d.setColor(color);
    }

    g2d.fill(rectangleKey);

    // Draw the note name on the key
    g2d.setColor(Style.COLOR_LIGHT_ORANGE);
    g2d.setFont(Style.FONT_TEXT_HIGHLIGHT);
    String fullName = getKeyFullName();
    if (fullName.length() > 2) { // ie "A#".length() = 2 vs "A".length() = 1
      g2d.drawString(fullName, 15, 20);
      g2d.drawString(getFlatPartKeyName(), 15, 35);
    } else {
      g2d.drawString(fullName, 19, 20);
    }

    // Draw an orange dot to the center if key is root note
    if (isRootNote) {
      g2d.setColor(Style.COLOR_LIGHT_ORANGE);
      g2d.fillOval(20, 100, 9, 9);
    }

    // Draw the name of the QWERTY key(s) mapped to this note
    g2d.setColor(Style.COLOR_GREY);
    if (qwertyMapping.size() == 1) {
      if (!mappingOnTwoLastRows) {
        g2d.drawString("( " + qwertyMapping.get(0) + " )", 14, 160);
      } else {
        g2d.drawString("( " + qwertyMapping.get(0) + " )", 14, 180);
      }
    } else if (qwertyMapping.size() == 2) {
      g2d.drawString("( " + qwertyMapping.get(0) + " )", 14, 160);
      g2d.drawString("( " + qwertyMapping.get(1) + " )", 14, 180);
    }
  }

  /**
   * Add a QWERTY key to the array of keys mapped to the key.
   * @param mapping Name of QWERTY key.
   */
  public void addMapping(Character mapping) {
    qwertyMapping.add(mapping);
  }

  /**
   * Remove all QWERTY key names in array.
   */
  public void clearMapping() {
    qwertyMapping.clear();
  }

  /**
   * Set the octave +/- 1.
   * @param move +/-1
   */
  public void moveOneOctave(int move) {
    int prev = getOctave();
    octave = String.valueOf(prev + move);
  }


  // ####################### GETTER METHODS ##################### //


  public String getKeyFullName() {
    return name.toString() + (color == Color.BLACK ? "#" : "") + octave;
  }

  public String getKeyNameWithoutOctave() {
    return name.toString() + (color == Color.BLACK ? "#" : "");
  }

  public int getOctave() {
    return Integer.valueOf(octave);
  }

  public Color getColor() {
    return color;
  }

  private String getFlatPartKeyName() {
    int charInt = (int) name.charValue();
    int next = charInt + 1;
    char nextChar = (char) next;
    String nameNext = Character.valueOf(nextChar).toString();
    nameNext = nameNext.equals("H") ? "A" : nameNext;
    return nameNext + "b" + octave;
  }

  public boolean isRootNote() {
    return isRootNote;
  }

  public boolean isInactive() {
    return isInactive;
  }


  // ####################### SETTER METHODS ##################### //


  public void setMappingOnTwoLastRows(boolean newValue) {
    mappingOnTwoLastRows = newValue;
  }

  public void setInactive(boolean newValue) {
    isInactive = newValue;
  }


  // ########### MOUSE EVENT LISTENER METHODS ############### //


  @Override
  public void mouseClicked(MouseEvent e) {
    // Ignore
  }

  /**
   * Notify the InputEventManager when a key is pressed, after checking that custom
   * keyboard is selected as source of input by the user, and the key is not inactive.
   * Repaint the key after setting isPressed = true.
   *
   * @param e ignore
   */
  @Override
  public void mousePressed(MouseEvent e) {
    if (InputEventManager.getInstance().getSource() != InputTypeEnum.Custom) return;
    if (!isInactive) {
      isPressed = true;
      repaint();
      InputEventManager.getInstance().customKeyPressed(getKeyFullName());
    }
  }

  /**
   * Notify the InputEventManager when a key is released, after checking that the
   * key is not inactive. Repaint the key after setting isPressed = false.
   * @param e ignore
   */
  @Override
  public void mouseReleased(MouseEvent e) {
    if (!isInactive) {
      isPressed = false;
      repaint();
      InputEventManager.getInstance().customKeyReleased(getKeyFullName());
    }
  }

  @Override
  public void mouseEntered(MouseEvent e) {
    // Ignore
  }

  @Override
  public void mouseExited(MouseEvent e) {
    // Ignore
  }
}
