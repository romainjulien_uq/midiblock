package playcontrolpane;

import playcontrolpane.custompiano.CustomPianoKey;

import java.awt.Color;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by romainjulien on 7/09/2015.
 */
public class KeyMapper {

  // A list of the 9 first keys on the 1st row of the QWERTY keyboard, as defined in KeyEvent
  private static final List<Integer> firstRowQWERTY = Arrays.asList(
      KeyEvent.VK_1,
      KeyEvent.VK_2,
      KeyEvent.VK_3,
      KeyEvent.VK_4,
      KeyEvent.VK_5,
      KeyEvent.VK_6,
      KeyEvent.VK_7,
      KeyEvent.VK_8,
      KeyEvent.VK_9
  );

  // A list of the 9 first keys on the 2nd row of the QWERTY keyboard, as defined in KeyEvent
  private static final List<Integer> secondRowQWERTY = Arrays.asList(
      KeyEvent.VK_Q,
      KeyEvent.VK_W,
      KeyEvent.VK_E,
      KeyEvent.VK_R,
      KeyEvent.VK_T,
      KeyEvent.VK_Y,
      KeyEvent.VK_U,
      KeyEvent.VK_I,
      KeyEvent.VK_O
  );

  // A list of the 9 first keys on the 3rd row of the QWERTY keyboard, as defined in KeyEvent
  private static final List<Integer> thirdRowQWERTY = Arrays.asList(
      KeyEvent.VK_A,
      KeyEvent.VK_S,
      KeyEvent.VK_D,
      KeyEvent.VK_F,
      KeyEvent.VK_G,
      KeyEvent.VK_H,
      KeyEvent.VK_J,
      KeyEvent.VK_K,
      KeyEvent.VK_L
  );

  // A list of the 9 first keys on the 4th row of the QWERTY keyboard, as defined in KeyEvent
  private static final List<Integer> fourthRowQWERTY = Arrays.asList(
      KeyEvent.VK_Z,
      KeyEvent.VK_X,
      KeyEvent.VK_C,
      KeyEvent.VK_V,
      KeyEvent.VK_B,
      KeyEvent.VK_N,
      KeyEvent.VK_M,
      KeyEvent.VK_COMMA,
      KeyEvent.VK_PERIOD
  );

  // Keep track of the list of custom piano keys to be displayed in the custom keyboard
  private static List<CustomPianoKey> customKeys;
  // Map the representation of a QWERTY key in KeyEvent to a key name
  private static Map<Integer, String> keyMap;

  public KeyMapper() {
    //
  }

  /**
   * When a new scale is selected by the user, create a list of 15 CustomPianoKey and
   * map them on the QWERTY keyboard.
   *
    * @param newScale List of note names (ie, C, C#, D etc)
   * @param rootNote The root note of the scale
   */
  public static void loadNewScale(List<String> newScale, String rootNote) {
    customKeys = new ArrayList<>();
    keyMap = new HashMap<>();

    findKeys(newScale, rootNote);
    mapKeys();
  }

  /**
   * Method called when the user wants to shif the keyboard left or right by one octave.
   * Add, or substract, 1 from the octave for each key displayed in the custom keyboard, then
   * redo the mapping.
   *
   * @param shift +/- 1
   */
  public static void shiftKeyboardOneOctave(int shift) {
    for (CustomPianoKey key : customKeys) {
      key.moveOneOctave(shift);
      // If the key is out of range, set it inactive
      key.setInactive(isOutOfRange(key.getOctave()));
    }

    // Redo mapping
    clearMapping();
    mapKeys();
  }

  /**
   * Shift the keyboard by one note to the left. This is done by adding a new key at the
   * beginning of the list, and remove the last one of the list.
   * We determine the key to be added based on the first key currently in list and
   * the current scale.
   *
   * @param scale Current scale
   * @param rootNote Root note name in scale
   */
  public static void shiftKeyboardLeftOneNote(List<String> scale, String rootNote) {
    // Values to construct the new key to be added at the beginning of the list
    int indexInScaleNewKey = getIndexPrevKey(scale, customKeys.get(0).getKeyNameWithoutOctave());
    String newKeyName = scale.get(indexInScaleNewKey);
    Color newKeyColor = findKeyColor(newKeyName);
    boolean newKeyIsRoot = isRootNote(newKeyName, rootNote);
    boolean newKeyIsOutOfRange;

    // Determine if the octave changes (-1) between the new key and the first key in list
    String octaveChangeAt = scale.get(findNumberLeadingAB(scale));
    int octave;
    if (customKeys.get(0).getKeyNameWithoutOctave().equals(octaveChangeAt)) {
      octave = customKeys.get(0).getOctave() - 1;
    } else {
      octave = customKeys.get(0).getOctave();
    }

    // Make sure the key is in range
    newKeyIsOutOfRange = isOutOfRange(octave);

    // Remove last key and add new one at the beginning of the list
    customKeys.remove(14);
    customKeys.add(0, new CustomPianoKey(
        newKeyName.charAt(0), // Key letter (A, B,...)
        String.valueOf(octave), // Octave
        newKeyColor, // color
        newKeyIsOutOfRange,
        newKeyIsRoot
    ));

    // Redo mapping
    clearMapping();
    mapKeys();
  }

  /**
   * Shift the keyboard by one note to the right. This is done by adding a new key at the
   * end of the list, and remove the first one of the list.
   * We determine the key to be added based on the last key currently in list and
   * the current scale.
   *
   * @param scale Current scale
   * @param rootNote Root note name in scale
   */
  public static void shiftKeyboardRightOneNote(List<String> scale, String rootNote) {
    // Values to construct the new key to be added at the end of the list
    int indexInScaleNewKey = getIndexNextKey(scale, customKeys.get(14).getKeyNameWithoutOctave());
    String newKeyName = scale.get(indexInScaleNewKey);
    Color newKeyColor = findKeyColor(newKeyName);
    boolean newKeyIsRoot = isRootNote(newKeyName, rootNote);
    boolean newKeyIsOutOfRange;

    // Determine if the octave changes (+1) between the new key and the last key in list
    String octaveChangeAt = scale.get(findNumberLeadingAB(scale));
    int octave;
    if (newKeyName.equals(octaveChangeAt)) {
      octave = customKeys.get(14).getOctave() + 1;
    } else {
      octave = customKeys.get(14).getOctave();
    }

    // Make sure the new key is in range
    newKeyIsOutOfRange = isOutOfRange(octave);

    // Remove first key and add new one at the end of the list
    customKeys.remove(0);
    customKeys.add(new CustomPianoKey(
        newKeyName.charAt(0), // Key letter (A, B,...)
        String.valueOf(octave), // Octave
        newKeyColor, // color
        newKeyIsOutOfRange,
        newKeyIsRoot
    ));

    // Redo mapping
    clearMapping();
    mapKeys();
  }


  // ####################### GETTER METHODS ##################### //


  /**
   * Get the list of custom keys displayed in the CustomKeyboard.
   * @return List of CustomPianoKey
   */
  public static List<CustomPianoKey> getCustomKeys() {
    return customKeys;
  }

  /**
   * Get the note name mapped to the given QWERTY key code.
   *
   * @param keyCode KeyEvent.keyCode
   * @return Note name mapped
   */
  public static String getKeyNameMappedTo(int keyCode) {
    try {
      return keyMap.get(keyCode);
    } catch (Exception e) {
      return null;
    }
  }

  /**
   * Get the octave of the first root note in list.
   *
   * @return Octave
   */
  public static int getRootNoteOctave() {
    for (CustomPianoKey key : customKeys) {
      if (key.isRootNote()) {
        return key.getOctave();
      }
    }
    // Should never get here
    return 0;
  }

  public static boolean lastKeyIsInactive() {
    return customKeys.get(14).isInactive();
  }

  public static boolean firstKeyIsInactive() {
    return customKeys.get(0).isInactive();
  }

  // ####################### HELPER METHODS ##################### //


  /**
   * The method is called when a new scale is selected by the user.
   * It creates 15 instances of CustomPianoKey corresponding to the first 15 notes based
   * on the given scale.
   *
   * @param scale Selected scale (list of note names)
   * @param rootNote Root note of the scale
   */
  private static void findKeys(List<String> scale, String rootNote) {
    // Values to construct the new key
    int index = 0;
    int octave = 0;
    boolean isOutOfRange;
    boolean isRootNote;
    Color keyColor;

    // Start at octave 0 if the root note is either A, A# or B
    int numberLeadingAB = findNumberLeadingAB(scale);
    if (numberLeadingAB == 0) {
      octave = 1;
    }

    // Add 15 CustomPianoKey to customKeys
    while (customKeys.size() < 15) {
      String keyName = scale.get(index);
      keyColor = findKeyColor(keyName);
      isOutOfRange = isOutOfRange(octave);
      isRootNote = isRootNote(keyName, rootNote);

      // Add key to the list
      customKeys.add(new CustomPianoKey(
          keyName.charAt(0), // Key letter (A, B,...)
          String.valueOf(octave), // Octave
          keyColor, // color
          isOutOfRange,
          isRootNote
      ));

      // Change octave after last of [A, A#, B] in scale
      if (numberLeadingAB == 0) {
        if (index == scale.size() - 1) {
          octave++;
        }
      } else {
        if (index == numberLeadingAB - 1) {
          octave++;
        }
      }

      // Start again at the beginning of the scale when the end is reached
      if (index == scale.size() - 1) {
        index = 0;
      } else {
        index++;
      }
    }
  }

  /**
   * Start by mapping the two top rows on QWERTY keyboard, where the first row corresponds
   * to black keys, and the second row corresponds to white keys.
   * If all 15 keys were not mapped in the first two rows, map the rest of the keys on the two
   * bottom rows of the QWERTY keyboard.
   *
   * The two first rows are mapped left-to-right, mapping as many keys as possible, with a maximum
   * of 9 keys on one row. The two last rows are mapped right-to-left.
   */
  private static void mapKeys() {
    // Ignore inactive keys, if any.
    int startIndex = getNumberLeadingInactiveNotes();
    int endIndex = 14 - getNumberTrailingInactiveNotes();

    if (!mapCustomKeysToQwertyKeyboard(firstRowQWERTY, secondRowQWERTY, startIndex, endIndex, 0, 1)) {
      mapCustomKeysToQwertyKeyboard(thirdRowQWERTY, fourthRowQWERTY, endIndex, 0, 8, -1);
    }
  }

  private static int getNumberLeadingInactiveNotes() {
    int index = 0;
    while (customKeys.get(index).isInactive()) {
      index++;
    }
    return index;
  }

  private static int getNumberTrailingInactiveNotes() {
    int index = 14;
    while (customKeys.get(index).isInactive()) {
      index--;
    }
    return 14 - index;
  }

  /**
   * A recursive method used to map the keys in customKeys to the specified blackRow and whiteRow.
   * The mapping is done in a way that the keys are mapped on the QWERTY keyboard as they would
   * be on a real piano, ie next black note in list is at the top right corner or previous
   * white note.
   * This recursive method maps as many keys as possible, up to a maximum of 9 on one row.
   * It can go both ways, mapping keys left to right or right to left on the keyboard.
   *
   * @param blackRow List of KeyEvent.keyCode
   * @param whiteRow List of KeyEvent.keyCode
   * @param indexKeyList The index of the key in customKeys list to be mapped.
   * @param endIndex The index of the last key in customKeys list to be mapped.
   * @param position The index of the keyCode of blackRow/whiteRow to map a key name to.
   * @param next +/- 1 :
   * @return true if 15 keys were mapped, false otherwise.
   */
  private static boolean mapCustomKeysToQwertyKeyboard(List<Integer> blackRow,
                                                       List<Integer> whiteRow,
                                                       int indexKeyList,
                                                       int endIndex,
                                                       int position,
                                                       int next) {

    if ((indexKeyList == endIndex + 1 && next == 1) || (indexKeyList == -1 && next == -1)) {
      // All keys were mapped
      return true;
    }

    if ((position == 9 && next == 1) || (position == -1 && next == -1)) {
      // STOP HERE! All 9 QWERTY key are mapped, however not all piano keys have been mapped yet
      return false;
    }

    // The key to map
    CustomPianoKey keyToMap = customKeys.get(indexKeyList);

    if (keyToMap.getColor() == Color.BLACK) { // WE ARE MAPPING ON BLACK ROW

      // Add a new mapping
      keyMap.put(blackRow.get(position), keyToMap.getKeyFullName());
      addMappingToCustomKey(keyToMap.getKeyFullName(), (char) blackRow.get(position).byteValue());


      if (next == 1) { // going left-to-right
        keyToMap.setMappingOnTwoLastRows(false);
        if (indexKeyList == endIndex) { // We can stop here, all piano keys are mapped
          return true;
        } else if (customKeys.get(indexKeyList + next).getColor() == Color.BLACK) {
          position += next;
        }
      } else { // next == -1, going right-to-left
        keyToMap.setMappingOnTwoLastRows(true);
        position += next;
      }

    } else { // WE ARE MAPPING ON WHITE ROW

      // Add a new mapping
      keyMap.put(whiteRow.get(position), keyToMap.getKeyFullName());
      addMappingToCustomKey(keyToMap.getKeyFullName(), (char) whiteRow.get(position).byteValue());

      if (next == 1) { // going left-to-right
        keyToMap.setMappingOnTwoLastRows(false);
        position += next;
      } else { // next == -1, going right-to-left
        if (indexKeyList == 0) { // We can stop here, all piano keys are mapped
          return true;
        } else if (customKeys.get(indexKeyList + next).getColor() == Color.WHITE) {
          position += next;
        }
        keyToMap.setMappingOnTwoLastRows(true);
      }
    }

    return mapCustomKeysToQwertyKeyboard(blackRow, whiteRow, indexKeyList + next, endIndex, position, next);
  }

  private static void addMappingToCustomKey(String fullKeyName, Character otherMapping) {
    for (CustomPianoKey key : customKeys) {
      if (key.getKeyFullName().equals(fullKeyName)) {
        key.addMapping(otherMapping);
      }
    }
  }

  private static boolean isOutOfRange(int octave) {
    return ((octave == 0) || (octave == 8));
  }

  private static void clearMapping() {
    keyMap.clear();
    for (CustomPianoKey key : customKeys) {
      key.clearMapping();
    }
  }

  private static int getIndexPrevKey(List<String> scale, String keyName) {
    int index = 0;

    while (!scale.get(index).equals(keyName)) {
      index++;
    }

    if (index == 0) {
      return scale.size() - 1;
    } else {
      return index - 1;
    }
  }

  private static int getIndexNextKey(List<String> scale, String keyName) {
    int index = 0;

    while (!scale.get(index).equals(keyName)) {
      index++;
    }

    if (index == scale.size() - 1) {
      return 0;
    } else {
      return index + 1;
    }
  }

  private static Color findKeyColor(String keyName) {
    return keyName.contains("#") ? Color.BLACK : Color.WHITE;
  }

  private static boolean isRootNote(String key, String rootNote) {
    return key.equals(rootNote);
  }

  private static int findNumberLeadingAB(List<String> scale) {
    int index = 0;

    while ((scale.get(index).contains("A") || (scale.get(index).contains("B")))) {
      index++;
    }
    return index;
  }

}
