package playcontrolpane;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

/**
 * The ScaleLoader is responsible for loading a file containing a list of scales (.csv) of the
 * form [scale name, note #1, note #2, ... note #n] where n is the number of notes in the scale,
 * and storing the content of that file.
 */
public class ScaleLoader {

  // A set of scale names
  private static Set<String> scaleList;
  // Map the scale names to a list of scales (a scale being a list of note names)
  private static HashMap<String, ArrayList<ArrayList<String>>> scaleNotes;

  // Keep track of the selected root note, and scale name, as selected by user
  private static String currentRootNote;
  private static ArrayList<String> currentScale;

  /**
   * Init instance variables.
   */
  public ScaleLoader() {
    scaleList = new HashSet<>();
    scaleNotes = new HashMap<>();
    currentScale = new ArrayList<>();
  }

  /**
   * Method called when a scale file is selected. The method parses the file and stores the list
   * of scales it contains in scaleList and scaleNotes.
   * @param scales Scale file
   * @return 0 if the scales were successfully retrieved.
   */
  public static int readScaleFile(File scales) {
    String scalesPath = scales.getPath();
    try {
      BufferedReader in = new BufferedReader(new FileReader(scalesPath));
      String line;
      // Read all lines in file
      while ((line = in.readLine()) != null) {

        java.util.List<String> noteList = Arrays.asList(line.split(","));
        if (!noteList.get(1).equals(noteList.get(noteList.size() - 1))) {
          return 1; // First and last notes in scale are different
        }

        // Getting the scale Name
        String scaleName = noteList.get(0);
        if (!scaleList.contains(scaleName)) {
          // Add a new scale to the map
          scaleNotes.put(scaleName, new ArrayList<>());
        }
        scaleList.add(scaleName); // A set will ignore duplicates

        // Fill new scale with notes, ignore the last one (duplicate of the first one) and the
        // 'flat' part of black notes
        ArrayList<String> notes = new ArrayList<>();
        for (int i = 1; i < noteList.size() - 1; i++) {
          String[] currentNote = (noteList.get(i).split("/"));
          notes.add(currentNote[0]);
        }
        scaleNotes.get(scaleName).add(notes);
      }
    } catch (IOException e) {
      return 2;
    } catch (ArrayIndexOutOfBoundsException e) {
      return 3;
    }
    return 0;
  }

  public static void setNewScale(String scaleName, String rootNote) {
    currentScale = new ArrayList<>();
    currentRootNote = rootNote;

    for (ArrayList<String> notes : scaleNotes.get(scaleName)) {
      if (notes.get(0).equals(rootNote)) {
        currentScale = notes;
      }
    }
  }


  // ####################### SETTER METHODS ##################### //


  public static Object[] getArrayScalesName() {
    return scaleList.toArray();
  }


  // ####################### GETTER METHODS ##################### //


  public static ArrayList<String> getCurrentScale() {
    return currentScale;
  }

  public static String getRootNote() {
    return currentRootNote;
  }
}