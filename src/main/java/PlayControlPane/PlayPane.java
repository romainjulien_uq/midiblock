package playcontrolpane;

import playcontrolpane.custompiano.CustomKeyboard;
import style.Style;
import java.awt.*;
import javax.swing.*;
import javax.swing.border.EtchedBorder;
import noteconverter.NoteConverter;
import output.OutputManager;
import output.outputevent.NoteOffEvent;
import output.outputevent.NoteOnEvent;
import output.outputevent.OutputEventListener;


/**
 * The play pane is where the custom keyboard appears, as well as the buttons to shift it.
 * It also contains a panel that displays the note played.
 */
public class PlayPane implements OutputEventListener {
  private JPanel playPane;

  private JPanel keyboardPane;
  private JPanel notePlayedPane;
  private CustomKeyboard keyboard;
  private JPanel keyboardShiftersWrapper;

  private JButton leftOctave;
  private JButton leftSingle;
  private JButton rightSingle;
  private JButton rightOctave;

  public PlayPane() {
    playPane = new JPanel(new BorderLayout());
    playPane.setBackground(Style.COLOR_GREY);
    playPane.setBorder(BorderFactory.createEtchedBorder(EtchedBorder.LOWERED));

    initAndLayoutComponents();

    // Listen to note played and display them in the 'note played panel'
    OutputManager.getInstance().addOutputEventListener(this);
  }

  /**
   * - Initiates and layout play panel components
   */
  private void initAndLayoutComponents() {
    playPane = new JPanel(new BorderLayout());
    playPane.setBackground(Style.COLOR_DARK_GREY);
    playPane.setBorder(BorderFactory.createEtchedBorder(EtchedBorder.LOWERED));

    keyboardPane = new JPanel(new BorderLayout());
    keyboardPane.setBackground(Style.COLOR_GREY);
    keyboardPane.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));

    initPlayPaneComponents();

    playPane.add(keyboardPane, BorderLayout.CENTER);
  }

  private void initPlayPaneComponents() {
    // Instanciate default custom keyboard
    keyboard = new CustomKeyboard();
    keyboard.setAlignmentY(Component.CENTER_ALIGNMENT);

    notePlayedPane = new JPanel();
    BoxLayout layout = new BoxLayout(notePlayedPane, BoxLayout.X_AXIS);
    notePlayedPane.setLayout(layout);
    notePlayedPane.setBorder(BorderFactory.createEtchedBorder(EtchedBorder.LOWERED));
    notePlayedPane.setBackground(Style.COLOR_LIGHT_GREY);

    JLabel title = new JLabel("Note Played: ");
    title.setFont(Style.FONT_TEXT_HIGHLIGHT);
    title.setForeground(Style.COLOR_DARK_GREY);
    title.setAlignmentY(Component.CENTER_ALIGNMENT);
    title.setBorder(BorderFactory.createEmptyBorder(10, 5, 10, 5));
    notePlayedPane.add(title);

    keyboardShiftersWrapper = new JPanel();
    BoxLayout wrapperLayout = new BoxLayout(keyboardShiftersWrapper, BoxLayout.X_AXIS);
    keyboardShiftersWrapper.setLayout(wrapperLayout);
    keyboardShiftersWrapper.setOpaque(false);

    initKeyboardShifters();

    keyboardShiftersWrapper.add(leftSingle);
    keyboardShiftersWrapper.add(leftOctave);
    keyboardShiftersWrapper.add(Box.createHorizontalStrut(10));
    keyboardShiftersWrapper.add(keyboard);
    keyboardShiftersWrapper.add(Box.createHorizontalStrut(10));
    keyboardShiftersWrapper.add(rightOctave);
    keyboardShiftersWrapper.add(rightSingle);

    keyboardPane.add(notePlayedPane, BorderLayout.NORTH);
  }

  private void initKeyboardShifters() {
    leftOctave = new JButton("<<");
    leftOctave.setFont(Style.FONT_TEXT_HIGHLIGHT);
    leftOctave.setForeground(Style.COLOR_DARK_ORANGE);
    leftOctave.setPreferredSize(new Dimension(50, leftOctave.getPreferredSize().height));
    leftOctave.setAlignmentY(Component.CENTER_ALIGNMENT);
    leftOctave.addActionListener((e) -> shiftKeyboardLeftOneOctave());

    leftSingle = new JButton("<");
    leftSingle.setFont(Style.FONT_TEXT_HIGHLIGHT);
    leftSingle.setForeground(Style.COLOR_DARK_ORANGE);
    leftSingle.setPreferredSize(new Dimension(50, leftSingle.getPreferredSize().height));
    leftSingle.setAlignmentY(Component.CENTER_ALIGNMENT);
    leftSingle.addActionListener((e) -> shiftKeyboardLeftOneNote());

    rightSingle = new JButton(">");
    rightSingle.setFont(Style.FONT_TEXT_HIGHLIGHT);
    rightSingle.setForeground(Style.COLOR_DARK_ORANGE);
    rightSingle.setPreferredSize(new Dimension(50, rightSingle.getPreferredSize().height));
    rightSingle.setAlignmentY(Component.CENTER_ALIGNMENT);
    rightSingle.addActionListener((e) -> shiftKeyboardRightOneNote());

    rightOctave = new JButton(">>");
    rightOctave.setFont(Style.FONT_TEXT_HIGHLIGHT);
    rightOctave.setForeground(Style.COLOR_DARK_ORANGE);
    rightOctave.setPreferredSize(new Dimension(50, rightOctave.getPreferredSize().height));
    rightOctave.setAlignmentY(Component.CENTER_ALIGNMENT);
    rightOctave.addActionListener((e) -> shiftKeyboardRightOneOctave());

  }

  private void shiftKeyboardLeftOneOctave() {
    if (!KeyMapper.firstKeyIsInactive()) {
      KeyMapper.shiftKeyboardOneOctave(-1);
      keyboard.refreshKeyboard();
    }
  }

  private void shiftKeyboardLeftOneNote() {
    if (!KeyMapper.firstKeyIsInactive()) {
      KeyMapper.shiftKeyboardLeftOneNote(ScaleLoader.getCurrentScale(), ScaleLoader.getRootNote());
      keyboard.refreshKeyboard();
    }
  }

  private void shiftKeyboardRightOneNote() {
    if (!KeyMapper.lastKeyIsInactive()) {
      KeyMapper.shiftKeyboardRightOneNote(ScaleLoader.getCurrentScale(), ScaleLoader.getRootNote());
      keyboard.refreshKeyboard();
    }
  }

  private void shiftKeyboardRightOneOctave() {
    if (!KeyMapper.lastKeyIsInactive()) {
      KeyMapper.shiftKeyboardOneOctave(1);
      keyboard.refreshKeyboard();
    }
  }

  public CustomKeyboard getKeyboard() {
    return keyboard;
  }

  public void showKeyboard() {
    keyboardPane.add(keyboardShiftersWrapper, BorderLayout.SOUTH);
    keyboardPane.updateUI();
  }

  /**
   * @return The input panel (contains all components of this view)
   */
  public JPanel getPanel() {
    return playPane;
  }

  @Override
  public void noteOn(NoteOnEvent event) {
    String noteName = NoteConverter.getNoteName(event.getMidiNumber());

    JLabel notePlayed = new JLabel(noteName);
    notePlayed.setFont(Style.FONT_TEXT_HIGHLIGHT);
    notePlayed.setForeground(Style.COLOR_DARK_ORANGE);
    notePlayed.setAlignmentY(Component.CENTER_ALIGNMENT);

    notePlayedPane.add(Box.createHorizontalStrut(5));
    notePlayedPane.add(notePlayed);
    notePlayedPane.updateUI();
  }

  @Override
  public void noteOff(NoteOffEvent event) {
    String noteName = NoteConverter.getNoteName(event.getMidiNumber());

    JLabel notePlayed = new JLabel(noteName);
    notePlayed.setFont(Style.FONT_TEXT_HIGHLIGHT);
    notePlayed.setForeground(Style.COLOR_GREY);
    notePlayed.setAlignmentY(Component.CENTER_ALIGNMENT);

    notePlayedPane.add(Box.createHorizontalStrut(5));
    notePlayedPane.add(notePlayed);
    notePlayedPane.updateUI();
  }
}

