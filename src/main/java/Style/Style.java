package style;

import java.awt.*;
import java.net.URL;
import javax.swing.*;

/**
 * The style (ie font, color) to keep the color and font schemes consistent
 * throughout the GUI. This class also stores links to images used in the GUI.
 */
public class Style {

  // Fonts used
  public static final Font FONT_TITLE = new Font("Helvetica Neue", Font.BOLD, 14);
  public static final Font FONT_TEXT_HIGHLIGHT = new Font("Helvetica Neue", Font.BOLD, 12);
  public static final Font FONT_TEXT_ITALIC = new Font("Helvetica Neue", Font.ITALIC, 12);
  public static final Font FONT_TEXT = new Font("Helvetica Neue", Font.PLAIN, 12);
  public static final Font FONT_ADD_BUTTON = new Font("Helvetica Neue", Font.PLAIN, 30);

  // Color used
  public static final Color COLOR_DARK_ORANGE = new Color(179, 84, 0, 255);
  public static final Color COLOR_DARK_ORANGE_TRANSPARENT = new Color(179, 84, 0, 100);
  public static final Color COLOR_ORANGE = new Color(179, 114, 41, 255);
  public static final Color COLOR_LIGHT_ORANGE = new Color(237, 174, 99, 255);
  public static final Color COLOR_LIGHT_ORANGE_TRANSPARENT = new Color(237, 174, 99, 100);
  public static final Color COLOR_DARK_GREY = new Color(56, 58, 59, 255);
  public static final Color COLOR_GREY = new Color(80, 81, 83, 255);
  public static final Color COLOR_LIGHT_GREY = new Color(203, 200, 196, 255);

  public static final Color COLOR_WHITE_TRANSPARENT = new Color(255, 255, 255, 50);
  public static final Color COLOR_BLACK_TRANSPARENT = new Color(0, 0, 0, 50);

  // Images used
  public static ClickImage recordButton;
  public static ClickImage pauseButton;
  public static ClickImage playButton;
  public static ClickImage stopButton;
  public static ImageIcon refreshIcon;

  public static void setRecordButton(URL released, URL pressed) {
    recordButton = new ClickImage(released, pressed);
  }

  public static void setPauseButton(URL released, URL pressed) {
    pauseButton = new ClickImage(released, pressed);
  }

  public static void setPlayButton(URL released, URL pressed) {
    playButton = new ClickImage(released, pressed);
  }

  public static void setStopButton(URL released, URL pressed) {
    stopButton = new ClickImage(released, pressed);
  }
}
