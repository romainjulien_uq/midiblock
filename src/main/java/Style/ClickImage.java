package style;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.net.URL;
import javax.swing.*;

/**
 * A ClickImage is made up of two images (one when is pressed, one when its released) to be
 * used as a custom button.
 */
public class ClickImage extends JLabel implements MouseListener {

  // Image displayed when the ClickImage is pressed, or set selected
  private URL pressed;
  // Image displayed when the ClickImage is released
  private URL released;
  // Used as a flag to "disable" the ClickImage
  private boolean isSelected = false;

  /**
   * Get the URLs of the images to use.
   * @param released
   * @param pressed
   */
  public ClickImage(URL released, URL pressed) {
    super(new ImageIcon(released));
    this.pressed = pressed;
    this.released = released;
    addMouseListener(this);
  }


  // ####################### GETTER METHODS ##################### //


  public boolean isSelected() {
    return isSelected;
  }


  // ####################### SETTER METHODS ##################### //


  /**
   * Display 'pressed' image if select = true, and 'released' image otherwise.
   * This is used as a way to disable the ClickImage (acting as a button).
   *
   * @param select Indicate the state of this ClickImage
   */
  public void setSelected(boolean select) {
    if (select) {
      setIcon(new ImageIcon(this.pressed));
    } else {
      setIcon(new ImageIcon(this.released));
    }
    isSelected = select;
  }


  // ########### MOUSE EVENT LISTENER METHODS ############### //


  @Override
  public void mouseClicked(MouseEvent e) {

  }

  @Override
  public void mousePressed(MouseEvent event) {
    if (isSelected()) {
      return;
    }
    setIcon(new ImageIcon(this.pressed));
  }

  @Override
  public void mouseReleased(MouseEvent event) {
    if (isSelected()) {
      return;
    }
    setIcon(new ImageIcon(this.released));
  }

  @Override
  public void mouseEntered(MouseEvent e) {

  }

  @Override
  public void mouseExited(MouseEvent e) {

  }
}
