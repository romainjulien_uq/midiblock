package style;

import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import javax.swing.*;
import javax.swing.border.Border;

/**
 * A ClickLabel is an extension of JLabel to be used as a button.
 */
public class ClickLabel extends JLabel implements MouseListener {

  // Color of the label when pressed
  private Color pressed;
  // Color of the label when released
  private Color released;
  // Used as a flag to "disable" the ClickLabel
  private boolean isSelected = false;
  // true if it should have a border, false otherwise
  private boolean hasBorder;

  /**
   * Get the text and the colors to use. Set a border if withBorder == true.
   *
   * @param text The text of the label
   * @param released The color of the label when pressed
   * @param pressed The color of the label when released
   * @param withBorder indicate if the ClickLabel should have a border
   */
  public ClickLabel(String text, Color released, Color pressed, boolean withBorder) {
    super(text);
    setForeground(released);

    this.pressed = pressed;
    this.released = released;
    this.hasBorder = withBorder;

    if (hasBorder) {
      setBorder();
    }
    addMouseListener(this);
  }


  // ####################### GETTER METHODS ##################### //


  public boolean isSelected() {
    return isSelected;
  }


  // ####################### SETTER METHODS ##################### //


  /**
   * Change color to 'pressed' if select = true, and 'released' otherwise.
   * This is used as a way to disable the ClickLabel (acting as a button).
   *
   * @param select Indicate the state of this ClickLabel
   */
  public void setSelected(boolean select) {
    if (select) setForeground(this.pressed);
    else setForeground(this.released);
    isSelected = select;
    if (hasBorder) {
      setBorder();
    }
  }

  /**
   * If the ClickLabel should have a border, then set a line border with insets of the same
   * color as the text.
   */
  public void setBorder() {
    Color borderColor = isSelected ? this.pressed : this.released;

    Border insets = BorderFactory.createEmptyBorder(2, 15, 2, 15);
    Border lineBorder = BorderFactory.createLineBorder(borderColor, 2, true);

    setBorder(BorderFactory.createCompoundBorder(lineBorder, insets));
  }


  // ########### MOUSE EVENT LISTENER METHODS ############### //


  @Override
  public void mouseClicked(MouseEvent e) {

  }

  @Override
  public void mousePressed(MouseEvent event) {
    if (isSelected()) return;
    setForeground(this.pressed);
    if (hasBorder) {
      setBorder();
    }
  }

  @Override
  public void mouseReleased(MouseEvent event) {
    if (isSelected()) return;
    setForeground(this.released);
    if (hasBorder) {
      setBorder();
    }
  }

  @Override
  public void mouseEntered(MouseEvent e) {

  }

  @Override
  public void mouseExited(MouseEvent e) {

  }
}
