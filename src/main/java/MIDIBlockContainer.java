import playcontrolpane.PlayPane;
import settings.ControlPane;
import settings.SettingsPane;
import style.Style;
import blockscontrolpane.BlocksConfigurationPane;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.*;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;
import javax.swing.border.EtchedBorder;

/**
 * The container encapsulates this entire application.
 */
public class MIDIBlockContainer {

  private JPanel topPane;

  private JPanel tabPane;
  private JLabel settingTab;
  private JLabel playTab;
  private JLabel selected;

  private JPanel settingPane;
  private JPanel playPane;

  private JPanel rightControlPane;

  private JPanel blocksConfigurationPane;

  public MIDIBlockContainer(Container container) {
    initGUI(container);
  }

  /**
   * The GUI is divided in two parts:
   * The top pane, contains a tab-driven panel on the left and a control panel on the
   * right. The tab-driven panel contains the tab pane (ie tab selection) and one of the setting or
   * the play pane.
   * The bottom pane is the blockConfiguration pane.
   * @param container
   */
  private void initGUI(Container container) {

    container.setBackground(Style.COLOR_DARK_GREY);

    topPane = new JPanel(new BorderLayout());
    topPane.setOpaque(false);
    topPane.setBorder(new EmptyBorder(0, 20, 0, 20));

    initTabPane();

    PlayPane play = new PlayPane();
    playPane = play.getPanel();
    settingPane = (new SettingsPane(play)).getPanel();
    ControlPane controls = new ControlPane();
    rightControlPane = (controls).getPanel();

    topPane.add(tabPane, BorderLayout.NORTH);
    // When application is launched, the setting panel is displayed
    topPane.add(settingPane, BorderLayout.CENTER);
    selected = settingTab;

    JPanel wrapRightControls = new JPanel();
    wrapRightControls.setOpaque(false);
    wrapRightControls.add(rightControlPane);
    topPane.add(wrapRightControls, BorderLayout.EAST);

    blocksConfigurationPane = (new BlocksConfigurationPane(controls)).getPanel();
    blocksConfigurationPane.setAlignmentX(Component.LEFT_ALIGNMENT);

    JPanel blockPaneWrapper = new JPanel();
    BoxLayout layout = new BoxLayout(blockPaneWrapper, BoxLayout.Y_AXIS);
    blockPaneWrapper.setLayout(layout);
    blockPaneWrapper.setBorder(new EmptyBorder(10, 20, 0, 20));
    blockPaneWrapper.setOpaque(false);
    blockPaneWrapper.add(blocksConfigurationPane);

    container.add(topPane, BorderLayout.CENTER);
    container.add(blockPaneWrapper, BorderLayout.SOUTH);
  }

  /**
   * The top of the container has a tab pane. contains two tabs
   * Play and MIDIBlock (ie, settings for the application).
   */
  private void initTabPane() {
    tabPane = new JPanel();
    BoxLayout layout = new BoxLayout(tabPane, BoxLayout.X_AXIS);
    tabPane.setLayout(layout);
    tabPane.setBackground(Style.COLOR_DARK_GREY);

    Border loweredBorder = BorderFactory.createEtchedBorder(EtchedBorder.LOWERED);
    Border tabInset = BorderFactory.createEmptyBorder(5, 30, 5, 30);

    settingTab = new JLabel("MIDIBlock");
    settingTab.setFont(Style.FONT_TEXT_HIGHLIGHT);
    settingTab.setForeground(Style.COLOR_LIGHT_GREY);
    settingTab.setBackground(Style.COLOR_GREY);
    settingTab.setOpaque(true);
    settingTab.setBorder(BorderFactory.createCompoundBorder(loweredBorder, tabInset));
    settingTab.addMouseListener(new MouseAdapter() {
      @Override
      public void mouseClicked(MouseEvent e) {
        switch (selected.getText()) {
          case "MIDI Files":
            break;
          default:
            selected.setBackground(Style.COLOR_DARK_GREY);
            selected.setOpaque(false);
            settingTab.setBackground(Style.COLOR_GREY);
            settingTab.setOpaque(true);
            selected = settingTab;

            topPane.remove(playPane);
            topPane.add(settingPane, BorderLayout.CENTER);
            topPane.updateUI();
        }
      }
    });

    playTab = new JLabel("Play");
    playTab.setFont(Style.FONT_TEXT_HIGHLIGHT);
    playTab.setForeground(Style.COLOR_LIGHT_GREY);
    playTab.setBackground(Style.COLOR_DARK_GREY);
    playTab.setOpaque(true);
    playTab.setBorder(BorderFactory.createCompoundBorder(loweredBorder, tabInset));
    playTab.addMouseListener(new MouseAdapter() {
      @Override
      public void mouseClicked(MouseEvent e) {
        switch (selected.getText()) {
          case "Play":
            break;
          default:
            selected.setBackground(Style.COLOR_DARK_GREY);
            selected.setOpaque(false);
            playTab.setBackground(Style.COLOR_GREY);
            playTab.setOpaque(true);
            selected = playTab;

            topPane.remove(settingPane);
            topPane.add(playPane, BorderLayout.CENTER);
            topPane.updateUI();
        }
      }
    });

    tabPane.add(settingTab);
    tabPane.add(playTab);

  }
}
